function [C] = kfupddefaultmtimes(mode, A, B) %#codegen
%KFUPDDEFAULTMTIMES 卡尔曼滤波更新计算中默认矩阵乘法
%
% Input Arguments:
% mode: 枚举标量，矩阵乘法标志，见MTimesFcnUpdAlgo，PHT表示P*H'，HPHT表示H*P*H'，Hx表示H*x，KH表示K*H，MPMT表示M*P*M'，KGammaMRGammaMTKT表示K*GammaMRGammaMT'*K'
% int8(mode):   1       2       3       4       5       6
% A:            P       H       H       K       M       K
% B:            H'      P       x       H       P       GammaMRGammaMT
%
% Output Arguments:
% D: 根据mode对应输出

coder.extrinsic('warning');

switch mode
    case {int8(KFUpdMTimesFcnType.PHT), int8(KFUpdMTimesFcnType.Hx), int8(KFUpdMTimesFcnType.KH)}
        C = A*B;
    case {int8(KFUpdMTimesFcnType.HPHT), int8(KFUpdMTimesFcnType.KGammaMRGammaMTKT)}
        C = A*B*A';
    case int8(KFUpdMTimesFcnType.MP)
        C = A*B;
    case int8(KFUpdMTimesFcnType.MPMT)
        C = A*B;
    otherwise
        warning('defaultmtimes_upd:mode', 'The mode mismatch');
        C = 0;
end
end