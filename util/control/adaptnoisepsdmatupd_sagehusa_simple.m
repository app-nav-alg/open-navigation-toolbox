function [R, Q] = adaptnoisepsdmatupd_sagehusa_simple(z, x, P, R, H, cycCnt, b, Q, K, PPost, PPost1, Phi) %#codegen
%ADAPTNOISEPSDMATUPD_SAGEHUSA_SIMPLE Sage-Husa自适应卡尔曼滤波的噪声统计估计器，简化式，适用于噪声均值为零
%
% Input Arguments:
% # z: n*1列向量，量测量
% # x: m*1列向量，当前周期滤波器外推后的状态向量
% # P: m*m矩阵，卡尔曼滤波器外推计算的当前周期的状态误差协方差矩阵
% # R: n*n矩阵，R为前一周期量测噪声序列协方差矩阵
% # H: n*m矩阵，量测矩阵
% # cycCnt: 标量，周期计数
% # b: 标量，遗忘因子，（0<b<1）通常取0.95~0.99
% # Q: m*m矩阵，前一周期的系统噪声序列协方差矩阵
% # q: m*1列向量，前一周期系统噪声序列均值
% # xPost: m*1列向量，当前周期滤波器更新后的状态向量
% # xPost1: m*1列向量，前一周期滤波器更新后的状态向量
% # K: m*n矩阵，卡尔曼滤波器增益矩阵
% # PPost: m*m矩阵，卡尔曼滤波器更新计算的当前周期状态误差协方差矩阵
% # PPost1: m*m矩阵，卡尔曼滤波器更新计算的前一周期状态误差协方差矩阵
% # Phi: m*m矩阵，离散化之后的状态转移矩阵
%
% Output Arguments:
% # r: n*1列向量，当前周期观测噪声序列均值
% # R: n*n矩阵，当前周期观测噪声序列协方差矩阵
% # q: m*1列向量，当前周期系统噪声序列均值
% # Q: m*m矩阵，当前周期系统噪声序列协方差矩阵
%
% References:
% # 《应用导航算法工程基础》“Sage-Husa自适应滤波”“卡尔曼滤波的实现”

dn = (1-b)/(1-b^(cycCnt+1));
z_Tilde = z - H*x;
R = (1-dn)*R + dn*(z_Tilde*z_Tilde' - H*P*H');
if nargin > 7 % 计算Q    
    Q = (1-dn)*Q + dn*(((K*z_Tilde)*z_Tilde')*K' + PPost - Phi*PPost1*Phi');
end
end