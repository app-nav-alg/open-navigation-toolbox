function [ P ] = covmatnumcondctrl( P, PDiagMin, PDiagMax )
%COVMATNUMCONDCTRL 协方差矩阵数值条件控制
%
% Input Arguments:
% # 见kalmfiltstep函数帮助说明
%
% Output Arguments:
% # 见kalmfiltstep函数帮助说明
%
% References:
% # 《应用导航算法工程基础》“误差协方差矩阵的数值条件控制”

P = (P+P') / 2;
P = limitposdefmatdiag(P, PDiagMin, PDiagMax);
P = posdefizesymmat(P);
end