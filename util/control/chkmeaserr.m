function [r, chiSqIndex, measResidChkPass, chiSqChkPass, ADiagSqrt] = chkmeaserr(x, P, zObs, H, GammaMRGammaMT, doMeasResidChk, chiSqChkThreshold)
%CHKMEASERR 检验粗大量测误差（量测残差检验与χ^2检验）
%
% Input Arguments:
% 见kalmfiltstep函数帮助说明
%
% Output Arguments:
% 见kalmfiltstep函数帮助说明
%
% References:
% 《应用导航算法工程基础》“粗大量测误差的检验”

% 量测残差检验
r = zObs - H*x;
A = GammaMRGammaMT + H*P*H';
ADiagSqrt = sqrt(diag(A));
if doMeasResidChk
    measResidChkPass = (abs(r) <= 3*ADiagSqrt);
else
    measResidChkPass = true(size(zObs));
end

% χ^2检验
chiSqIndex = r' / A * r;
if ((chiSqChkThreshold<0) || (chiSqIndex<=chiSqChkThreshold))
    chiSqChkPass = true;
else
    chiSqChkPass = false;
end
end