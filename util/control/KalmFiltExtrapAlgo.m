classdef (Enumeration) KalmFiltExtrapAlgo < int32
    enumeration
        NORMAL(1)
        ITER(2)
    end
end