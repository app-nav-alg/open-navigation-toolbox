classdef (Enumeration) KalmFiltUpdAlgo < int32
    enumeration
        NORMAL(1)
        JOSEPH(2)
        JOSEPH_SIMPLE(3)
    end
end