function [C] = kfextrapdefaultmtimes(mode, A, B) %#codegen
%KFEXTRAPDEFAULTMTIMES 卡尔曼滤波外推计算中默认矩阵乘法
%
% Input Arguments:
% mode: 枚举标量，矩阵乘法标志位，见MTimesFcnExtrapAlgo，FF表示F*F，FSquareF表示F^2*F，Phix表示Phi*x，PhiPPhiT表示Phi*P*Phi'
% int8(mode):   1       2       3       4
% A:            F       F^2     Phi     Phi
% B:            F       F       x       P
%
% Output Arguments:
% D: 根据mode对应输出

coder.extrinsic('warning');

switch mode
    case {int8(KFExtrapMTimesFcnType.FF), int8(KFExtrapMTimesFcnType.FSquareF), int8(KFExtrapMTimesFcnType.Phix)}
        C = A*B;    
    case int8(KFExtrapMTimesFcnType.PhiPPhiT)
        C = A*B;
    case int8(KFExtrapMTimesFcnType.PhiP)
        C = A*B;
    otherwise
        warning('defaultmtimes_extrap:mode', 'The mode mismatch');
        C = 0;
end
end