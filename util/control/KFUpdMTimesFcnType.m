classdef (Enumeration) KFUpdMTimesFcnType < int32
    enumeration
        PHT(1)
        HPHT(2)
        Hx(3)
        KH(4)
        MPMT(5)
        KGammaMRGammaMTKT(6)
        MP(7)
    end
end