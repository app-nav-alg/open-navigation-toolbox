classdef (Enumeration) KFExtrapMTimesFcnType < int32
    enumeration
        FF(1)
        FSquareF(2)
        Phix(3)
        PhiPPhiT(4)
        PhiP(5)
    end
end