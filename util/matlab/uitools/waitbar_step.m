function [ fout ] = waitbar_step( x, step, whichbar ) %#codegen
%WAITBAR_STEP 仅在达到指定步长后更新进度条
%
% Tips
% # 使用本函数以减少进度条更新次数，缩短运行时间
%
% Input Arguments
% # x: 见waitbar函数文档
% # step: 标量，下一次更新与上一次更新之间所需的间隔，在0至1之间
% # whichbar: 见waitbar函数文档
%
% Output Arguments
% # fout: 见waitbar函数文档，在并行计算的worker模式下返回为空
%
% Examples
% # hWB = waitbar_step(0, 0.01);
% for i=1:100
%     waitbar_step(i/100, 0.01, hWB);
% end
% close(hWB);
%
% Assumptions and Limitations
% # 仅同时支持一个进度条实例

coder.extrinsic('waitbar', 'iscellstr');

persistent WBS_stepSumForRefresh

if isempty(WBS_stepSumForRefresh)
    WBS_stepSumForRefresh = 0;
end

if coder.target('MATLAB') || coder.target('MEX') || isempty(getCurrentWorker()) % 并行运算模式下不显示进度条
    if nargin < 3 % 创建无名称进度条
        WBS_stepSumForRefresh = 0;
        fout = waitbar(x);
    elseif ischar(whichbar) || iscellstr(whichbar) % 创建有名称进度条
        WBS_stepSumForRefresh = 0;
        fout = waitbar(x, whichbar);
    else % 更新进度条
        if x >= WBS_stepSumForRefresh
            fout = waitbar(x, whichbar);
            WBS_stepSumForRefresh = WBS_stepSumForRefresh + step;
        end
    end
else
    fout = [];
end
end