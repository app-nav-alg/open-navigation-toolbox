function [ B ] = rss_cg( A, dim ) %#codegen
%RSS_CG 计算矩阵在指定维上的和方根（root sum square）
%
% Input Arguments:
% A: 矩阵
% dim: 标量，指定计算所沿的维数

B = sum(A.^2, dim).^(1/2);
end
