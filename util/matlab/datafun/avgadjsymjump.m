function [B] = avgadjsymjump(A, threshold, adjLen) %#codegen
%AVGADJSYMJUMP 平均相邻的正负对称的跳变
%
% Tips:
% 1. 相邻对称跳变点指类似于0 100 0 -100 0的相邻的正负向跳变，本例子中threshold为100，adjLen为2
%
% Input Arguments:
% A: 二维矩阵，按列进行处理
% threshold: 向量，长度与A列数相同，对应于A各列的跳变阈值，即相邻元素之差达到此值的认为发生跳变
% adjLen: 标量，如果找到一个跳变点索引为i，则在i+1至i+adjLen的元素中寻找对称跳变点，默认为2
%
% Output Arguments:
% B: 尺寸与A相同的矩阵，A的各列中的对称跳变点在B中的取值变为该点对的平均值，其它值与A相同

assert(ismatrix(A));
if (nargin < 3)
    adjLen = 2;
end

B = A;
[m, n] = size(B);
for j=1:n
    for i=2:m
        jumpAmpPrior = B(i, j)-B(i-1, j);
        if abs(jumpAmpPrior) >= threshold(j) % NOTE: 使用线性索引
            for k=(i+1):(i+adjLen)
                if k < m
                    jumpAmpPost = B(k+1, j)-B(k, j);
                    if (abs(jumpAmpPost)>=threshold(j)) ...
                            && (abs(jumpAmpPrior+jumpAmpPost)>abs(jumpAmpPrior)) ...
                            && (abs(jumpAmpPrior+jumpAmpPost)>abs(jumpAmpPost))
                        tmp = (B(i, j) + B(k, j)) / 2;
                        B(i, j) = tmp;
                        B(k, j) = tmp;
                        break;
                    end
                end
            end
        end
    end
end