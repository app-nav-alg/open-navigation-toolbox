function [ s ] = clusterstd( X, clusterLength ) %#codegen
%CLUSTERSTD 将二维矩阵沿列方向分为长度相等的多个簇，并沿列方向计算各簇的方差
%
% Input Arguments:
% X: 二维矩阵
% clusterLength: 标量，簇长度
%
% Output Arguments:
% s: 二维矩阵

rowNum = ceil(size(X, 1)/clusterLength);
s = coder.nullcopy(NaN(rowNum, size(X, 2)));
for i=1:rowNum
    bgnRowIdx = (i-1)*clusterLength + 1;
    endRowIdx = i * clusterLength;
    s(i, :) = std(X(bgnRowIdx:min(endRowIdx, end), :), 0, 1);
end
end