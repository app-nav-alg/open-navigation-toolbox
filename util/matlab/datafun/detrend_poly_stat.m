function [y] = detrend_poly_stat(x, statFlag, delNonStat, n) %#codegen
% DETREND_POLY_STAT 去掉多项式拟合后的趋势项，仅针对静态段
%
% Input Arguments
% # x: 二维矩阵，原始数据
% # statFlag: 逻辑向量，长度与x行数相等，静态标识，true表示x对应的行为静态，否则为false
% # delNonStat: 逻辑标量，true表示删除x中非静态的行，否则保留这些行，但对应的结果为0，默认为true
% # n: 标量，多项式拟合阶数，默认为2
%
% Output Arguments
% # y: 二维矩阵，列数与x列数相等，当delNonStat为true时，行数为statFlag中为true的元素个数，否则行数与x行数相等。各静止段去掉趋势项后的噪声（一个静止段指statFlag中连续为true的一段）

if nargin < 4
    n = 2;
end
if nargin < 3
    delNonStat = true;
end

assert(length(statFlag)==size(x, 1));
nStatic = nnz(int8(statFlag));
if delNonStat
    y = NaN(nStatic, size(x, 2));
    nCurElem = 0;
else
    y = zeros(size(x));
end
[statSectBgnIdx, statSectEndIdx, statSectNum] = findlargemagsects(double(statFlag(:)), 0, 0.5, 0, 0, 0, false);
for j=1:statSectNum
    xInd = statSectBgnIdx(j):statSectEndIdx(j);
    xFit = xInd - statSectBgnIdx(j) + 1;
    if delNonStat
        nNextElem = nCurElem + length(xInd);
        yInd = (nCurElem+1):(nNextElem);
        nCurElem = nNextElem;
    else
        yInd = xInd;
    end
    for i=1:size(x, 2)
        [p, ~, mu] = polyfit(xFit', x(xInd, i), n); % NOTE: 居中并缩放以提高数值精度
        y(yInd, i) = x(xInd, i)-polyval(p, (xFit-mu(1))/mu(2))';
    end
end
end