function [D] = circdiff(A, n, dim)
%CIRCDIFF 计算A数组沿dim维的n次差值，按循环做差方式
%
% Input Arguments
% # A, n, dim: 见内置函数diff的帮助说明
%
% Output Arguments
% D: 尺寸与A相同的数组，沿dim维的n次差值，做差前将A的dim维数前面n个子数组复制到dim维最后，以实现循环做差

if nargin < 3
    dim = 1;
end
if nargin < 2
    n = 1;
end

expSize = zeros(1, ndims(A));
expSize(1, dim) = n;
dimSize = size(A, dim);
A = expand(A, expSize);
A(partsub2ind(size(A), dim, dimSize+(1:n))) = A(partsub2ind(size(A), dim, 1:n));
D = diff(A, n, dim);
end