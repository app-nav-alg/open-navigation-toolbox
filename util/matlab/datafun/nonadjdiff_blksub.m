function [ Y ] = nonadjdiff_blksub( X, dist ) %#codegen
%NONADJDIFF_BLKSUB 计算二维矩阵沿列方向上序号相差固定值的元素之差，采用整块相减的方法
%
% Input Arguments:
% X: 二维矩阵
% dist: 标量，序号差值
%
% Output Arguments:
% Y: 二维矩阵，行数比X少dist，列数与X相同

m = size(X, 1);
Y = X(dist+1:m, :) - X(1:m-dist, :);
end