function [B] = finitemean(A) %#codegen
%FINITEMEAN 计算矩阵A中各列有限值元素的平均值
%
% Input Arguments:
% A: 矩阵
%
% Output Arguments:
% B: 行向量，长度与A的列数相同，各元素为A对应列有限值（非NaN及Inf）元素的平均值

n = size(A, 2);
ind = isfinite(A);
A(~ind) = 0;
B = NaN(1, n);
for j=1:n
    B(:, j) = sum(A(:, j)) / nnz(ind(:, j));
end
end