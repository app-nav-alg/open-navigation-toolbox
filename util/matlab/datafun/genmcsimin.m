function [MCSimIn] = genmcsimin(par, nSim)
%GENMCSIMIN 根据参数表自动生成mcsim函数in输入参数中的其它域
%
% Tips
% # 本函数判断多次仿真时的参数分布类型的规则是：
%     1、如果第3列非NaN且为正数，则为单边均匀分布；
%     2、如果第3列非NaN且为负数，则为对称均匀分布；
%     3、如果par.refVal第2列非NaN，则为正态分布；
%     4、如果以上均不符合，则为正全值分布，且发出警告。
%
% Input Arguments
% # par: 仿真参数表
% # nSim: 标量，仿真次数
%
% Output Arguments
% # MCSimIn: mcsim函数的in输入参数，包含以下域：
%     grpParIdx, grpParDistType, grpSimNum: 前n（n为仿真参数个数）组为包含单个参数的单次仿真，后n组为刨除单个参数的nSim次仿真，最后1组为包含所有参数的nSim次仿真。多次仿真时，每个参数的分布类型由par.refVal中非NaN的数值确定。
%     par: par输入参数

nPar = size(par, 1);
MCSimIn.grpParIdx{nPar*2+1, 1} = (1:nPar)';
MCSimIn.grpParDistType{nPar*2+1, 1} = ones(nPar, 1);
refValCol = par.refVal(:, 3);
MCSimIn.grpParDistType{nPar*2+1, 1}(~isnan(refValCol)&(refValCol>0), 1) = 3;
MCSimIn.grpParDistType{nPar*2+1, 1}(~isnan(refValCol)&(refValCol<0), 1) = -3;
MCSimIn.grpParDistType{nPar*2+1, 1}(~isnan(par.refVal(:, 2)), 1) = 2;
if any(MCSimIn.grpParDistType{nPar*2+1, 1} == 1)
    warning('genmcsimin:unfitdisttype', 'Distribution type is neither normal nor uniform in a multi-time simulation.');
end
MCSimIn.grpSimNum(nPar*2+1, 1) = nSim;
for i=1:nPar
    MCSimIn.grpParIdx{i, 1} = i;
    MCSimIn.grpParIdx{i+nPar, 1} = setdiff(1:nPar, i)';
    MCSimIn.grpParDistType{i, 1} = 1;
    MCSimIn.grpParDistType{i+nPar, 1} = MCSimIn.grpParDistType{nPar*2+1, 1}(setdiff(1:nPar, i));
    MCSimIn.grpSimNum(i, 1) = 1;
    MCSimIn.grpSimNum(i+nPar, 1) = nSim;
end
MCSimIn.par = par;
end