function [ B ] = rms_cg( A, dim ) %#codegen
%RMS_CG 计算矩阵在指定维上的均方根（root mean square）
%
% Input Arguments:
% A: 矩阵
% dim: 标量，指定计算所沿的维数

B = (mean(A.^2, dim)).^(1/2);
end