function C = min_abs(A, B, NaNFlag)
%MIN_ABS 计算A、B中绝对值较小的元素，返回带符号的最小值
%
% Input Arguments
% # A, B, NaNFlag: 见内置函数min的帮助说明
%
% Output Arguments
% # C: 尺寸与A、B相同的数组，各元素为A、B中绝对值较小的那一个（带符号）

AAbs = abs(A);
BAbs = abs(B);
if nargin < 3
    CAbs = min(AAbs, BAbs);
else
    CAbs = min(AAbs, BAbs, NaNFlag);
end
C = sign(A) .* CAbs;
CFromBInd = (CAbs == BAbs);
C(CFromBInd) = sign(B(CFromBInd)) .* CAbs(CFromBInd);
end