function [g, ID] = findgroups_consec(v)
%FINDGROUPS_CONSEC 对向量v的元素分组，相同且连续的元素记为一组，返回组序号g
%   此处显示详细说明

g = ones(size(v));
ID = v;
nGroups = 1;
for i=2:length(v)
    if isequal(v(i), v(i-1))
        g(i) = g(i-1);
    else
        g(i) = g(i-1) + 1;
        nGroups = nGroups + 1;
        ID(nGroups) = v(i);
    end
end
ID((nGroups+1):end) = [];