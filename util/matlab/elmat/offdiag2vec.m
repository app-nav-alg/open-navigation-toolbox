function [ v ] = offdiag2vec( A, rowMajor, triangle ) %#codegen
% OFFDIAG2VEC 将矩阵的非对角线元素放入一个列向量内
%
% Input Arguments
% # A: 数组
% # rowMajor: 逻辑标量，true表示按A的行取非对角线元素，false表示按列取，默认为true
% # triangle: 标量，大于0表示取上三角，小于0表示取下三角，等于0表示取所有非对角线元素，默认为0
%
% Output Arguments
% # v: 当A为矩阵时为列向量，当A为维数大于2的数组时为比A维数小1的数组，第一维尺寸为A非对角线元素个数，后续维尺寸与A第2维之后维度尺寸相同
%
% Assumptions and Limitations
% # 当A维数大于2时不支持代码生成

if nargin < 3
    triangle = 0;
end
if nargin < 2
    rowMajor = true;
end

if rowMajor
    B = A;
else
    B = transpose_nd(A);
    triangle = -triangle;
end
sz = size(B);
count = 0;
if ndims(A) > 2 %#ok<ISMAT>
    ind_2plus = arrayfun(@(x)(1:x), sz(1, 3:end), 'UniformOutput', false);
    sz_2plus = num2cell(sz(1, 3:end));
    sz1 = sz(1, 1)*sz(1, 2)-min(sz(1, 1:2));
    if triangle ~= 0
        sz1 = sz1 / 2;
    end
    v = NaN(sz1, sz_2plus{:});
    for i=1:sz(1, 1)
        for j=1:sz(1, 2)
            if triangle == 0
                expr = (i ~= j);
            elseif triangle > 0
                expr = (i < j);
            else
                expr = (i > j);
            end
            if expr
                count = count + 1;
                v(count, ind_2plus{:}) = B(i, j, ind_2plus{:});
            end
        end
    end
else
    sz1 = numel(B)-min(sz);
    if triangle ~= 0
        sz1 = sz1 / 2;
    end
    v = NaN(sz1, 1);
    for i=1:sz(1, 1)
        for j=1:sz(1, 2)
            if triangle == 0
                expr = (i ~= j);
            elseif triangle > 0
                expr = (i < j);
            else
                expr = (i > j);
            end
            if expr
                count = count + 1;
                v(count, 1) = B(i, j);
            end
        end
    end
end
end