function [ A ] = horzexpand_byref( A, sz ) %#codegen
%HORZEXPAND_BYREF 在第2维上扩展矩阵的尺寸
%
% Input Arguments:
% A: 矩阵
% sz: 标量，第2维的目标尺寸

catsize = size(A);
catsize(1，2) = sz - size(A, 2);
A = horzcat(A, zeros(catsize));
end
