function [A] = vec2tril(v, K, sz, otherEleVal, rowMajor) %#codegen
%VEC2TRIL 将向量各元素依次填入矩阵的下三角部分中
%
% Input Arguments:
% v: 向量
% K: 标量，指定填入元素位于矩阵的K对角线及以下部分，K=0表示主对角线，K>0表示主对角线上的第K条对角线，K<0表示主对角线下的第-K条对角线，默认为0
% sz: 1*2向量，目标矩阵大小，默认为[3 3]
% otherEleVal: 目标矩阵没有填及的元素值，默认为0
% rowMajor: 逻辑标量，true表示按A的行放置元素，false表示按列放置，默认为true
%
% Output Arguments:
% A: 二维矩阵

if nargin < 5
    rowMajor = true;
end
if nargin < 4
    otherEleVal = 0;
end
if nargin < 3
    sz = [3 3];
end
if nargin < 2
    K = 0;
end

A = otherEleVal * ones(sz);
count = 0;
if rowMajor
    iMax = sz(1, 1); % 行
else
    iMax = sz(1, 2); % 列
end
for i=1:iMax
    if rowMajor
        jMin = 1; % 列
        jMax = min(i+K, sz(1, 2));
    else
        jMin = max(i-K, 1); % 行
        jMax = sz(1, 1);
    end
    for j=jMin:jMax
        count = count + 1;
        if count > length(v)
            return;
        end
        if rowMajor
            A(i, j) = v(count); % NOTE: 使用线性索引
        else
            A(j, i) = v(count); % NOTE: 使用线性索引
        end
    end
end
end