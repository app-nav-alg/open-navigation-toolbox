function [vExp] = expandoddinterval(v, halfInc)
%EXPANDODDINTERVAL 增加向量奇数间隔长度
%
% Input Arguments:
% v: 向量
% halfInc: 标量，扩展值
%
% Output Arguments:
% vExp: 向量，尺寸与v相同，由v将奇数序号的元素减去halfInc，将偶数序号的元素加上halfInc得到

vExp = NaN(size(v));
for i=1:length(v)
    if mod(i, 2) ~= 0
        vExp(i) = v(i) - halfInc;
    else
        vExp(i) = v(i) + halfInc;
    end
end
end