function [ B, equalRowIdx, equalColIdx ] = stripequalrowcol( A, E, dim ) %#codegen
%STRIPEQUALROWCOL 去除矩阵中对应位置元素与另一个矩阵全相等的行和列
%
% Input Arguments:
% A: 二维矩阵
% E: 尺寸与A相同的二维矩阵
% dim: 标量，1表示仅去掉行，2表示仅去掉列，其它表示同时去掉行和列，默认为0
%
% Output Arguments:
% B: 二维矩阵，去除矩阵A中对应位置元素与矩阵E全相等的行和列得到的矩阵
% equalRowIdx: 行向量，矩阵A中对应位置元素与矩阵E全相等的行的序号（相对于A矩阵而言，以1为基准）
% equalColIdx: 行向量，矩阵A中对应位置元素与矩阵E全相等的列的序号（相对于A矩阵而言，以1为基准）

if nargin < 3
    dim = 0;
end

isEqual = eq(A, E); % 可以将sym类型的元素转换为logical类型
B = A;
equalRowIdx = zeros(1, 0);
equalColIdx = zeros(1, 0);
coder.varsize('B');
coder.varsize('equalRowIdx', 'equalColIdx', [1, Inf], [0 1]);
if dim ~= 2
    isRowEqual = all(isEqual, 2)';
    equalRowIdx = find(isRowEqual);
    B(equalRowIdx, :) = [];
end
if dim ~= 1
    isColEqual = all(isEqual, 1);
    equalColIdx = find(isColEqual);
    B(:, equalColIdx) = [];
end
end