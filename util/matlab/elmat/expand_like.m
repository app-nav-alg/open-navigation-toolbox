function [B] = expand_like(A, MLike, expElement) %#codegen % TODO: 改为resize，包括缩小尺寸
%EXPAND_LIKE 扩展矩阵A的尺寸至MLike
%
% Input Arguments
% # A: 矩阵
% # MLike: 维数与A相同的矩阵，A对应的维数尺寸小于MLike的将扩展到与MLike该维尺寸相同
% # expElement: 标量，指定扩展元素的值，默认为NaN
%
% Output Arguments
% # B: 矩阵

if nargin < 3
    expElement = NaN;
end

B = expand(A, size(MLike)-size(A), expElement);
end