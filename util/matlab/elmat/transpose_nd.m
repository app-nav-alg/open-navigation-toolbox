function [ Y ] = transpose_nd( X )
%TRANSPOSE_ND 取多维矩阵前2维的转置
%
% Input Arguments:
% X: 多维矩阵
%
% Output Arguments:
% Y: 多维矩阵，维数与X相同，前两维为X前两维的转置

order = 1:ndims(X);
order(1) = 2;
order(2) = 1;
Y = permute(X, order);
end