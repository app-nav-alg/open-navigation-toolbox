function [ B ] = vertinsert( A, rowIndex, val ) %#codegen
%VERTINSERT 在矩阵指定的各行插入元素全部为指定值的行
%
% Input Arguments:
% A: 二维矩阵
% rowIndex: 此向量指定插入各行的序号（相对于插入后形成的矩阵而言，以1为基准），重复的元素被忽略
% val: 标量，插入行的元素值
%
% Output Arguments:
% B: 二维矩阵，列数与A相同，行数比A大rowIndex的长度

% 忽略重复的元素
rowIndex = unique(rowIndex);

% 初始化全零矩阵，并预置最终尺寸
B = zeros(size(A, 1) + length(rowIndex), size(A, 2));

% 按指定值的行进行赋值
flag = zeros(size(B, 1), 1);
for i=1:length(rowIndex)
    B(rowIndex(i), :) = ones(1, size(A, 2)) * val;
    flag(rowIndex(i)) = 1;
end

% 对其它行进行赋值
k = 1;
for i=1:size(B, 1)
    if flag(i) == 0
        B(i, :) = A(k, :);
        k = k + 1;
    end
end
end