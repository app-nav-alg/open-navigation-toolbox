function [ A ] = vec2offdiag( v, sz, otherElemVal, rowMajor ) %#codegen
%VEC2OFFDIAG 将向量各元素依次填入矩阵的非对角线元素内
%
% Input Arguments:
% v: 向量或二维矩阵，当为二维矩阵时，将按行进行转换
% sz: 1*2向量，目标矩阵大小，默认为[3 3]
% otherElemVal: 目标矩阵的其它元素（包括对角线及没有填及的）值，默认为0
% rowMajor: 逻辑标量，true表示按A的行放置元素，false表示按列放置，默认为true
%
% Output Arguments:
% A: 矩阵，当v为向量时，A为二维矩阵，当v为二维矩阵时，A为三维矩阵，第3维尺寸与v行数相同

if iscolumn(v)
    vReshaped = v';
else
    vReshaped = v;
end
[m, n] = size(vReshaped);
if nargin < 4
    rowMajor = true;
end
if nargin < 3 || isempty(otherElemVal)
    otherElemVal = 0;
end
if nargin < 2 || isempty(sz)
    sz = [3 3 m];
end

A = otherElemVal * ones(sz, class(v));
count = 0;
if rowMajor
    iMax = sz(1, 1); % 行
    jMax = sz(1, 2); % 列
else
    iMax = sz(1, 2); % 列
    jMax = sz(1, 1); % 行
end
for i=1:iMax
    for j=1:jMax
        if i ~= j
            count = count + 1;
            if count > n
                return;
            end
            if rowMajor
                A(i, j, :) = vReshaped(:, count);
            else
                A(j, i, :) = vReshaped(:, count);
            end
        end
    end
end
end