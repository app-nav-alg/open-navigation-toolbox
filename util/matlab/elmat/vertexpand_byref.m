function [ A ] = vertexpand_byref( A, sz ) %#codegen
%VERTEXPAND_BYREF 在第1维上扩展矩阵的尺寸
%
% Input Arguments:
% A: 矩阵
% sz: 标量，第1维的目标尺寸

catsize = size(A);
catsize(1, 1) = sz - size(A, 1);
A = vertcat(A, zeros(catsize));
end
