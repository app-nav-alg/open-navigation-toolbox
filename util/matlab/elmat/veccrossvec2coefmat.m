function [M] = veccrossvec2coefmat(a, b) %#codegen
%VECCROSSVEC2COEFMAT 将a×(b×c)转换为c的系数矩阵形式
%
% Input Arguments
% # a: 3*1向量
% # b: 3*1向量
%
% Output Arguments
% # M: 3*3矩阵，a×(b×c)=Mc
%
% References:
% # 《应用导航算法工程基础》“三重积展开为系数矩阵形式”

M = repmat(b, 1, 3)*diag(a) - (a'*b)*eye(3);
end