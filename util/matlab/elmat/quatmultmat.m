function [M] = quatmultmat(q, conjugate)
%QUATMULTMAT 计算代表四元数乘法的矩阵
% Input Arguments
% # q: 长度为4的向量，四元数
% # conjugate: 逻辑标量，true表示输出(q?*)，否则输出(q?)，默认为false
%
% Output Arguments
% # M: 4*4矩阵，代表四元数乘法的矩阵
%
% References
% # 《应用导航算法工程基础》 “四元数乘法及其性质”

if nargin < 2
    conjugate = false;
end

q = q(:);
if conjugate
    M = [q(1) -q(2:4)'; q(2:4) q(1)*eye(3)-skewsymmat(q(2:4))];
else
    M = [q(1) -q(2:4)'; q(2:4) q(1)*eye(3)+skewsymmat(q(2:4))];
end
end