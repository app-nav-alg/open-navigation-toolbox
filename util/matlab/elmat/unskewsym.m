function [vec] = unskewsym(mat) %#codegen
%UNSKEWSYM 由反对称矩阵计算向量
%
% Input Arguments:
% mat: 3*3矩阵
%
% Output Arguments:
% vec: 3*1向量

vec = [(mat(3, 2)-mat(2, 3))/2; (mat(1, 3)-mat(3, 1))/2; (mat(2, 1)-mat(1, 2))/2];
end