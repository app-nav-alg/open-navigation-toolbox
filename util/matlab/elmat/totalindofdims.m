function [totalIndCell] = totalindofdims(A)
%TOTALINDOFDIMS 矩阵指定维的所有元素的下标索引
%   此处显示详细说明

if nargin < 2
    dims = 1:ndims(A);
end

sz = size(A);
totalIndCell = arrayfun(@(i)(1:sz(i)), dims, 'UniformOutput', false);
end