function [ B ] = offdiag( A )
%OFFDIAG 求矩阵对角线元素置零后得到的矩阵
%
% Input Arguments:
% A: 二维或三维矩阵
%
% Output Arguments:
% B: 二维或三维矩阵，尺寸与A相同

B = A;
[m, n, p] = size(A);
for i=1:p
    for j=1:min(m, n)
        B(j, j, i) = 0;
    end
end
end