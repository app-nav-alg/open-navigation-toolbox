function [ B ] = sectprodm( A, sectInd, largerIndLeftMult ) %#codegen
%SECTPRODM 三维矩阵的前两维分段连乘
%
% Input Arguments:
% A: n*n*p矩阵
% sectInd: 长度为s向量，除最后一个元素外的其它元素为各段在A第3维中的起始序号，最后一个元素为最后一段的结束序号+1，默认为[1 p+1]（即将A的所有页分为一段）
% largerIndLeftMult: 逻辑标量，true表示序号较大的n*n矩阵左乘以连乘积，false表示右乘，默认为true
%
% Output Arguments:
% B: n*n*(s-1)矩阵，前两维的矩阵为A的前两维中对应段的矩阵连乘得到

if nargin < 3
    largerIndLeftMult = true;
end

n = size(A, 1);
p = size(A, 3);
if nargin < 2
    sectInd = [1 p+1];
end
s = length(sectInd);
B = NaN([n, n, s-1]);
j = 0;
for i=1:p
    if (j<(s-1)) && (i==sectInd(j+1)) % NOTE: 使用线性索引
        j = j + 1;
        B(:, :, j) = A(:, :, i);
    elseif (j>0) && (i<sectInd(j+1)) % NOTE: 使用线性索引
        if largerIndLeftMult
            B(:, :, j) = A(:, :, i) * B(:, :, j);
        else
            B(:, :, j) = B(:, :, j) * A(:, :, i);
        end
    end
end
end