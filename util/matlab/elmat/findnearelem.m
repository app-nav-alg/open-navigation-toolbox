function [ ind1 ] = findnearelem( v1, v2, doInputCheck ) %#codegen
%FINDNEARELEM 找出向量v1中与v2各元素最接近的元素的序号
%
% Input Arguments
% # v1, v2: 向量，应同为单调递增或单调递减的
% # doInputCheck: 逻辑标量，true表示检查v1与v2的单调递增（递减）性，否则不检查，若不检查，返回的结果可能仅是局部符合的
%
% Output Arguments
% # ind1: 向量，尺寸与v2相等，v1的第ind1(i)个元素与v2的第i个元素最为接近

if nargin < 3
    doInputCheck = true;
end

if doInputCheck
    if ~issorted(v1) || ~issorted(v2) || length(unique(v1(:)'))~=length(v1) || length(unique(v2(:)'))~=length(v2)
        error('findnearelem:notsortedorunique', 'Input vector(s) is not sorted or contains repeated element(s).');
    end
end

% NOTE: 本函数使用线性索引
len1 = length(v1);
len2 = length(v2);
ind1 = NaN(size(v2));
v1Ind = 1;
for i=1:len2
    while (v1Ind<len1) && (abs(v2(i)-v1(v1Ind)) > abs(v2(i)-v1(v1Ind+1)))
        v1Ind = v1Ind + 1;
    end
    ind1(i) = v1Ind;
end
end