function [ B ] = horzinsert( A, colIndex, val ) %#codegen
%HORZINSERT 在矩阵指定的各列插入元素全部为指定值的列
%
% Input Arguments:
% A: 二维矩阵
% colIndex: 此向量指定插入各列的序号（相对于插入后形成的矩阵而言，以1为基准），重复的元素被忽略
% val: 标量，插入列的元素值
%
% Output Arguments:
% B: 二维矩阵，行数与A相同，列数比A大colIndex的长度

colIndex = unique(colIndex);
B = A;
coder.varsize('B');
for i=1:length(colIndex)
    if colIndex(i) == 1
        M = NaN(size(A, 1), 0);
    else
        M = B(:, 1:(colIndex(i)-1));
    end
    B = horzcat(M, ones(size(A, 1), 1) * val, B(:, colIndex(i):end));
end
end