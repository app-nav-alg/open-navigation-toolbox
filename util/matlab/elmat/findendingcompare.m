function [ ind ] = findendingcompare( v, comparator, relatOp, firstOrLast )
%FINDENDINGCOMPARE 寻找向量中第一个或最后一个与某个值有关（大于、小于等）的元素的索引
%
% Input Arguments:
% v: 向量，各元素与comparator比较的向量
% comparator: 标量，比较值
% relatOp: 关系操作符函数句柄，可以是eq、gt、lt、ne、ge、le等
% firstOrLast: 'first'表示第一个，'last'表示最后一个，默认为'first'
%
% Output Arguments:
% ind: 标量或空，v中与comparator比较符合relatOp为真的第一个或最后一个元素的序号，未找到为空

if nargin < 4
    firstOrLast = 'first';
end

x = relatOp(v, comparator);
ind = find(x, 1, firstOrLast);
end