function [ A, ind1, ind2 ] = replcommsamp( A1, v1, A2, v2, tol )
%REPLCOMMSAMP 将矩阵A1中最后一维序号为向量v1中与v2相等的元素序号值的块替换为A2中相应的值
%
% Input Arguments:
% A1: 矩阵，维数与A2相同，最后一维尺寸与v1长度相同，除最后一维外的其它维尺寸与A2相同，被替换者
% v1: 向量
% A2: 矩阵，最后一维尺寸与v2长度相同，替换者
% v2: 向量
% tol: 认为v1与v2元素相同的阈值，默认为eps
%
% Output Arguments:
% A: 矩阵，尺寸与A1相同，最后一维序号为ind1的块与A2中最后一维序号为ind2的块相同，否则与A1中对应的块相同
% ind1, ind2: 向量，v1与v2中相等元素在各自向量中的序号，详细参见findeqelem函数帮助

if nargin < 5
    tol = eps;
end

% NOTE: 本函数使用线性索引
[ ind1, ind2 ] = findeqelem( v1, v2, tol );
A = A1;
sz1 = size(A1);
sz2 = size(A2);
blkNumElem1 = prod(sz1(1, 1:(end-1)));
blkNumElem2 = prod(sz2(1, 1:(end-1)));
for i=1:length(ind1)
    indBgn1 = blkNumElem1*(ind1(i)-1)+1;
    indEnd1 = blkNumElem1*ind1(i);
    indBgn2 = blkNumElem2*(ind2(i)-1)+1;
    indEnd2 = blkNumElem2*ind2(i);
    A(indBgn1:indEnd1) = A2(indBgn2:indEnd2);
end
end