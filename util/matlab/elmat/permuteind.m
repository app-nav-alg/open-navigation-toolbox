function [ B ] = permuteind( A, index, dim ) %#codegen
%PERMUTEIND 对矩阵指定维数的索引重新排序
%
% Input Arguments:
% A: 矩阵
% index: 向量，长度与A的dim维尺寸相同
% dim: 标量，指定需要重新排序的维数，默认为1（对行操作）
%
% Output Arguments:
% B: 尺寸与A相同的矩阵，第dim维第i个索引的内容与A的第dim维第index(i)个索引的内容相同

if nargin < 3
    dim = 1;
end

% NOTE: 本函数使用了线性索引
% 将待排序的维数移至最后一维以便排序操作
N = ndims(A);
dimOrder = [1:(dim-1) (dim+1):N dim];
ASD = permute(A, dimOrder);

% 对最后一维索引重新排序
arraySize = size(ASD);
blockSize = prod(arraySize(1:(end-1)));
B = ASD;
if length(index) ~= arraySize(end)
    error('permuteind:invalidIndexLength', 'Length of index is not equal to the size of dim of A.');
end
for i=1:arraySize(end)
    bgnIndA = blockSize * (index(i)-1) + 1;
    endIndA = blockSize * index(i);
    bgnIndB = blockSize * (i-1) + 1;
    endIndB = blockSize * i;
    B(bgnIndB:endIndB) = ASD(bgnIndA:endIndA); % NOTE: 假定了column-major的线性索引方式
end

% 将最后一维换回指定维数
dimOrder = [1:(dim-1) N dim:(N-1)];
B = permute(B, dimOrder);
end