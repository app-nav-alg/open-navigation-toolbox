function [ B ] = lastdimsubarray(A, lastDimInd) %#codegen
%LASTDIMSUBARRAY 提取数组最后一维指定索引的子数组
%
% Input Arguments:
% A: 数组
% ind: 向量，A最后一维的索引
%
% Output Arguments:
% B: 数组，最后一维尺寸与ind长度相同，除最后一维外尺寸与A相同
%
% Assumptions and Limitations:
% 1. 假设MATLAB使用列优先索引

% NOTE: 本函数使用线性索引
szA = size(A);
szExclLastDim = szA(1, 1:(end-1));
nElemExclLastDim = prod(szExclLastDim);
szLastDim = length(lastDimInd);

B = NaN([szExclLastDim szLastDim]);
for i=1:szLastDim
    linearIndA = ((lastDimInd(i)-1)*nElemExclLastDim+1):(lastDimInd(i)*nElemExclLastDim);
    linearIndB = ((i-1)*nElemExclLastDim+1):(i*nElemExclLastDim);
    B(linearIndB) = A(linearIndA);
end