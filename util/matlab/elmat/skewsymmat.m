function [mat] = skewsymmat( vec ) %#codegen
%SKEWSYMMAT 计算向量的反对称矩阵
%
% Input Arguments:
% vec: 长度为3的向量
%
% Output Arguments:
% mat: 3*3矩阵

mat = [0, -vec(3), vec(2); vec(3), 0, -vec(1); -vec(2), vec(1), 0];
end