function [ B ] = expand( A, expSize, expElement ) %#codegen
% EXPAND 在各维数上扩展矩阵的尺寸
%
% Input Arguments:
% A: 矩阵
% expSize: 长度为A的维数的非负向量，指定A对应的维数需要扩展的尺寸大小，小于0的元素将被替换为0，默认全为1
% expElement: 标量，指定扩展元素的值，默认为NaN
%
% Output Arguments:
% B: 矩阵，各维的尺寸比A大expSize
%
% Assumptions and Limitations:
% 1. 代码生成仅支持二维矩阵

if nargin < 3
    expElement = NaN;
end
if nargin < 2
    expSize = ones(1, ndims(A));
end

expSize(expSize<0) = 0;
if ismatrix(A) % 对二维矩阵单独处理以支持代码生成
    [m, n] = size(A);
    B = horzcat(vertcat(A, expElement*ones(expSize(1), n)), expElement*ones(m+expSize(1), expSize(2))); %NOTE:使用线性索引
else
    B = A;
    for i=1:ndims(A)
        szCat = size(B);
        szCat(1, i) = expSize(i); %NOTE:使用线性索引
        C = expElement * ones(szCat);
        B = cat(i, B, C);
    end
end