function [ K ] = commutmat( p, m )
%COMMUTMAT 计算交换矩阵
%
% Input Arguments
% m, n: 标量，矩阵行、列数
%
% Output Arguments
% K: 行数及列数均为m*n的方阵，对于任意m*n矩阵A，有K*vec(A)=vec(A')
%
% References:
% # 《应用导航算法工程基础》“矩阵的Kronecker乘积、向量化处理（vec操作符）及交换矩阵”

K = zeros(p*m);
for i=1:p
    for j=1:m
        epi = zeros(p, 1);
        epi(i) = 1;
        emj = zeros(m, 1);
        emj(j) = 1;
        K = K + kron(epi*(emj'), emj*(epi'));
    end
end
end