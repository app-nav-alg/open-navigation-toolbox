function [] = plot2dmat_sp(A, figNum, figName, hFig, subPlotDim)
%PLOT2DMAT_SP 按subPlotDim维绘制二维矩阵曲线
%
% Input Arguments:
% A: m*n二维矩阵
% figNum: 绘图的图数，默认为1
% figName: 绘图的名称，默认为空
% hFig: 第一幅绘图的句柄，若不输入将新建绘图
% subPlotDim: 标量，该维数上的每一项占用一副子图，默认为2（每列占用一幅子图，自变量沿行变化）

if nargin < 5
    subPlotDim = 2;
end
if nargin < 4
    hFig = [];
end
if nargin < 3
    figName = '矩阵';
end
if nargin < 2
    figNum = 1;
end

assert(ismatrix(A));
pointDim = setdiff(1:2, subPlotDim);
A = permute(A, [pointDim, subPlotDim]);
[~, n] = size(A);
subPlotPerFig = ceil(n/figNum);
[subPlotRowNum, subPlotColNum] = roundsqrt(subPlotPerFig);
dimSpecs = {'行', '列'};

postFix = [];
for k = 1:figNum % 画k张图
    if figNum > 1
        postFix = ['-' int2str(k)];
    end
    preparefig([figName postFix], hFig-1+k);
    if k < figNum
        endColIndex = k*subPlotPerFig;
    else
        endColIndex = n;
    end
    for j = (1+(k-1)*subPlotPerFig):endColIndex % j为矩阵列数
        subplot(subPlotRowNum, subPlotColNum, j-(k-1)*subPlotPerFig);
        plot(A(:, j));
        title(['第' int2str(j) dimSpecs{subPlotDim}]);
        grid on;
    end
end
end