function [] = plot3dmat_sp(A, showLegend, figNum, figName, hFig, subPlotDim, plotDim)
%PLOT3DMAT_SP 绘制三维矩阵曲线
%
% Input Arguments:
% A: m*n*p三维矩阵
% showLegend: 逻辑标量，true表示显示图例，默认为false
% figNum: 绘图的图数，默认为1
% figName: 绘图的名称，默认为空
% hFig: 第一幅绘图的句柄，若不输入将新建绘图
% subPlotDim: 标量，该维数上的每一索引占用一副子图，默认为2（每列占用一幅子图）
% plotDim: 标量，该维数上的每一索引占用一条曲线，默认为1（自变量沿第三维变化）

if nargin < 7
    plotDim = 1;
end
if nargin < 6
    subPlotDim = 2;
end
if nargin < 5
    hFig = [];
end
if nargin < 4
    figName = '矩阵';
end
if nargin < 3
    figNum = 1;
end
if nargin < 2
    showLegend = false;
end

pointDim = setdiff(1:3, [subPlotDim, plotDim]);
A = permute(A, [plotDim, subPlotDim, pointDim]);
[m, n, p] = size(A);
subPlotPerFig = ceil(n/figNum);
[subPlotRowNum, subPlotColNum] = roundsqrt(subPlotPerFig);

colorSpecs = {'b', 'g', 'r', 'c', 'm', 'y', 'k'};
colorSpecsLen = length(colorSpecs);
dimSpecs = {'行', '列', '页'};

postFix = [];
for k = 1:figNum % 画k张图
    if figNum > 1
        postFix = ['-' int2str(k)];
    end
    preparefig([figName postFix], hFig-1+k);
    if k < figNum
        endColIndex = k*subPlotPerFig;
    else
        endColIndex = n;
    end
    for j = (1+(k-1)*subPlotPerFig):endColIndex % j为矩阵列数
        subplot(subPlotRowNum, subPlotColNum, j-(k-1)*subPlotPerFig);
        for i = 1:m
            plot(1:p, squeeze(A(i, j, :)), 'DisplayName', ['第' int2str(i) dimSpecs{plotDim}], 'Color', colorSpecs{1, mod(i, colorSpecsLen)+1});
            if showLegend
                legend('show', 'Location', 'Best');
            end
            hold on;
        end
        title(['第' int2str(j) dimSpecs{subPlotDim}]);
        grid on;
    end
end
end