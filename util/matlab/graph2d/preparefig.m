function [hFig] = preparefig( figName, hFig, clearFig )
%PREPAREFIG 预备绘图
%
% input arguments:
% figName: 绘图的名称，默认为空
% hFig: 绘图的句柄，若不输入或为空将新建绘图
% clearFig: 逻辑标量，为true时将清空hFig指定的绘图，默认为true
%
% output arguments:
% hFig: 绘图的句柄

if nargin < 1
    figName = '';
end
if nargin < 2
    hFig = [];
end
if nargin < 3
    clearFig = true;
end

if ~isempty(hFig)
    figure(hFig);
    if clearFig
        clf;
    end
    if isempty(getCurrentWorker()) && ~isunix % NOTE: unix不支持docked
        set(hFig, 'WindowStyle', 'docked', 'Name', figName);
    else
        set(hFig, 'Name', figName); % 停泊窗口样式在并行计算模式下可能出错
    end
else
    if isempty(getCurrentWorker()) && ~isunix
        hFig = figure('WindowStyle', 'docked', 'Name', figName);
    else
        hFig = figure('Name', figName); % 停泊窗口样式在并行计算模式下可能出错
    end
end
end