function [] = plot3dsymmat_sp(A, plotDiag, showLegend, figNum,  figName, hFig)
%PLOT3DSYMMAT_SP 绘制前两维为对称矩阵的三维矩阵曲线，自变量沿第三维变化
%
% Input Arguments:
% A: m*m*p三维矩阵，其中每一页二维矩阵为对称阵
% plotDiag: 逻辑标量，true表示绘制对角线元素，默认为true
% showLegend: 逻辑标量，true表示显示图例，默认为false
% figNum: 绘图的图数，默认为1
% figName: 绘图的名称，默认为空
% hFig: 绘图的句柄，若不输入将新建绘图

if nargin < 6
    hFig = [];
end
if nargin < 5
    figName = '对称矩阵';
end
if nargin < 4
    figNum = 1;
end
if nargin < 3
    showLegend = false;
end
if nargin < 2
    plotDiag = true;
end

[m, ~, p] = size(A);
subPlotPerFig = ceil(m/figNum);
[subPlotRowNum, subPlotColNum] = roundsqrt(subPlotPerFig);

colorSpecs = {'b', 'g', 'r', 'c', 'm', 'y', 'k'};
colorSpecsLen = length(colorSpecs);

if mod(m, 2) == 0  % m为偶数
    for k = 1:figNum % 画k张图
        preparefig([figName '-' int2str(k)], hFig);
        if k < figNum
            endColIndex = k*subPlotPerFig;
        else
            endColIndex = m;
        end
        for i = (1+(k-1)*subPlotPerFig):endColIndex % i为矩阵列数
            subplot(subPlotRowNum, subPlotColNum, i-(k-1)*subPlotPerFig);
            if (i < (m+2)/2) && (mod(i, 2) ~= 0) % 奇数列
                plotsymmatcolelem(1:p, i, i:(m/2+i));
            elseif (i < (m+2)/2) && (mod(i, 2) == 0) % 偶数列
                plotsymmatcolelem(1:p, i, i:((m-2)/2+i));
            elseif i == (m+2)/2
                plotsymmatcolelem(1:p, i, i:(m/2+i-1));
            elseif (i > (m+2)/2) && (((mod(i, 2) ~= 0)&&(mod((m+2)/2, 2) == 0)) || ((mod(i, 2) == 0)&&(mod((m+2)/2, 2) ~= 0)))
                plotsymmatcolelem(1:p, i, [i:m 1:(i-m/2)]);
            else
                plotsymmatcolelem(1:p, i, [i:m 1:(i-(m+2)/2)]);
            end
        end
    end
else % m为奇数
    for k = 1:figNum % 画k张图
        preparefig([figName '-' int2str(k)], hFig);
        if k < figNum
            endColIndex = k*subPlotPerFig;
        else
            endColIndex = m;
        end
        for i = (1+(k-1)*subPlotPerFig):endColIndex % i为矩阵列数
            subplot(subPlotRowNum, subPlotColNum, i-(k-1)*subPlotPerFig);
            if i <= (m+1)/2
                plotsymmatcolelem(1:p, i, i:((m+1)/2+i-1));
            else
                plotsymmatcolelem(1:p, i, [i:m 1:((2*i-m-1)/2)]);
            end
        end
    end
end

    function [] = plotsymmatcolelem(t, colIndex, rowIndexVec)
        for rowIndex = rowIndexVec
            if plotDiag || (rowIndex~=colIndex)
                plot(t, squeeze(A(rowIndex, colIndex, :)), 'DisplayName', ['元素_' int2str(rowIndex) '_' int2str(colIndex)], 'Color', colorSpecs{1, mod(rowIndex, colorSpecsLen)+1});
                if showLegend
                    legend('show', 'Location', 'Best');
                end
                hold on;
            end
        end
        title(['第' int2str(colIndex) '列第' int2str(rowIndexVec(1, 1)) '-' int2str(rowIndexVec(1, end)) '行元素']);
        grid on;
    end
end