function [folderPath, fileName] = getdirandfilenamefrompath(filePath) % TODO: 用fileparts函数替代
%GETDIRANDFILENAMEFROMPATH 从文件路径中提取文件夹路径和文件名
%
% Input Arguments:
% fileFullPath: 字符串，文件完整路径
%
% Output Arguments:
% folderPath: 字符串，文件夹路径
% fileName: 字符串，文件名

sFileFullPath = strfind(filePath, filesep);
folderPath = filePath(1:sFileFullPath(end)-1);
fileName = filePath(sFileFullPath(end)+1:end);