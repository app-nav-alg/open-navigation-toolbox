function [ childStruct, namesAreFound ] = getchildnodebyname( parentStruct, nameChain )
%GETCHILDNODEBYNAME 查找父节点的名字为指定值的子节点
%
% Input Arguments:
% parentStruct: 结构体，格式同parsexml函数的输出
% nameChain: 元胞向量，为需要查找的节点在parentStruct中各层的名字
% Output Arguments:
% childStruct: 目标结构体，如果父结构体中包含同名的子结构体，则只返回第一个
% namesAreFound: 逻辑标量，true表示找到目标结构体

% NOTE: 本函数使用线型索引
nameChainInd = 1;
childStruct = parentStruct;
while nameChainInd <= length(nameChain)
    [childStruct, nameIsFound] = getchild(childStruct, nameChain(nameChainInd));
    if nameIsFound
        nameChainInd = nameChainInd + 1;
    else
        break;
    end
end
namesAreFound = (nameChainInd == (length(nameChain)+1));
end

function [s, nameIsFound] = getchild(s, name)
nameIsFound = false;
for i=1:length(s.Children)
    if strcmp(s.Children(i).Name, name)
        s = s.Children(i);
        nameIsFound = true;
        break;
    end
end
end