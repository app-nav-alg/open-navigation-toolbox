function [varargout] = vec2arrays(v, sz)
%VEC2ARRAYS 将向量v按sz的尺寸以列优先方式转换为矩阵
%
% Input Arguments
% # v: 向量
% # sz: 元胞向量，各元素为向量，输出矩阵的尺寸
%
% Output Arguments
% # varargout: 输出个数与sz长度相同

idxStart = 1;
% varargout = cell([1, length(sz)]);
for i=1:length(sz)
    thisNumElem = prod(sz{i});
    if (thisNumElem==1)
        varargout{i} = v(idxStart);
    else
        varargout{i} = reshape(v(idxStart:(idxStart+thisNumElem-1)), sz{i});
    end
    idxStart = idxStart + thisNumElem;
end
end