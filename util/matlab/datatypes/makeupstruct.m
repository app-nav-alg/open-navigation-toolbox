function [ sFull ] = makeupstruct( s, fullFieldNames, fullFieldValues)
%MAKEUPSTRUCT 补全结构体的域
%
% Input Arguments:
% s: 结构体
% fullFieldNames: 元胞向量，元素为字符串，sFull中应包含的域的名称
% fullFieldValues: 长度与fullFieldNames相同的元胞向量，元素为对应域除最后一维以外的值，该值沿最后一维复制
%
% Output Arguments:
% sFull: 补全后的结构体，包含s中原有的域（域值不变），且包含fullFieldNames中所有的域，补全的各域最后一维的尺寸与s中第一个域的最后一维尺寸相同

curFieldNames = fieldnames(s);
lastDimSize = size(s.(curFieldNames{1}), ndims(s.(curFieldNames{1})));
sFull = s;
for i=1:length(fullFieldNames)
    if ~isfield(s, fullFieldNames{i})
        if iscolumn(fullFieldValues{i})
            tmp = 1;
        else
            tmp = ndims(fullFieldValues{i});
        end
        sizeVec = [ones(1, tmp) lastDimSize];
        sFull.(fullFieldNames{i}) = repmat(fullFieldValues{i}, sizeVec);
    end
end
end