function [ A ] = cellofvec2mat( C )
%CELLOFVEC2MAT 将包含向量的元胞数组转换为矩阵
%
% Input Arguments:
% C: 多维元胞数组，各元素为数值向量
%
% Output Arguments:
% A: 维数比C多1的数值矩阵，第2至最后一维的尺寸与C相同，第1维的尺寸为C中向量的最大长度，分别为C中对应元素的向量（长度不足的元素为NaN）

szCell = size(C);
maxLength = 0;
for i=1:prod(szCell)
    if length(C{i}) > maxLength % NOTE: 使用线性索引
        maxLength = length(C{i}); % NOTE: 使用线性索引
    end
end
A = NaN([maxLength szCell]);
for i=1:prod(szCell)
    v = NaN(maxLength, 1);
    v(1:length(C{i}), 1) = C{i}; % NOTE: 使用线性索引
    startIdx = (i-1)*maxLength+1;
    A(startIdx:(startIdx+maxLength-1)) = v;
end
