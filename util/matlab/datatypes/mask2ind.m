function [indArr] = mask2ind(mask, base, indTrueElemOnly, delFalseElem) %#codegen
%MASK2IND 将掩码转换为索引
%
% Input Arguments:
% mask: 逻辑数组
% base: 标量，indArr索引序号的基，indArr中值最小的元素的值为base，默认为NaN（即不进行归基操作）
% indTrueElemOnly: 逻辑标量，true表示只记mask中为true元素的序号，否则记mask中所有元素的序号，默认为false
% delFalseElem: 逻辑标量，true表示indArr中删除mask中false值对应的元素，默认为false
%
% Output Arguments:
% indArr: 当delFalseElem为false时，尺寸与mask相同的数组，否则尺寸比mask小，mask中值为false的元素在indArr中的对应元素为NaN（或者被删除），值为true的元素在indArr中的值为该元素在mask中的线性索引序号（当indTrueElemOnly为false时）或mask中true元素的序号（当indTrueElemOnly为true时），均减去偏移值
%
% Algorithms:
% 偏移值=ind1-base，其中ind1为mask中第一个true元素的线性索引

if nargin < 4
    delFalseElem = false;
end
if nargin < 3
    indTrueElemOnly = false;
end
if nargin < 2
    base = NaN;
end

% NOTE: 本函数使用线性索引
indArrFull = NaN(size(mask));
ind = find(mask);
if ~isempty(ind) && indTrueElemOnly
    val = 1:length(ind);
else
    val = ind;
end
indArrFull(ind) = val;
if ~isnan(base) && ~isempty(val)
    indArrFull = indArrFull - (val(1)-base);
end
if delFalseElem
    indArr = indArrFull(~isnan(indArrFull));
else
    indArr = indArrFull;
end
end