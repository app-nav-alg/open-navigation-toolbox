function [ sDst ] = structcpy_specfields( sDst, sSrc, fieldNames, recursive )
%STRUCTCPY_SPECFIELDS 复制结构体中指定名称的域
%
% Input Arguments
% # sDst: 目标结构体
% # sSrc: 源结构体
% # fieldNames: 元胞向量，需要复制的结构体名，若为空，则设置为sDst原有的所有域名，默认为空
% # recursive: 逻辑标量，对sDst为结构体的域递归调用本函数（递归调用时fieldNames为空），默认为true
%
% Output Arguments
% # sDst: 目标结构体，包含的指定域的内容与源结构体的对应域相同（如果源结构体有对应的域的话），其它未指定的域内容不变

if nargin < 4
    recursive = true;
end
if nargin < 3
    fieldNames = {};
end

% NOTE: 本函数使用线性索引
if isempty(fieldNames)
    fieldNames = fieldnames(sDst);
end
for i=1:length(fieldNames)
    if isfield(sSrc, fieldNames{i})
        if isfield(sDst, fieldNames{i}) && isstruct(sDst.(fieldNames{i})) && isscalar(sDst.(fieldNames{i})) && recursive
            sDst.(fieldNames{i}) = structcpy_specfields(sDst.(fieldNames{i}), sSrc.(fieldNames{i}), {}, recursive);
        else
            sDst.(fieldNames{i}) = sSrc.(fieldNames{i});
        end
    end
end
end