function [ sDst ] = structcpy_fieldnames( sSrc )
%STRUCTCPY_FIELDNAMES 复制结构体的域，仅复制域名，各域设置为空
%
% Input Arguments
% # sSrc: 源结构体
%
% Output Arguments
% # sDst: 目标结构体，包含的域数量及名称不变，各域内容为空

% NOTE: 本函数使用线性索引
names = fieldnames(sSrc);
for i=1:length(names)
    sDst.(names{i}) = [];
end
end