function [ ] = playmat( matFilePath )
% PLAYMAT 播放储存在MAT文件中的声音
%
% Input Arguments:
% matFilePath: MAT文件路径，该文件包含y（列向量或列数为2的矩阵，音频信号）和Fs（标量，采样频率，单位Hz）两个变量

tmp = load(matFilePath);
sound(tmp.y, tmp.Fs);
end