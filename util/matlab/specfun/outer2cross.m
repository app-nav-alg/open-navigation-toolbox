function [v] = outer2cross(A) %#codegen
%OUTER2CROSS 由向量外积计算向量叉积
%
% Input Arguments
% # A: 3*3*p数组，各页为列向量a、b的外积a*b'
%
% Output Arguments
% # v: p*3矩阵，各行为a、b的叉积a×b

v = NaN(size(A, 3), 3);
v(:, 1) = squeeze(A(2, 3, :) - A(3, 2, :));
v(:, 2) = squeeze(A(3, 1, :) - A(1, 3, :));
v(:, 3) = squeeze(A(1, 2, :) - A(2, 1, :));
end