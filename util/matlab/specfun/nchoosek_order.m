function [CP] = nchoosek_order(v, k) %#codegen
%NCHOOSEK_ORDER 考虑排列顺序的组合
%
% Input Arguments
% # v, k: 参见函数nchoosek的帮助
%
% Output Arguments
% # CP: 行数为n!/((n–k)!)（n为向量v的长度），列数为k的矩阵，各行包含了v中任意k个元素的考虑排列顺序的组合

C = nchoosek(v, k);
[mC, nC] = size(C);
assert(nC == k);
ind = 1:k;
P = perms(ind);
[mP, nP] = size(P);
assert(nP == k);
CP = NaN(mC*mP, k);
for i=1:mP
    CP(((i-1)*mC+1):(i*mC), :) = permuteind(C, P(i, :), 2);
end
end