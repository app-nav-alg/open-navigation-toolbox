function [DHMS] = msecs2hms(msecs)
% MSECS2HMS 将毫秒转化为天时分秒
%
% Input Arguments:
% msecs: m*1向量，单位msecs
%
% Output Arguments:
% DHMS: m*4矩阵，每列分别为天、时、分、秒

DHMS(:, 1) = fix(msecs/1000/3600/24);
rem = msecs-DHMS(:, 1)*3600*1000*24;
DHMS(:, 2) = fix(rem/1000/3600);
rem = rem-DHMS(:, 2)*3600*1000;
DHMS(:, 3) = fix(rem/1000/60);
DHMS(:, 4) = round((rem-DHMS(:, 3)*60*1000))/1000;
end