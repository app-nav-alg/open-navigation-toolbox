function [ R ] = rem_round( X, Y )
%REM_ROUND 相除后的余数，商按四舍五入取整
%
% Input Arguments:
% X, Y: 尺寸相同的矩阵
%
% Output Arguments:
% R: 尺寸与X相同的矩阵，X-n.*Y，n为X按元素除以Y后四舍五入取整量

R = X - round(X./Y).*Y;
end
