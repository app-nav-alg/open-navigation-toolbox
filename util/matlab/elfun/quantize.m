function [ QA, frac ] = quantize( A, hRoundFcn ) %#codegen
%QUANTIZE 按列量化矩阵
%
% Input Arguments
% # A: 二维矩阵
% # hRoundFcn: 取整函数句柄，默认为@fix
%
% Output Arguments
% # QA: 二维矩阵，尺寸与A相同
% # frac: 二维矩阵，尺寸与A相同，各行量化后的小数部分，A+[zeros(1, 3); frac(1:(end-1), :)]与QA+frac相等

if nargin < 2
    hRoundFcn = @fix;
end

QA = NaN(size(A));
frac = NaN(size(A));
currFrac = zeros(1, size(A, 2));
for i=1:size(A, 1)
    QA(i, :) = hRoundFcn(A(i, :)+currFrac);
    currFrac = A(i, :) + currFrac - QA(i, :);
    frac(i, :) = currFrac;
end
end