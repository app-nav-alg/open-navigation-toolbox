function [ Y ] = truncate( X, digitsAfterPoint )
%TRUNCATE 截取到小数点后指定位数
%
% Input Arguments:
% X: 矩阵
% digitsAfterPoint: 标量，指定小数点后的位数，该位数（含）之前的位数将保留，之后的一位将四舍五入
%
% Output Arguments:
% Y: 尺寸与X相同的矩阵，截取后的结果

den = 10 ^ digitsAfterPoint;
Y = round(X*den) / den;
end