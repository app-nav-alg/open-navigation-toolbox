function B = wrap( A, min, max ) %#codegen
%WRAP 将矩阵的元素规整到指定的区间内
%
% Input Arguments:
% A: 矩阵
% min, max: 标量，矩阵A的各元素将被规整到[min, max)区间
%
% Output Arguments:
% B: 尺寸与A相同的矩阵

B = mod(A-min, max-min) + min;
end
