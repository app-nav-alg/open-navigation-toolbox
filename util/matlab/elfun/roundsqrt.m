function [m, n] = roundsqrt(x)
%ROUNDSQRT 开方并取整
%
% Input Arguments:
% x: 标量
%
% Output Arguments:
% m: 标量，与x平方根最接近的整数
% n: 标量，不小于x/m的最小整数

m = round(sqrt(x));
n = ceil(x/m);
end