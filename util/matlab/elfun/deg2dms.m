function [DMS] = deg2dms(deg)
% DEG2DMS 将角度转化为度分秒
%
% Input Arguments:
% deg: 向量，单位°
%
% Output Arguments:
% DMS: n*3矩阵，n为deg长度，各列分别为度、分、秒

n = length(deg);
DMS = NaN(n, 3);
for i=1:n
    DMS(i, 1) = fix(deg(i));
    rem = abs(deg(i)-DMS(i, 1)) * 60;
    DMS(i, 2) = floor(rem);
    DMS(i, 3) = (rem-DMS(i, 2)) * 60;
end
end