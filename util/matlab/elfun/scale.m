function [ B ] = scale( A, lowBound, upBound )
%SCALE 将矩阵的元素按比例缩放到指定区间内
%
% Input Arguments:
% A: 输入矩阵
% lowBound: 标量，输出矩阵各元素的最小值，默认为0
% upBound: 标量，输出矩阵各元素的最大值，默认为1
%
% Output Arguments:
% B: 输出矩阵，尺寸与A相同

if nargin < 3
    upBound = 1;
end
if nargin < 2
    lowBound = 0;
end

curMax = max(A(:));
curMin = min(A(:));
B = (upBound-lowBound)/(curMax-curMin)*(A-curMin) + lowBound;
end