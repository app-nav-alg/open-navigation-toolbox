function [second] = hms2secs(hms)
% HMS2SECS 将时分秒转化为秒
%
% Input Arguments:
% hms: m*1向量，格式为hhmmss
%
% Output Arguments:
% second: m*1向量，单位秒

h = fix(hms./10000);
m = fix(hms./100) - h*100;
s = hms - h*10000 - m*100;
second = h*3600 + m*60 + s;
end