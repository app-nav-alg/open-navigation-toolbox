function [data, dataStat] = logtestdatastat(testCase, data, dataDesc, fcnNames, dim, logVerbosity, doSqueeze, doPlot)
%LOGTESTDATASTAT 将数据统计信息输出到测试记录中
%
% Input Arguments
% # testCase: matlab.unittest.TestCase对象
% # data: 数组，待统计数据
% # dataDesc: 字符串，数据描述
% # fcnNames: 各元素为函数名字符串的元胞向量，函数名可以为sum、cumsum、prod、cumprod、mean、mode、median、diff、std、var、max、min，默认为{'max', 'mean'}，输入空元胞向量将使用默认值
% # dim: 标量，统计所沿的维度，默认为1
% # logVerbosity: matlab.unittest.Verbosity枚举，为空或者默认时为Terse
% # doSqueeze: 逻辑标量，true表示压缩dataStat中长度为1的维，默认为true
% # doPlot: 逻辑标量，true表示绘图（仅对1~3维统计结果，按长度最小的维数分子图），默认为false
%
% Output Arguments
% # data: 输入data原样返回
% # dataStat: 元胞向量，各元素为data的统计值

if nargin < 4 || isempty(fcnNames)
    fcnNames = {'max', 'mean'};
end
if nargin < 5
    dim = 1;
end
if (nargin<6) || isempty(logVerbosity)
    logVerbosity = matlab.unittest.Verbosity.Terse;
end
if nargin < 7
    doSqueeze = true;
end
if nargin < 8
    doPlot = false;
end

dataStat = cell(size(fcnNames));
for i=1:length(fcnNames)
    fName = fcnNames{i};
    if strcmp(fName, 'sum') || strcmp(fName, 'cumsum') || strcmp(fName, 'prod') || strcmp(fName, 'cumprod') ...
            || strcmp(fName, 'mean') || strcmp(fName, 'mode') || strcmp(fName, 'median')
        dataStat{i} = feval(fName, data, dim);
    elseif strcmp(fName, 'diff')
        dataStat{i} = feval(fName, data, 1, dim);
    elseif strcmp(fName, 'std') || strcmp(fName, 'var')
        dataStat{i} = feval(fName, data, 0, dim);
    elseif strcmp(fName, 'max') || strcmp(fName, 'min')
        dataStat{i} = feval(fName, data, [], dim);
    else
        error('不支持的函数名称！');
    end
    if doSqueeze
        dataStat{i} = squeeze(dataStat{i});
    end
    str = num2str(dataStat{i});
    dataStatDesc = [dataDesc, fName];
    if size(str, 1) == 1 % 当数据在一行内显示时，转换为char数组
        testCase.log(logVerbosity, [dataStatDesc, '：', newline str]);
    else % 转换为string数组
        str = string(dataStat{i});
        str = permute(str, ndims(str):-1:1); % 最后一维各元素合并到各行
        for j=1:(ndims(str)-1)
            str = join(str, repmat('|', 1, j));
        end
        testCase.log(logVerbosity, [[dataStatDesc, '：']; str]);
    end
    if doPlot
        d = ndims(dataStat{i});
        if d == 1
            preparefig(dataStatDesc);
            plot(dataStat{i});
            title(dataStatDesc);
            grid on;
        elseif d == 2
            [~, I] = min(size(dataStat{i}));
            plot2dmat_sp(dataStat{i}, 1, dataStatDesc, [], I);
        elseif d == 3
            [~, I] = sort(size(dataStat{i}));
            plot3dmat_sp(dataStat{i}, true, 1, dataStatDesc, [], I(1), I(2));
        end
    end
end
end