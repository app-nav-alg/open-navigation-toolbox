function [ hLines ] = xperplines_ylim( x, xMax, xMin, lineStyle, colors )
%XPERPLINES_YLIM 绘制与X轴垂直的横跨整个Y值域范围的直线
%
% Input Arguments
% # x: 向量，所绘制直线与X轴交点的X坐标
% # xMax: 标量，x值大于xMax时将不绘制，默认为max(x)
% # xMin: 标量，x值小于xMin时将不绘制，默认为min(x)
% # lineStyle: 字符串，直线样式，参见line函数说明，默认为'-.'
% # colors: m*3矩阵，m为颜色数量，绘制时，将依次按colors的各行给直线分配颜色，默认为淡绿、淡红
%
% Output Arguments
% # hLines: 与x尺寸相同的向量，各直线的句柄

% NOTE: 本函数使用线性索引
if nargin < 5
    colors = [[16 255 16]/255; [255 160 160]/255];
end
if nargin < 4
    lineStyle = '-.';
end
if nargin < 3
    xMin = min(x);
end
if nargin < 2
    xMax = max(x);
end

hLines = NaN(size(x));
for i=1:length(x)
    if (x(i)>=xMin) && (x(i)<=xMax)
        hLines(i) = line([x(i) x(i)], ylim, 'LineStyle', lineStyle, 'Color', colors(mod(i-1, size(colors, 1))+1, :));
        set(get(get(hLines(i), 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off'); % Exclude line from legend
    end
end
end