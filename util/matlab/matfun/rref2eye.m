function [B, Q] = rref2eye(A)
%RREF2EYE 将初等行变换得到的行最简型矩阵通过列交换变换为左上方为单位阵的矩阵
%
% Input Arguments:
% A: 二维矩阵，行最简型矩阵
%
% Output Arguments:
% B: 二维矩阵，尺寸与A相同，由A通过列交换变换而来，左上方为单位阵
% Q: 二维矩阵，对应于列交换的矩阵，B=AQ

r = double(rank(A));
n = size(A, 2);
B = A;
Q = eye(n);
for i=2:r
    if B(i, i) ~= 1
        for j=i+1:n
            if B(i, j) == 1
                tmp = B(:, i);
                B(:, i) = B(:, j);
                B(:, j) = tmp;
                Qk = eye(n);
                Qk(i, i) = 0;
                Qk(j, j) = 0;
                Qk(i, j) = 1;
                Qk(j, i) = 1;
                Q = Q * Qk;
                break;
            end
        end
    end
end
assert(all(all(A*Q == B)));
end