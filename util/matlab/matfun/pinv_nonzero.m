function [ G, zeroRowIndex, zeroColIndex ] = pinv_nonzero( A ) %#codegen
% PINV_NONZERO 计算A的非零行非零列构成的矩阵的伪逆矩阵并扩展到A'尺寸
%
% Tips:
% 1. 对非零列数较多的矩阵计算右逆矩阵，对非零行数较多的矩阵计算左逆矩阵
%
% Input Arguments:
% A: 二维矩阵
%
% Output Arguments:
% G: 尺寸与A转置相同的矩阵
% zeroRowIndex: 行向量，矩阵G全为0的行（即矩阵A中全为零的列）的序号（相对于矩阵G而言，以1为基数）
% zeroColIndex: 行向量，矩阵G全为0的列（即矩阵A中全为零的行）的序号（相对于矩阵G而言，以1为基数）
%
% Algorithms:
% 1. A矩阵去除所有元素全为零的行和列之后构成矩阵B
% 2. B的伪逆矩阵按A的元素全为零的行和列的位置对应扩充元素全为零的列和行得到G

[B, zeroColIndex, zeroRowIndex] = stripequalrowcol(A, zeros(size(A)));
if isa(A, 'sym')
    G = horzinsert(vertinsert(pinv_direct(B), zeroRowIndex, 0), zeroColIndex, 0); % pinv函数不能处理符号变量
else
    G = horzinsert(vertinsert(pinv(B), zeroRowIndex, 0), zeroColIndex, 0);
end
end