function [A,jb] = rref_cg(A,tol) %#codegen
%RREF_CG rref的代码生成版本，从MATLAB的rref函数复制而来，去掉了有理数模式

[m,n] = size(A);

% Compute the default tolerance if none was provided.
if (nargin < 2), tol = max(m,n)*eps(class(A))*norm(A,'inf'); end

% Loop over the entire matrix.
i = 1;
j = 1;
jb = zeros(1, 0);
while (i <= m) && (j <= n)
    % Find value and index of largest element in the remainder of column j.
    [p,k] = max(abs(A(i:m,j))); k = k+i-1;
    if (p <= tol)
        % The column is negligible, zero it out.
        A(i:m,j) = zeros(m-i+1,1);
        j = j + 1;
    else
        % Remember column index
        jb = [jb j];
        % Swap i-th and k-th rows.
        A([i k],j:n) = A([k i],j:n);
        % Divide the pivot row by the pivot element.
        A(i,j:n) = A(i,j:n)/A(i,j);
        % Subtract multiples of the pivot row from all the other rows.
        for k = 1:(i-1)
            A(k,j:n) = A(k,j:n) - A(k,j)*A(i,j:n);
        end
        for k = (i+1):m % NOTE: 更改以用于代码生成
            A(k,j:n) = A(k,j:n) - A(k,j)*A(i,j:n);
        end
        i = i + 1;
        j = j + 1;
    end
end
end