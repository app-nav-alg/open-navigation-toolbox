function B = adjoint(A) %# codegen
%ADJOINT 计算方阵的伴随阵
%
% Input Arguments:
% A: 二维方阵
%
% Output Arguments:
% B: 二维方阵，A的伴随阵

n = size(A, 1);
B = NaN(n);
C = NaN(n-1);
for i = 1 : n
    for j = 1 : n
        C = A([1:i-1, i+1:n], [1:j-1, j+1:n]); % 余子式
        B(j, i) = (-1)^(i+j) * det(C);
    end
end
end