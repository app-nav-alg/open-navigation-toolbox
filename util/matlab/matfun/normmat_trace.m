function [ PNorm ] = normmat_trace( P )
%NORMMAT_TRACE 使用迹归一化矩阵
%
% Input Arguments:
% P: m*m*p矩阵，p为采样数
%
% Output Arguments:
% PNorm: m*m*p矩阵，用迹归一化后的矩阵

[mRows, ~, pPages] = size(P);
PNorm = NaN(size(P));
for i = 1:pPages
    PNorm(:, :, i) = mRows/trace(P(:, :, i))*P(:, :, i);
end
end