function [ B ] = posdefizesymmat( A ) %#codegen
%POSDEFIZESYMMAT 将对称矩阵非负定化
%
% Input Arguments:
% A: 二维对称方阵
%
% Output Arguments:
% B: 尺寸与A相同的对称矩阵
%
% Assumptions and Limitations:
% 1. 本函数不能保证输出是非负定的，只能增强输入矩阵的负定性
%
% References:
% [1] 理论文档 版本号1.0 章“卡尔曼滤波” 节1.4.1“非负定性的保持”

B = A;
m = size(B, 1);
for i=1:m
    if B(i, i) < 0
        B(i, i) = 0;
    end
end
for i=1:m
    for j=i:m
        p = B(i, i)*B(j, j);
        if abs(B(i, j)*B(j, i)) > p
            B(i, j) = sqrt(p)*sign(B(i, j)+B(j, i));
            B(j, i) = B(i, j);
        end
    end
end
end