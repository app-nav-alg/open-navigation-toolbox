function G = pinv_direct(A) %#codegen
%PINV_DIRECT 计算矩阵的伪逆矩阵，采用直接求逆方法
%
% Tips:
% 1. 对列数较多的矩阵计算右逆矩阵，对行数较多的矩阵计算左逆矩阵
%
% Input Arguments:
% A: 二维矩阵
%
% Output Arguments:
% B: 二维矩阵，尺寸与A的转置相同，A的伪逆矩阵

[m, n] = size(A);
if (m>n) % 左逆矩阵
    G = (A'*A) \ A'; % inv(A'*A) * A'
elseif (m<n) % 右逆矩阵
    G = A' / (A*A'); % A' * inv(A*A')
else % 逆矩阵
    G = inv(A);
end