function [] = saveallfigs( filePathPrefix, format, inludeDateTime )
%SAVEALLFIGS 保存所有绘图
%
% Input Arguments:
% filePathPrefix: 保存文件路径前缀，包括目录名及文件名前缀，如'C:\Data\Test1'，若为空将保存在当前目录
% format: 字符串，保存文件格式，参见saveas函数说明，默认为fig
% inludeDateTime: 逻辑标量，true表示在文件名最后加上保存的日期及时间，默认为true

% NOTE: 本函数使用线性索引

if nargin < 3
    inludeDateTime = true;
end
if nargin < 2
    format = 'fig';
end
if nargin < 1
    filePathPrefix = [pwd '\'];
end

h = findobj(0, '-depth', 1);
h = setdiff(h, 0);
matlabVersion = version;
for i=1:length(h)
    if ishghandle(h(i))
        if inludeDateTime
            dt = ['@' datestr(now, 'yyyy-mm-ddTHH-MM-SS')];
        else
            dt = '';
        end
        if strcmp(matlabVersion(end-6:end-1), 'R2014a')
            saveas(h(i), [filePathPrefix int2str(h(i)) '-' get(h(i), 'Name') dt], format);
        else
            saveas(h(i), [filePathPrefix int2str(h(i).Number) '-' get(h(i), 'Name') dt], format);
        end
    end
end
end