function [loggedVar] = logvar(var, varName) %#ok<INUSL>
%LOGVAR 将var变量记录到'g_vl_varName'的全局元胞数组中
%
% Input Arguments
% # var: 待记录的变量
% # varName: 字符串，待记录的变量名
%
% Output Arguments:
% # loggedVar: 元胞向量，记录下变量，每次记录的变量为元胞向量的一个元素

fullName = ['g_vl_' varName];

if ~evalin('base', ['exist(''', fullName, ''', ''var'')'])
    evalin('base', ['global ' fullName]);
    evalin('base', [fullName ' = {};']);
end
eval(['global ' fullName]);
eval([fullName '{end+1, 1} = var;']);

if nargout > 0
    loggedVar = eval(fullName);
end