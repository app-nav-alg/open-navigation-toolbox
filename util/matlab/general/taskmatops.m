function [] = taskmatops(opName)
%TASKMATOPS 删除或移动当前工作目录下task*子目录中的.mat文件
%
% Input Arguments
% # opName: 字符串，'delete'或‘move'，默认为'move'

if nargin < 1
    opName = 'move';
end

i = 1;
while exist(['task' int2str(i)], 'dir')
    oldFolder = cd(['task' int2str(i)]);
    switch opName
        case 'delete'
            delete *.mat;
        case 'move'
            movefile('*.mat', oldFolder);
        otherwise
            warning('taskmatops:unsupportedoperation', 'Unsupported operation.');
    end
    cd(oldFolder);
    i = i + 1;
end
end