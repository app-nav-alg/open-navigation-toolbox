function [filePaths, fileFolders, fileNames, varargout] = ls_recurs(fileNamePattern, rootPath, hFcn, recursiveSearch)
%LS_RECURS 列出rootPath及其子目录下所有文件名为fileName的文件的路径及目录名
%
% Input Arguments
% # fileNamePattern: 字符串，文件名，可包含通配符
% # rootPath: 字符串，根目录名，空表示当前工作目录，默认为空
% # hFcn: 函数句柄，对返回的filePaths中每一项进行操作的函数，默认为空
% # recursiveSearch: 逻辑标量，true表示递归搜索子目录，否则只搜索根目录，默认为true
%
% Output Arguments
% # filePath: 元胞列向量，每个元素为字符串，各文件的路径
% # fileFolder: 元胞列向量，每个元素为字符串，各文件的所在目录
% # fileName: 元胞列向量，每个元素为字符串，各文件的名称
% # varargout: cellfun输出，每个值为元胞列向量，每个元素为对每个文件应用hFcn的返回值

if nargin < 4
    recursiveSearch = true;
end
if nargin < 3
    hFcn = [];
end
if (nargin < 2) || isempty(rootPath)
    rootPath = pwd;
end

if recursiveSearch
    listing = dir(fullfile(rootPath, '**', fileNamePattern));
else
    listing = dir(fullfile(rootPath, fileNamePattern));
end
listing = listing(~cellfun(@isempty, {listing.date}));
filePaths = cellfun(@(x, y)fullfile(x, y), {listing.folder}', {listing.name}', 'UniformOutput', false);
if nargout > 1
    fileFolders = {listing.folder}';
end
if nargout > 2
    fileNames = {listing.name}';
end
if ~isempty(hFcn)
    varargout = cell(1, nargout-3);
    [varargout{:}] = cellfun(hFcn, filePaths, 'UniformOutput', false);
end