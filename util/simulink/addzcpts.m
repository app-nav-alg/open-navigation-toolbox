function [ simInZC ] = addzcpts(simIn, ZCTimes, tEps)
%ADDZCPTS 对Simulink的to workspace量增加过零指示点
%
% Input Arguments:
% simIn: structure with time格式的to workspace模块输出量，signal dimensions为3
% ZCTimes: 列向量，指定增加过零点的时间，函数将忽略第一个和最后一个时间
% tEps: 标量，时间比较时的阈值
%
% Output Arguments:
% simInZC: 增加加过零指示点后的to workspace量

sort(ZCTimes);
lastZCTimeIndex = 1;
for i=2:length(ZCTimes)-1
    for j=lastZCTimeIndex:length(simIn.time)-1
        if abs(simIn.time(j)-ZCTimes(i)) < tEps
            if ZCTimes(i) - simIn.time(j) > 0
                simIn.signals.values = vertcat(simIn.signals.values(1:j, :), ...
                    simIn.signals.values(j+1, :)*2-simIn.signals.values(j+2, :), ... % 后向增加数据，线性外插
                    simIn.signals.values(j+1:size(simIn.signals.values, 1), :));
            elseif ZCTimes(i) - simIn.time(j) < 0
                simIn.signals.values = vertcat(simIn.signals.values(1:j-1, :), ...
                    simIn.signals.values(j-1, :)*2-simIn.signals.values(j-2, :), ... % 前向增加数据，线性外插
                    simIn.signals.values(j:size(simIn.signals.values, 1), :));
            else
                error('信号时间%f与指定的过零时间%f重合，无法自动判断前向或后向增加信号值', simIn.time(j), ZCTimes(i));
            end
            simIn.time(j) = ZCTimes(i);
            simIn.time = vertcat(simIn.time(1:j), ...
                ZCTimes(i), ...
                simIn.time(j+1:length(simIn.time)));
            lastZCTimeIndex = j + 1;
            break;
        end
    end
end
simInZC = simIn;