function [rImagePoints] = changecheckerboardorigin(imagePoints, boardSize, rot90k)
%CHANGECHECKERBOARDORIGIN 更改标定板原点
%
% Input Arguments:
% # imagePoints: m*2*n矩阵，参考detectCheckerboardPoints
% # boardSize: 1*2向量, 参考detectCheckerboardPoints
% # rot90k: 1*n标量, 参考rot90
%
% Output Arguments:
% # rImagePoints: m*2*n矩阵，重排后的值
%

rImagePoints = NaN(size(imagePoints));
rowNum = boardSize(2) - 1;
colNum = boardSize(1) - 1;
n = size(imagePoints, 3);
assert(n == length(rot90k));
for i = 1:n
    tmpImagePoints = imagePoints(:, :, i);
    
    points.x = reshape(tmpImagePoints(:, 1), [rowNum, colNum]);
    points.y = reshape(tmpImagePoints(:, 2), [rowNum, colNum]);
    
    points.x = rot90(points.x, rot90k(i));
    points.y = rot90(points.y, rot90k(i));
    
    rImagePoints(:, 1, i) = reshape(points.x, [rowNum*rowNum, 1]);
    rImagePoints(:, 2, i) = reshape(points.y, [rowNum*rowNum, 1]);
end
end