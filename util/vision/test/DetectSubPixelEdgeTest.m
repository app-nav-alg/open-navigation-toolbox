classdef DetectSubPixelEdgeTest < matlab.unittest.TestCase

    methods(Test)
        function vldSubPixelEdgeDetectTest(testCase)
            imageSize = [400, 400];
            imageEdge = 250;
            nNeighborPoints = 6;
            edgeIntcoeffs = genimganddetectsubpixeledge(imageSize, imageEdge, int8(1), nNeighborPoints);
            edgeIntcoeffsErr = edgeIntcoeffs(1, 2) - (imageEdge+0.5);
            disp('二阶多项式拟合算法边缘误差');
            disp(rms(edgeIntcoeffsErr));
            
            edgeLogarithm = genimganddetectsubpixeledge(imageSize, imageEdge, int8(2), nNeighborPoints);
            edgeLogarithmErr = edgeLogarithm(1, 2) - (imageEdge+0.5);
            disp('灰度差取对数算法边缘误差');
            disp(rms(edgeLogarithmErr));
            testCase.verifyLessThanOrEqual(abs(rms(edgeLogarithmErr)), 1e-5);
            
            edgeGauss = genimganddetectsubpixeledge(imageSize, imageEdge, int8(3), nNeighborPoints);
            edgeGaussErr = edgeGauss(1, 2) - (imageEdge+0.5);
            disp('高斯曲线拟合算法边缘误差');
            disp(rms(edgeGaussErr));
            testCase.verifyLessThanOrEqual(rms(edgeGaussErr), 1e-5);
        end
    end
end
