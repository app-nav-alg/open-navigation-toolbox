function [pts] = generateellipse(nSamp, center, semiAxes, angle)
%GENERATEELLIPSE 生成椭圆边缘点
% 
% Input Arguments
% # nSamp：1*1数组，横坐标间距1以内的边缘点的数目
% # center：1*2数组，椭圆圆心
% # semiAxes：1*2数组，椭圆长轴和短轴
% # angle：1*1数组，椭圆倾斜角，单位：弧度

points(1:nSamp*semiAxes(1)*2+1, 1) = -semiAxes(1):1/nSamp:semiAxes(1);
points(1:nSamp*semiAxes(1)*2+1, 2) =(sqrt(1 - (points(1:nSamp*semiAxes(1)*2+1, 1)).^2/semiAxes(1)^2))*semiAxes(2);
points(nSamp*semiAxes(1)*2+2:nSamp*semiAxes(1)*4+2, 1) = -semiAxes(1):1/nSamp:semiAxes(1);
points(nSamp*semiAxes(1)*2+2:nSamp*semiAxes(1)*4+2, 2) = -sqrt(1 - (points(nSamp*semiAxes(1)*2+2:nSamp*semiAxes(1)*4+2, 1)).^2/semiAxes(1)^2)*semiAxes(2);

pts(:, 1) = points(:, 1) * cos(angle) - points(:, 2) * sin(angle) + center(1);
pts(:, 2) = points(:, 1) * sin(angle) + points(:, 2) * cos(angle) + center(2);
end

