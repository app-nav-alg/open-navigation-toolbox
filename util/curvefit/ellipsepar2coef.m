function [coef] = ellipsepar2coef(centers, semiAxes, angles)
%ELLIPSEPAR2COEF 根据椭圆圆心、长半轴短半轴、倾斜角计算椭圆方程系数
%
% Input Arguments
% # centers: m*2矩阵，m为椭圆圆心个数
% # semiAxes: m*2矩阵，椭圆长半轴和短半轴
% # angles: 长度为m的列向量，椭圆长半轴与x轴正向夹角，单位：弧度
%
% Output Arguments
% # coef: m*6矩阵，椭圆方程系数，A*x^2 + B*x*y + C*y^2 + D*x E*y + F = 0

m = size(centers, 1);
C = NaN(3, 3, m);
coef = NaN(m, 6);
for iEllipse = 1:m
    CStd = diag([1/semiAxes(iEllipse, 1)^2, 1/semiAxes(iEllipse, 2)^2, -1]); % 标准椭圆二次型
    H = [cos(angles(iEllipse)) -sin(angles(iEllipse)) centers(iEllipse, 1);
        sin(angles(iEllipse)) cos(angles(iEllipse)) centers(iEllipse, 2);
        0 0 1]; % 转到指定圆心及转角的单应矩阵
    C(:, :, iEllipse) = ((H')\CStd)/H; % 指定椭圆二次型
    coef(iEllipse,:) = [C(1, 1, iEllipse), C(1, 2, iEllipse)+C(2, 1, iEllipse), C(2, 2, iEllipse), C(1, 3, iEllipse)+C(3, 1, iEllipse), C(2, 3, iEllipse)+C(3, 2, iEllipse), C(3, 3, iEllipse)];
end
end