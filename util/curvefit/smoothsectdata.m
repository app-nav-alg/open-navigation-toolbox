function [data] = smoothsectdata(data, smoothSpan, t, tSects, sectMask, smoothIterNum, dim, deductAvg)
% SMOOTHSECTDATA 按移动平均方式（采用反射型边界条件）平滑分段的数据
%
% Input Arguments:
% data: m*n矩阵，m为t的长度，输入值为原始数据，输出值尺寸与输入值相同，为平滑后的数据（第一个数据段开始时刻点之前的数据和sectMask指定不平滑的数据置为NaN）
% smoothSpan: 参见smooth函数说明
% t: 向量，数据采样时间，单位s
% tSects: 向量，各数据段的开始时间点（需要包含第一个数据段的开始时刻点，不需要包含最后一个数据段的结束时间点），单位s，如果为空，则将被本函数设为t(1)，默认为空
% sectMask: 长度与tSects相等的逻辑向量，true表示需要平滑tSects中对应元素指示的时刻（包含）开始至tSects中下一个元素指示的时刻（不包含）结束的时段，如果为空，则将被本函数设为与tSects尺寸相同的全为true的逻辑向量，默认为空
% smoothIterNum: 平滑次数，默认为1
% dim: 标量，待平滑的维数，默认为1
% deductAvg: true指定扣除平滑后数据的平均值，默认为false

if nargin < 8
    deductAvg = false;
end
if nargin < 7
    dim = 1;
end
if nargin < 6
    smoothIterNum = 1;
end
if nargin < 5
    sectMask = [];
end
if nargin < 4
    tSects = [];
end

if isempty(tSects)
    tSects = t(1);
end
if isempty(sectMask)
    sectMask = true(size(tSects));
end

% 平滑数据
t = t(:);
tSectsEx = [tSects(:); t(end)+1];
% 去掉tSects首元素时刻前的数据
data(t<tSectsEx(1), :) = NaN;
% 处理tSects中各段的数据
for i=1:length(sectMask)
    idx = find((t>=tSectsEx(i)) & (t<tSectsEx(i+1)));
    if sectMask(i)
        % 平滑未屏蔽的段
        data(idx, :) = smooth_boundcond(data(idx, :), smoothSpan, dim, 'moving', 'reflect', smoothIterNum);
        if deductAvg
            data(idx, :) = data(idx, :) - repmat(mean(data(idx, :)), length(idx), 1);
        end
    else
        % 无效化被屏蔽的段
        data(idx, :) = NaN;
    end
end
end