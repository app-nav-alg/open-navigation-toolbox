classdef EllipseCoefParTest < matlab.unittest.TestCase
    
    methods(Test)
        function vldEllipseCoefParTest(testCase)
            nSamp = 10;
            center = [2, 6];
            semiAxes = [5, 2];
            angle = 12*pi/6;
            pts = generateellipse(nSamp, center, semiAxes, angle);
            [centerFit, semiAxesFit, angleFit] = fitellipse(pts);
            centerFitError = center - centerFit;
            semiAxesFitError = [max(semiAxes), min(semiAxes)] - semiAxesFit;
            angleFitError = angle - angleFit;
            if semiAxes(1) < semiAxes(2)
                angleFitError = angleFitError - pi/2;
            end
            disp('拟合椭圆圆心误差');
            disp(centerFitError);
            disp('拟合椭圆长半轴和短半轴误差');
            disp(semiAxesFitError);
            disp('拟合椭圆倾斜角误差');
            disp(angleFitError);
            testCase.verifyLessThanOrEqual(abs(centerFitError), [1e-5, 1e-5]);
            testCase.verifyLessThanOrEqual(abs(semiAxesFitError), [1e-5, 1e-5]);
            
            coefCal = ellipsepar2coef(centerFit, semiAxesFit, angleFit);
            [centerCal, semiAxesCal, angleCal] = ellipsecoef2par(coefCal);
            centerCalError = centerFit - centerCal;
            semiAxesCalError = semiAxesFit - semiAxesCal;
            angleCalError = angleFit - angleCal;
            disp('计算椭圆圆心误差');
            disp(abs(centerCalError));
            disp('计算椭圆长半轴和短半轴误差');
            disp(abs(semiAxesCalError));
            disp('计算椭圆倾斜角误差');
            disp(abs(angleCalError));
            testCase.verifyLessThanOrEqual(abs(centerCalError), [1e-5, 1e-5]);
            testCase.verifyLessThanOrEqual(abs(semiAxesCalError), [1e-5, 1e-5]);
        end
    end
end

