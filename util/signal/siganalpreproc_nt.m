function [y, tOut] = siganalpreproc_nt(x, tIn, varargin)
%SIGANALPREPROC_NT 信号分析App自定义预处理接口函数（忽略时间参数）
%
% Input Argument
% # varargin: 元胞数组，首个元素为处理函数句柄，其余为该函数除输入信号外的剩余参数

assert(nargin > 2);
hFcn = varargin{1};
if nargin > 3
    y = hFcn(x, varargin{2:end});
else
    y = hFcn(x);
end
tOut = tIn;
end