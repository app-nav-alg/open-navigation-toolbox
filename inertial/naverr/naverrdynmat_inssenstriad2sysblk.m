function [FBlk] = naverrdynmat_inssenstriad2sysblk(coef, in, inPrime, sensTypeIsAcc, triadSensStateMask)
%NAVERRDYNMAT_INSSENSTRIAD2SYSBLK 计算误差动态矩阵的加速度计或陀螺三元组状态→系统状态块
%
% Input Arguments
% # coef: 3*3矩阵，系数矩阵，在惯导系统误差中通常为CBN，无单位
% # in: 3*1向量，对于加速度计三元组为比力fB，单位m/s^2，对于陀螺三元组为角速度omegaIBB，单位rad/s
% # inPrime: 3*1向量，对于陀螺三元组为比力fB，单位m/s^2，对于加速度计三元组为角速度omegaIBB，单位rad/s
% # sensTypeIsAcc: 逻辑标量，true表示包含三元组为加速度计，false表示为陀螺
% # triadSensStateMask: 对于加速度计为1*15逻辑向量，对于陀螺为1*21逻辑向量，true表示包含该器件状态，false表示不包含该器件状态
%
% Output Arguments
% # FBlk: 3*15或3*21矩阵，误差动态矩阵的加速度计或陀螺三元组状态→系统状态块，为减少计算量，根据triadSensStateMask指示不需要计算的部分元素没有计算，为0
%
% Assumptions and Limitations
% # 理论上，传入本函数的导航参数应为理想值，实际计算中以惯导计算的参数为输入
%
% References
% # 《应用导航算法工程基础》“考虑惯性器件误差模型的线性化方程组”

coder.extrinsic('sym');
% REF1式9.163
if isempty(coder.target) && (isa(coef, 'sym') || isa(in, 'sym') || isa(inPrime, 'sym'))
    if sensTypeIsAcc
        FBlk = zeros(3, 15, 'sym');
    else
        FBlk = zeros(3, 21, 'sym');
    end
else
    if sensTypeIsAcc
        FBlk = zeros(3, 15);
    else
        FBlk = zeros(3, 21);
    end
end
% 零偏
if any(triadSensStateMask(1, 1:3))
    FBlk(:, 1:3) = coef;
end
% 标度因数
for i=1:3
    if triadSensStateMask(1, 3+i)
        FBlk(:, 3+i) = coef(:, i) * in(i, 1);
    end
end
% 失准角
inExp = [in(2, 1) in(3, 1) in(1, 1) in(3, 1) in(1, 1) in(2, 1)];
col = [1 1 2 2 3 3];
for i=1:6
    if triadSensStateMask(1, 6+i)
        FBlk(:, 6+i) = coef(:, col(1, i)) * inExp(1, i);
    end
end
if sensTypeIsAcc
    % 二次项
    for i=1:3
        if triadSensStateMask(1, 12+i)
            FBlk(:, 12+i) = coef(:, i) * (in(i, 1).^2);
        end
    end
else
    % g敏感项
    inPrimeExp = repmat(inPrime', 1, 3);
    colPrime = [1 1 1 2 2 2 3 3 3];
    for i=1:9
        FBlk(:, 12+i) = coef(:, colPrime(1, i)) * inPrimeExp(1, i); % 全部算出以便于挠性惯组对称性处理
    end
    % 取负值
    FBlk = -FBlk;
end
end