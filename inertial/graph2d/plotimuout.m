function [] = plotimuout(t, par, parUnits, parPlotMask, tSects, figName, hFig, m, n, p)
%PLOTIMUOUT 绘制结构体形式的惯组输出曲线
%
% Tips:
% 1. 每种类型的输出占用一个subplot
%
% Input Arguments:
% t: 向量，时间，单位s
% par: 结构体形式的惯组输出数据（可以但不必须包含acc、gyro等域），参数组数与t长度相等，时间在最后一维上
% parUnits: 域与par相同的结构体，对应各域的单位，默认为^
% 其它参数: 参见plotstructpar函数帮助

if nargin < 10
    p = 1;
end
if nargin < 9
    n = 0;
end
if nargin < 8
    m = 0;
end
if nargin < 7
    hFig = [];
end
if nargin < 6
    figName = [];
end
if nargin < 5
    tSects = [];
end
if nargin < 4
    parPlotMask = [];
end
if nargin < 3
    parUnits = struct('acc', '\^', 'gyro', '\^');
end

parTitles = struct('acc', '加速度计输出', 'gyro', '陀螺输出');
parAxis.acc = {'X' 'Y' 'Z'}';
parAxis.gyro = {'X' 'Y' 'Z'}';
parUnitConvertCoef = struct('acc', 1, 'gyro', 1);
if isempty(figName)
    figName = '惯组输出';
end
plotstructpar(t, par, parTitles, parUnits, parUnitConvertCoef, parAxis, 'fieldPlotMask', parPlotMask, 'tSects', tSects, 'figName', figName, 'hFig', hFig, 'm', m, 'n', n, 'p', p);
end