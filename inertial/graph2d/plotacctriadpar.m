function [] = plotacctriadpar(t, par, parName, fieldPlotMask, tSects, figName, tUnit, marker, hFig, m, n, p)
%PLOTACCTRIADPAR 绘制结构体形式的加速度计三元组参数曲线
%
% Tips:
% # 每种类型的参数占用一个subplot
%
% Input Arguments:
% # par: 结构体或结构体向量形式的加速度计参数（可以但不必须包含KScal0、dfBiasS、dKScalP、dPSS0等域），参数组数与t长度相等，时间在最后一维上，单位为相应的国际单位制单位
% # parName: 元胞向量，长度与par相同，各元素为字符串，对应各组参数的名称，默认为空
% # 其它参数: 参见plotstructpar函数帮助
%
% Assumptions and Limitations:
% # 本函数在单位转换时重力加速度采用了9.8m/s^2的默认值

% TODO: 加速度二次项系数在画图曲线中未给出
if nargin < 12
    p = 1;
end
if nargin < 11
    n = 0;
end
if nargin < 10
    m = 0;
end
if nargin < 9
    hFig = [];
end
if nargin < 8
    marker = 'o';
end
if nargin < 7
    tUnit = 's';
end
if nargin < 6
    figName = [];
end
if nargin < 5
    tSects = [];
end
if nargin < 4
    fieldPlotMask = [];
end
if (nargin<3) || isempty(parName)
    parName = repmat({''}, size(par));
end

fieldTitles = struct('KScal0', '加速度计标度因数', 'dfBiasS', '加速度计零偏', 'dKScalP', '加速度计标度因数误差', 'dPSS0', '加速度计安装误差');
fieldUnits = struct('KScal0', '(^/s)/g', 'dfBiasS', 'μg', 'dKScalP', 'ppm', 'dPSS0', 'μrad');
parLegends = struct;
for i=1:length(par)
    parLegends(i).KScal0 = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dfBiasS = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dKScalP = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z'];[parName{i} 'X1'] [parName{i} 'Y1'] [parName{i} 'Z1'];[parName{i} 'X2'] [parName{i} 'Y2'] [parName{i} 'Z2']}';
    parLegends(i).dPSS0 = {[parName{i} 'XX'] [parName{i} 'XY'] [parName{i} 'XZ']; [parName{i} 'YX'] [parName{i} 'YY'] [parName{i} 'YZ']; [parName{i} 'ZX'] [parName{i} 'ZY'] [parName{i} 'ZZ']};
    parLegends(i).SO = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
end
fieldUnitConvertCoef = struct('KScal0', 9.8, 'dfBiasS', 1e6/9.8, 'dKScalP', 1e6, 'dPSS0', 1e6); %TODO: 加速度计dKScalP二次项误差系数实应为1e6*9.8，因此未画图
if isempty(fieldPlotMask)
    fieldPlotMask.KScal0 = true(3, 1);
    fieldPlotMask.dfBiasS = true(3, 1);
    fieldPlotMask.dKScalP = [ true(3, 1) false(3,2) ];
    fieldPlotMask.dPSS0 = logical(vec2offdiag(ones(6, 1)));
end
if isempty(figName)
    figName = '加速度计参数';
end
plotstructpar(t, par, fieldTitles, fieldUnits, fieldUnitConvertCoef, parLegends, 'fieldPlotMask', fieldPlotMask, 'tSects', tSects, 'figName', figName, 'tUnit', tUnit, 'marker', marker, 'hFig', hFig, 'm', m, 'n', n, 'p', p);
end