function [] = plotgyrotriadpar(t, par, parName, fieldPlotMask, tSects, figName, tUnit, marker, hFig, m, n, p)
%PLOTGYROTRIADPAR 绘制结构体形式的陀螺三元组参数曲线
%
% Tips:
% 1. 每种类型的参数占用一个subplot
%
% Input Arguments:
% par: 结构体形式或结构体向量形式的陀螺参数（可以但不必须包含KScal0、domegaIBBiasS、dSF、dKScalP、dKScalN、dPSS0、DGSens等域），参数组数与t长度相等，时间在最后一维上，单位为相应的国际单位制单位
% parName: 元胞向量，长度与par相同，各元素为字符串，对应各组参数的名称，默认为空
% 其它参数: 参见plotstructpar函数帮助
%
% Assumptions and Limitations:
% 1. 本函数在单位转换时重力加速度采用了9.8m/s^2的默认值

if nargin < 12
    p = 1;
end
if nargin < 11
    n = 0;
end
if nargin < 10
    m = 0;
end
if nargin < 9
    hFig = [];
end
if nargin < 8
    marker = 'o';
end
if nargin < 7
    tUnit = 's';
end
if nargin < 6
    figName = [];
end
if nargin < 5
    tSects = [];
end
if nargin < 4
    fieldPlotMask = [];
end
if (nargin<3) || isempty(parName)
    parName = repmat({''}, size(par));
end

sec2Rad = pi/(180*3600);
fieldTitles = struct('KScal0', '陀螺标度因数', 'domegaIBBiasS', '陀螺零偏', 'dSF', '陀螺标度因数误差', 'dKScalP', '陀螺正向标度因数误差', 'dKScalN', '陀螺负向标度因数误差', 'dPSS0', '陀螺安装误差', 'DGSens', '陀螺g敏感项系数');
fieldUnits = struct('KScal0', '^/″', 'domegaIBBiasS', '°/h', 'dSF', 'ppm', 'dKScalP', 'ppm', 'dKScalN', 'ppm', 'dPSS0', 'μrad', 'DGSens', '°/h/g');
parLegends = struct;
for i=1:length(par)
    parLegends(i).KScal0 = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).domegaIBBiasS = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dSF = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dKScalP = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z'];[parName{i} 'X1'] [parName{i} 'Y1'] [parName{i} 'Z1'];[parName{i} 'X2'] [parName{i} 'Y2'] [parName{i} 'Z2']}';
    parLegends(i).dKScalN = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z'];[parName{i} 'X1'] [parName{i} 'Y1'] [parName{i} 'Z1'];[parName{i} 'X2'] [parName{i} 'Y2'] [parName{i} 'Z2']}';
    parLegends(i).dPSS0 = {[parName{i} 'XX'] [parName{i} 'XY'] [parName{i} 'XZ']; [parName{i} 'YX'] [parName{i} 'YY'] [parName{i} 'YZ']; [parName{i} 'ZX'] [parName{i} 'ZY'] [parName{i} 'ZZ']};
    parLegends(i).DGSens = {[parName{i} 'XX'] [parName{i} 'XY'] [parName{i} 'XZ']; [parName{i} 'YX'] [parName{i} 'YY'] [parName{i} 'YZ']; [parName{i} 'ZX'] [parName{i} 'ZY'] [parName{i} 'ZZ']};
end
fieldUnitConvertCoef = struct('KScal0', sec2Rad, 'domegaIBBiasS', 1/sec2Rad, 'dSF', 1e6, 'dKScalP', 1e6, 'dKScalN', 1e6, 'dPSS0', 1e6, 'DGSens', 9.8/sec2Rad);
if isempty(fieldPlotMask)
    fieldPlotMask.KScal0 = true(3, 1);
    fieldPlotMask.domegaIBBiasS = true(3, 1);
    fieldPlotMask.dSF = true(3, 1);
    fieldPlotMask.dKScalP = [ true(3, 1) false(3,2) ];
    fieldPlotMask.dKScalN = [ true(3, 1) false(3,2) ];
    fieldPlotMask.dPSS0 = logical(vec2offdiag(ones(6, 1)));
    fieldPlotMask.DGSens = true(3);
end
if isempty(figName)
    figName = '陀螺参数';
end
plotstructpar(t, par, fieldTitles, fieldUnits, fieldUnitConvertCoef, parLegends, 'fieldPlotMask', fieldPlotMask, 'tSects', tSects, 'figName', figName, 'tUnit', tUnit, 'marker', marker, 'hFig', hFig, 'm', m, 'n', n, 'p', p);
end