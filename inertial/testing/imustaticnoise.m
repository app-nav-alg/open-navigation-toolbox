function [staticAccNoise, staticGyroNoise, CBL] = imustaticnoise(specVelInc, angleInc, staticSectBgnInd, staticSectEndInd, g, Dt, L, omegaIE, azm, azmAxis) %#codegen
%IMUSTATICNOISE 计算静止段上的惯组器件噪声
%
% Input Arguments
% # specVelInc: m*3矩阵，比速度增量，单位m/s
% # angleInc: m*3矩阵，角增量，单位rad
% # staticSectBgnInd: 向量，specVelInc与angleInc中各静止段的起始索引序号
% # staticSectEndInd: 向量，specVelInc与angleInc中各静止段的结束索引序号
% # g: 标量，重力加速度，单位m/s^2
% # Dt: 标量，specVelInc与angleInc采样周期，单位s
% # L: 标量，纬度，单位rad
% # omegaIE: 标量，地球自转角速率，单位rad/s
% # azm: 标量，体坐标系B的azmaxis轴在水平面上的投影相对于北向的夹角（由北向绕顺时针转动到该投影为正），单位rad
% # azmAxis: 标量，1-3分别对应X、Y、Z，其它值默认为X轴
%
% Output Arguments
% # staticAccNoise: m1*3矩阵，m1为静止段总长度，静止段上的加速度计噪声
% # staticGyroNoise: m1*3矩阵，m1为静止段总长度，静止段上的陀螺噪声
% # CBL: 3*3*p矩阵，p为静止段数量，各静止段上的姿态矩阵（采用解析粗对准求得）

if nargin < 10
    azmAxis = 1;
end

if nargin < 9
    azm = NaN(1);
end

totalStaticCnt = sum(staticSectEndInd-staticSectBgnInd+1);
staticAccNoise = NaN(totalStaticCnt, 3);
staticGyroNoise = NaN(totalStaticCnt, 3);
CBL = NaN(3, 3, length(staticSectBgnInd));
curStaticCnt = 0;
for i = 1:length(staticSectBgnInd)
    thisStaticSectLen = staticSectEndInd(i) - staticSectBgnInd(i) + 1;
    indWhole = staticSectBgnInd(i):staticSectEndInd(i);
    indStatic = (curStaticCnt+1):(curStaticCnt+thisStaticSectLen);
    if (i==1 && (~isnan(azm)))
        CBL(:, :, i) = coarsealign_stat_extazm(mean(specVelInc(indWhole, :), 1)'/Dt, azm, azmAxis);
    else
        CBL(:, :, i) = coarsealign_stat_rv2(mean(specVelInc(indWhole, :), 1)'/Dt, mean(angleInc(indWhole, :), 1)'/Dt); % 该位置上的方向余弦矩阵
    end
    staticAccNoise(indStatic, :) = specVelInc(indWhole, :) - repmat((CBL(:, :, i)'*[0; 0; -g])'*Dt, thisStaticSectLen, 1);
    staticGyroNoise(indStatic, :) = angleInc(indWhole, :) ...
        - repmat((CBL(:, :, i)'*[omegaIE*cos(L); 0; -omegaIE*sin(L)])'*Dt, thisStaticSectLen, 1);
    curStaticCnt = curStaticCnt + thisStaticSectLen;
end
assert(totalStaticCnt==curStaticCnt);
end