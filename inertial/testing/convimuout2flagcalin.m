function [flagCalIn] = convimuout2flagcalin( accOut, gyroOut, t, tSects, rotTimeExt, samplePeriod)
%CONVIMUOUT2FLAGCALIN 将器件输出文件转换为带静止/转动标志的标定输入文件
%
% Input Arguments:
% accOut, gyroOut: n*3矩阵，n为采样数，分别为X-Z加速度计和陀螺输出数据
% t: n*1向量，时间
% tSects: 列向量，每段的开始时间，列向量，第一个为0的元素和最后一个指定静止段结束时间的元素将被忽略，单位与t相同
% rotTimeExt: 指定转动段两端各延长的时间，单位与t相同，默认为空（不延长）
% samplePeriod: 采样周期，用于检查数据是否有断点，单位与t相同，默认为空（不检查断点）
%
% Output Arguments:
% flagCalIn: n*7矩阵，包含加速度计、陀螺输出数据和静止/转动标志

if nargin < 6
    samplePeriod = [];
end
if nargin < 5
    rotTimeExt = [];
end
% 时间段预处理
if tSects(1, 1) == 0
    tSects = tSects(2:length(tSects), :);
end
if mod(length(tSects), 2) == 1
    tSects = tSects(1:length(tSects)-1, :);
end
if ~isempty(rotTimeExt)
    tSects = expandoddinterval(tSects, rotTimeExt);
end
% 检查数据是否有断点，精度为1us
if ~isempty(samplePeriod)
    for i=1:(size(t, 1)-1)
        if abs(t(i+1, 1) - t(i, 1) - samplePeriod) > 1e-6
            warning('convinsout2flagcalin:warning', '采样数据有断点，在时间%f至%f之间', t(i, 1), t(i+1, 1));
        end
    end
end
% 设置标志位
flag = zeros(size(t));
j = 1;
if ~isempty(samplePeriod)
    tInt = round(t/samplePeriod); % 转为整数再比较，避免浮点误差，下同
    tSectsInt = round(tSects/samplePeriod);
    for i=1:size(t, 1)
        if (j<=length(tSects)) && (tInt(i, 1)>tSectsInt(j, 1)) % 等于分段时间点时尚保持标志，大于分段时间点后改变标志
            j = j + 1;
        end
        flag(i, 1) = mod(j-1, 2);
    end
else
    for i=1:size(t, 1)
        if (j<=length(tSects)) && (t(i, 1)>tSects(j, 1)) % 等于分段时间点时尚保持标志，大于分段时间点后改变标志
            j = j + 1;
        end
        flag(i, 1) = mod(j-1, 2);
    end
end
flagCalIn = [accOut, gyroOut, flag];
end