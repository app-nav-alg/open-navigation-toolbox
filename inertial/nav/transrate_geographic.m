function [ omegaENN ] = transrate_geographic( vN, L, h, RE, eSq ) %#codegen
%TRANSRATE_GEOGRAPHIC 将地速转换为位移角速度
%
% Tips:
% # 导航系N使用ENU地理坐标系
%
% Input Arguments:
% # vN: 3*1向量，N系下的地速，单位m/s
% # L: 标量，纬度，单位rad
% # h: 标量，高度，单位m
% # RE: 标量，地球长半轴，单位m
% # eSq: 标量，地球偏心率的平方，无单位
%
% Output Arguments:
% # omegaENN: 3*1向量，N系下的位移角速度，单位rad/s
%
% References:
% #《应用导航算法工程基础》“位移角速度”

[RMeridian, RNormal] = earthradiiofcurv(L, RE, eSq);
omegaENN = [-vN(2, 1)/(RMeridian+h), vN(1, 1)/(RNormal+h), vN(1, 1)*tan(L)/(RNormal+h)]';
end