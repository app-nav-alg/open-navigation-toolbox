function [ omegaENNz ] = verttransrate_freeazm( omegaIE, sinL ) %#codegen
%VERTTRANSRATE_FREEAZM 计算自由方位坐标系下的垂向位移角速度
%
% Tips:
% # 导航系n取自由方位坐标系（初始为ENU）
%
% Input Arguments:
% # omegaIE: 标量，地球自转角速率，单位rad/s
% # sinL: 标量，纬度的正弦值
%
% Output Arguments:
% # omegaENNz: 标量，自由方位坐标系下的垂向位移角速度，单位rad/s
%
% References:
% #《应用导航算法工程基础》“位移角速度”

omegaENNz = -omegaIE * sinL;
end