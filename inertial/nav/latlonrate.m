function [ LRate, lambdaRate ] = latlonrate(vEast, vNorth, L, h, RE, eSq) %#codegen
%LATLONRATE 计算由地速引起的经向和纬向角速度
%
% Input Arguments:
% # vNorth: 标量，北向地速，单位m/s
% # vEast: 标量，东向地速，单位m/s
% # L: 标量，纬度，单位rad
% # h: 标量，高度，单位m
% # RE: 标量，地球长半轴，单位m
% # eSq: 标量，地球偏心率的平方，无单位
%
% Output Arguments:
% # LRate: 标量，经向角速度，方向向西，单位rad/s
% # lambdaRate: 标量，纬向角速度，方向沿地球自转轴由南向北，单位rad/s
%
% References:
% # 《应用导航算法工程基础》“经向角速度和纬向角速度”

[RMeridian, RNormal] = earthradiiofcurv(L, RE, eSq);
LRate = vNorth / (RMeridian+h); % Eq. 7.4 
lambdaRate = vEast / ((RNormal+h)*cos(L)); % Eq. 7.5
end