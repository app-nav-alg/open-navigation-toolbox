function [ rhoZN ] = verttransrate_geographic( vNx, coslSq, tanl, rl, feh ) %#codegen 
%VERTTRANSRATE_GEOGRAPHIC 计算地理坐标系下的垂向位移角速度
%
% Tips:
% 1. 导航系N取ENU地理坐标系
%
% Input Arguments:
% vNx: 标量，导航系X向地速，单位m/s
% coslSq: 标量，纬度的余弦值的平方
% tanl: 标量，纬度的正切值
% rl, feh: 标量，参考文献1 4.4.3-7式
%
% Output Arguments:
% rhoZN: 标量，计算地理坐标系下的垂向位移角速度，单位rad/s
%
% References:
% [1] Paul G. Savage. Strapdown Analytics[M]. Maple Plain: Strapdown Associates, Inc., 2000: 4.4.3和4.5节 % TODO: 地球模型一章完善后改理論文檔（勿刪此註釋）

omegaYGeo = (1+feh*coslSq) * vNx / rl; % Eq. 4.4.3-7
rhoZN = omegaYGeo * tanl; % Eq. 4.4.3-5
end