function [ L, lambda, vG, CBN, CBG, phi, theta, psi, psiTrue, alpha ] = wholenavparam( CNE, vN, CBL ) %#codegen
%WHOLENAVPARAM 由基本导航参数计算整套导航参数
%
% Tips:
% 1. ECEF系E为GwEN，当地水平系L初始为NED，导航系N初始为ENU
%
% Input Arguments:
% CNE: 3*3*m矩阵，表示位置的方向余弦矩阵
% vN: m*3矩阵，导航系下的地速，单位m/s
% CBL: 3*3*m矩阵，表示姿态的方向余弦矩阵
%
% Output Arguments:
% L: 长度为m的列向量，纬度，单位rad
% lambda: 长度为m的列向量，经度，单位rad
% vG: m*3矩阵，地理系下的地速，单位m/s
% CBN: 3*3*m矩阵，表示姿态的方向余弦矩阵
% CBG: 3*3*m矩阵，表示姿态的方向余弦矩阵
% phi: 长度为m的列向量，滚动角，单位rad，具体定义参见dcm2rollpityawhead函数
% theta: 长度为m的列向量，俯仰角，单位rad，具体定义参见dcm2rollpityawhead函数
% psi: 长度为m的列向量，偏航角，单位rad，具体定义参见dcm2rollpityawhead函数
% psiTrue: 长度为m的列向量，真北方位角，单位rad，具体定义参见dcm2rollpityawhead函数
% alpha: 长度为m的列向量，游移方位角，单位rad

[L, lambda, alpha] = dcm2latlonwangle(CNE);
vG = navvel2geovel(vN, alpha);
CBN = NaN(size(CBL));
CBG = NaN(size(CBL));
CLN = [0 1 0; 1 0 0; 0 0 -1]';
CNG = angle2dcm_cg(-alpha, zeros(size(alpha)), zeros(size(alpha)));
for i=1:size(CBL, 3)
    CBN(:, :, i) = CLN * CBL(:, :, i);
    CBG(:, :, i) = CNG(:, :, i) * CBN(:, :, i);
end
[ phi, theta, psi, psiTrue ] = dcm2rollpityawhead( CBL, alpha, 0 );
end