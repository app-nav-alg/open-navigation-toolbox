function [ earthPar ] = calcearthpar_pgs( CNE, h, vNx,  navCoordType, useExtGravity, extGrav, cst ) %#codegen
%CALCEARTHPAR_PGS 计算地球相关参数
%
% Tips:
% 1. E1系EUGw，E系GwEN，初始N系ENU
%
% Input Arguments:
% CNE: 位置矩阵，具体定义参见函数dcm2latlonwangle
% h: 高度
% vNx: 导航系下x轴速度分量
% navCoordType: 导航坐标系类型，参见sinistep_pgs函数cfg.navCoordType说明
% useExtGravity: 是否使用外部重力加速度
% extGrav: 使用外部重力加速度时的重力加速度值
% cst: 常量结构体
%
% Output Arguments:
% earthPar: 地球参数结构体：FCN、rhoNz、gN、omegaIEN
%
% References:
% [1] Paul G. Savage. Strapdown Analytics[M]. Maple Plain: Strapdown Associates, Inc., 2000

%%
% E系定义转换，输入为GwEN，参考文献[1]使用EUGw
CNE1 = [0 1 0; 0 0 1; 1 0 0]*CNE;

[earthPar.FCN, RSa, rl, feh] = curvemat(CNE1, h, cst.RE, cst.f); % REF1式5.3-18
switch navCoordType
    case NavCoordType.GEO % 地理坐标系
        cosls = CNE1(2, 1)^2 + CNE1(2, 2)^2; % REF1式4.4.2.1-2
        tanl = CNE1(2, 3) / sqrt(cosls); % REF1式4.4.2.1-3
        earthPar.rhoNz = verttransrate_geographic(vNx, cosls, tanl, rl, feh);
    case NavCoordType.FREE_AZM % 自由方位坐标系
        earthPar.rhoNz = verttransrate_freeazm(cst.omegaIE, CNE1(2, 3));
    otherwise % 游移方位坐标系
        earthPar.rhoNz = 0;
end
if useExtGravity
    earthPar.gN = [0, 0, -extGrav]';
else
    earthPar.gN = gravity_krb(CNE1, h, cst.RE, cst.f, cst.mu, cst.J2, cst.J3, cst.omegaIE, RSa);
end
earthPar.omegaIEN = earthrate(CNE, cst.omegaIE);
end