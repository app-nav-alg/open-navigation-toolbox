function [ earthPar ] = calcearthpar_qyy( CNE, h, vNx, navCoordType, useExtGravity, extGrav, cst ) %#codegen
%CALCEARTHPAR_QYY 按照秦永元老师课本使用的地球相关参数模型计算
%
% Tips:
% 1. E系EUGw，初始N系ENU
%
% Input Arguments:
% CNE: 位置矩阵，具体定义参见函数dcm2latlonwangle
% h: 高度
% vNx: 导航系下x轴速度分量
% navCoordType: 导航坐标系类型，参见sinistep_pgs函数cfg.navCoordType说明
% useExtGravity: 是否使用外部重力加速度
% extGrav: 使用外部重力加速度时的重力加速度值
% cst: 常量结构体
%
% Output Arguments:
% earthPar: 地球参数结构体：FCN、rhoNz、gN、omegaIEN
%
% References:
% [1] 秦永元. 惯性导航[M]. 科学出版社. 2006
% [2] Paul G. Savage. Strapdown Analytics[M]. Maple Plain: Strapdown Associates, Inc., 2000

%%
sinL = CNE(2, 3);
sinL2 = sinL*sinL;
[RM, RN] = earthradiiofcurv_qyy(sinL, cst.RE, cst.f);
RMh = RM + h;
RNh = RN + h;

if navCoordType == NavCoordType.GEO
    sinalpha = 0;
    cosalpha = 1;
else
    sinalpha = CNE(2,1)/sqrt(1-sinL2);
    cosalpha = CNE(2,2)/sqrt(1-sinL2);
end
FC11 = 1/RMh + (1/RNh - 1/RMh)*sinalpha^2;
FC12 = (1/RNh - 1/RMh)*sinalpha*cosalpha;
FC21 = FC12;
FC22 = 1/RMh + (1/RNh - 1/RMh)*cosalpha^2;
earthPar.FCN = [FC11 FC12 0; FC21 FC22 0; 0 0 0]; % REF2式5.3.11

switch navCoordType
    case NavCoordType.GEO % 地理坐标系
        cosls = CNE(2, 1)^2 + CNE(2, 2)^2; % REF2式4.4.2.1-2
        tanl = CNE(2, 3) / sqrt(cosls); % REF2式4.4.2.1-3
        earthPar.rhoNz = vNx * tanl / RNh;
    case NavCoordType.FREE_AZM % 自由方位坐标系
        earthPar.rhoNz = -cst.omegaIE * sinL;
    otherwise % 游移方位坐标系
        earthPar.rhoNz = 0;
end
if useExtGravity
    earthPar.gN = [0, 0, -extGrav]';
else
    earthPar.gN = gravity_qyy(CNE, h, cst.RE);
end
earthPar.omegaIEN = earthrate(CNE, cst.omegaIE);
end