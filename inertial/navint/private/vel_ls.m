function [ vN_n, eVC3_n ] = vel_ls( vN_n, Tn, delh_n, eVC3_n1, C, cst ) %#codegen
%VEL_LS 低速速度解算
%
% Input Arguments:
% vN_n: 3*1向量，加入垂直通道阻尼之前的速度，单位m/s
% Tn: 标量，低速循环的周期，单位s
% delh_n: 标量，高度误差信号，单位m
% eVC3_n1: 标量，前一个时刻垂直通道控制信号之一
% C: 1*3向量，高度通道反馈增益系数，分别反馈至速度二次微分、速度微分、高度微分
% cst: 结构体，常量
%
% Output Arguments:
% vN_n: 3*1向量，加入垂直通道阻尼之后的速度，单位m/s
% eVC3_n: 标量，垂直通道控制信号之一
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本1.3 %

% REF1式8.38
eVC3_n = eVC3_n1 + C(1, 1)*delh_n*Tn;
eVC1_n = eVC3_n + C(1, 2)*delh_n;
vN_n = vN_n - eVC1_n*Tn*cst.uZNN; % REF1式8.37
end