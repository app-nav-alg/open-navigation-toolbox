function [ DrRot_m ] = DrRot_m_1stodform( supsilon_m, salpha_m, upsilon_m, alpha_m ) %#codegen
%DRROT_M_1STODFORM 位置旋转补偿项的1阶近似形式
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

DrRot_m = (cross(salpha_m, upsilon_m) + cross(alpha_m, supsilon_m)) / 6; % REF1式8.80
end