function [ CB_mL_n ] = att_ls_dcm( CB_mL_n1, sumDrN_m, earthPar_n, earthPar_n1, Tn, cst, useNormOrthoInAttCalc, fcnRV2DCM ) %#codegen
%ATT_LS_DCM 低速姿态解算（方向余弦矩阵法）
%
% Input Arguments:
% CB_mL_n1: 初始条件
% sumDrN_m: 梯形算法位移增量
% earthPar_n: 地球参数结构体变量
% Tn: 低速循环的周期，单位s
% cst: 常量结构体
% useNormOrthoInAttCalc: 方向余弦矩阵规范正交化
% fcnRV2DCM: 由旋转矢量计算方向余弦矩阵的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

%%
% 内插公式，REF1式8.29
omegaIEN_nh = (earthPar_n.omegaIEN + earthPar_n1.omegaIEN) / 2;
rhoNz_nh = (earthPar_n.rhoNz + earthPar_n1.rhoNz) / 2;
FCN_nh = (earthPar_n.FCN + earthPar_n1.FCN) / 2; 
zeta_n = cst.CNL * (omegaIEN_nh*Tn + rhoNz_nh*cst.uZNN*Tn + FCN_nh*cross(cst.uZNN, sumDrN_m)); % REF1式8.28
CL_n1L_n = fcnRV2DCM(-zeta_n'); % REF1式8.27
CB_mL_n = CL_n1L_n * CB_mL_n1; % REF1式8.1

if useNormOrthoInAttCalc
    CB_mL_n = dcmnormortho_order(CB_mL_n); % 规范化及正交化
end
end