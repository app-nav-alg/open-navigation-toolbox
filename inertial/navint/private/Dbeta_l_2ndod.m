function [ Dbeta_l ] = Dbeta_l_2ndod( alpha_l1, Dalpha_lx ) %#codegen
%DBETA_L_2NDOD 圆锥补偿项的二阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

Dbeta_l = cross((alpha_l1 + Dalpha_lx(:, 2)/6), Dalpha_lx(:, 1)) / 2; % REF1式8.23
end