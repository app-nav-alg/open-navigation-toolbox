function [ Dsalpha_l ] = Dsalpha_l_2ndod( alpha_l1, Dalpha_lx, Tl ) %#codegen
%DSALPHA_L_2NDOD 角速度双重积分项的二阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

Dsalpha_l = alpha_l1*Tl + (5*Dalpha_lx(:, 1)+Dalpha_lx(:, 2))*Tl/12; % REF1式8.90
end