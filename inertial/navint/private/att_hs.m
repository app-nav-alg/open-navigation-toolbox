function [ alpha_l, beta_l ] = att_hs( alpha_l1, Dalpha_lx, beta_l1, fcnDbeta_l) %#codegen
%ATT_HS 高速姿态解算
%
% Input Arguments:
% Dalpha_lx: 3*n矩阵，第1列为Dalpha_l，第2列为Dalpha_l-1，依此类推
% fcnDbeta_l: 计算Dbeta_l的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

alpha_l = alpha_l1 + Dalpha_lx(:, 1); % REF1式8.15
Dbeta_l = fcnDbeta_l(alpha_l1, Dalpha_lx);
beta_l = beta_l1 + Dbeta_l; % REF1式8.13
end