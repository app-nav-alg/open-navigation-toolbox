function [ CNE_n, h_n, delh_n ] = pos_ls( CNE_n1, h_n1, sumDrN_m, earthpar_n1, earthpar_n2, Tn, hAltm_n, C, cst, fcnRV2DCM ) %#codegen
%POS_LS 低速位置解算
%
% Input Arguments:
% CNE_n1: 前一时刻位置矩阵
% h_n1: 前一时刻高度
% sumDrN_m: 累计位移增量
% earthpar_n1, earthpar_n2: 地球参数结构体：FCN, rhoNz, gN, omegaIEN
% Tn: 低速循环的周期，单位s
% fcnRV2DCM: 由旋转矢量计算方向余弦矩阵的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本1.3 %

%%
Dh_n = dot(cst.uZNN, sumDrN_m); % REF1式8.70

% REF1式8.75
rhoNz_nh = (3*earthpar_n1.rhoNz -earthpar_n2.rhoNz) / 2;
FCN_nh = (3*earthpar_n1.FCN -earthpar_n2.FCN) / 2;
xi_n = rhoNz_nh*cst.uZNN*Tn + FCN_nh*cross(cst.uZNN, sumDrN_m);
CN_nN_n1 = fcnRV2DCM(xi_n'); % REF1式8.74

h_n = h_n1 + Dh_n; % REF1式8.70
% REF1式8.72
delh_n = h_n - hAltm_n;
if any(C)
    eVC2_n = C(1, 3) * delh_n;
    h_n = h_n - eVC2_n*Tn; % REF1式8.71
end

CNE_n = CNE_n1 * CN_nN_n1; % REF1式8.73
end