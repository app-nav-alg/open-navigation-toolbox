function [ DrScrl_l, supsilon_l, salpha_l ] = pos_hs( DrScrl_l1, supsilon_l1, salpha_l1, DvScul_l1, upsilon_l1, Dupsilon_lx, alpha_l1, Dalpha_lx, Tl, fcndrScrl_l, fcnDsupsilon_l, fcnDsalpha_l ) %#codegen
%POS_HS 高速位置解算
%
% Input Arguments:
% Dupsilon_lx: 3*n矩阵，第1列为Dupsilon_l，第2列为Dupsilon_l-1，依此类推
% Dalpha_lx: 3*n矩阵，第1列为Dalpha_l，第2列为Dalpha_l-1，依此类推
% fcndrScrl_l: 计算fcndrScrl_l的函数句柄
% fcnDsupsilon_l: 计算fcnDsupsilon_l的函数句柄
% fcnDsalpha_l: 计算fcnDsalpha_l的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

% REF1式8.88
Dsalpha_l = fcnDsalpha_l(alpha_l1, Dalpha_lx, Tl);
salpha_l = salpha_l1 + Dsalpha_l;
Dsupsilon_l = fcnDsupsilon_l(upsilon_l1, Dupsilon_lx, Tl);
supsilon_l = supsilon_l1 + Dsupsilon_l;

% REF1式8.86
drScrl_l = fcndrScrl_l(supsilon_l1, Dsupsilon_l, salpha_l1, Dsalpha_l, DvScul_l1, upsilon_l1, Dupsilon_lx, alpha_l1, Dalpha_lx, Tl);
DrScrl_l = DrScrl_l1 + drScrl_l;
end