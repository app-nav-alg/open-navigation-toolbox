function [ DvScul_l, upsilon_l ] = vel_hs( DvScul_l1, upsilon_l1, Dupsilon_lx, alpha_l1, Dalpha_lx, fcndvScul_l ) %#codegen
%VEL_HS 高速速度解算
%
% Input Arguments:
% Dupsilon_lx: 3*n矩阵，第1列为Dupsilon_l，第2列为Dupsilon_l-1，依此类推
% Dalpha_lx: 3*n矩阵，第1列为Dalpha_l，第2列为Dalpha_l-1，依此类推
% fcndvScul_l: 计算dvScul_l的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

upsilon_l = upsilon_l1 + Dupsilon_lx(:, 1); % REF1式8.61
dvScul_l = fcndvScul_l(upsilon_l1, Dupsilon_lx, alpha_l1, Dalpha_lx); % REF1式8.63
DvScul_l = DvScul_l1 + dvScul_l; % REF1式8.59
end