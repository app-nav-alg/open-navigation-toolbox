function [ dvScul_l ] = dvScul_l_2ndod( upsilon_l1, Dupsilon_lx, alpha_l1, Dalpha_lx ) %#codegen
%DVSCUL_L_2NDOD 划桨补偿项的二阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

dvScul_l = (cross((alpha_l1 + Dalpha_lx(:, 2)/6), Dupsilon_lx(:, 1)) + cross((upsilon_l1 + Dupsilon_lx(:, 2)/6), Dalpha_lx(:, 1))) / 2; % REF1式8.63
end