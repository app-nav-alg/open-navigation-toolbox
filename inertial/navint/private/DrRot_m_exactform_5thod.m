function [ DrRot_m ] = DrRot_m_exactform_5thod( supsilon_m, salpha_m, upsilon_m, alpha_m ) %#codegen
%DRROT_M_EXACTFORM_5THOD 位置旋转补偿项精确形式的5阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

alphanorm = norm(alpha_m);
DrRot_m = ((alphanorm^4/5040 - alphanorm^2/120 + 1/6)*eye(3) + (alphanorm^4/40320 - alphanorm^2/720 + 1/24)*skewsymmat(alpha_m)) ...
    * (cross(salpha_m, upsilon_m) + cross(alpha_m, supsilon_m)); % REF1式8.85
end