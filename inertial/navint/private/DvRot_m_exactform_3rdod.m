function [ DvRot_m ] = DvRot_m_exactform_3rdod( upsilon_m, alpha_m ) %#codegen
%DVROT_M_EXACTFORM_3RDOD 速度旋转补偿项精确形式的3阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

alphanorm = norm(alpha_m);
DvRot_m = (-alphanorm^2/24 + 1/2) * cross(alpha_m, upsilon_m) + (-alphanorm^2/120 + 1/6) * cross(alpha_m, cross(alpha_m, upsilon_m)); % REF1式8.57
end