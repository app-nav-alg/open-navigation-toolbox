function [ Dsupsilon_l ] = Dsupsilon_l_2ndod( upsilon_l1, Dupsilon_lx, Tl ) %#codegen
%DSUPSILON_L_2NDOD 比力双重积分项的二阶算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

Dsupsilon_l = upsilon_l1*Tl + (5*Dupsilon_lx(:, 1)+Dupsilon_lx(:, 2))*Tl/12; % REF1式8.90
end