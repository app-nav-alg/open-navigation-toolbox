function [ qB_mL_n ] = att_ls_quat( qB_mL_n1, sumDrN_m, earthPar_n, earthPar_n1, Tn, cst, useNormOrthoInAttCalc, fcnRV2Quat ) %#codegen
%ATT_LS_QUAT 低速姿态解算（四元数法）
%
% Input Arguments:
% qB_mL_n1: 初始条件
% sumDrN_m: 梯形算法位移增量
% earthPar_n: 地球参数结构体变量
% Tn: 低速循环的周期，单位s
% cst: 常量结构体
% useNormOrthoInAttCalc: 四元数规范化
% fcnRV2Quat: 由旋转矢量计算四元数的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

%%
% 内插公式，REF1式8.29
omegaIEN_nh = (earthPar_n.omegaIEN + earthPar_n1.omegaIEN) / 2;
rhoNz_nh = (earthPar_n.rhoNz + earthPar_n1.rhoNz) / 2;
FCN_nh = (earthPar_n.FCN + earthPar_n1.FCN) / 2; 
zeta_n = cst.CNL * (omegaIEN_nh*Tn + rhoNz_nh*cst.uZNN*Tn + FCN_nh*cross(cst.uZNN, sumDrN_m)); % REF1式8.28
qL_n1L_n = fcnRV2Quat(-zeta_n'); % REF1式8.34
qB_mL_n = quatmultiply(qL_n1L_n, qB_mL_n1); % REF1式8.32

if useNormOrthoInAttCalc
    qB_mL_n = quatnormalize_pgs(qB_mL_n); % 四元数规范化，经实测精度比MATLAB Aerospace工具箱的quatnormalize()稍高
end
end