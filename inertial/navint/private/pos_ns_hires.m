function [ DrN_m, sumDrN_m ] = pos_ns_hires( sumDrN_m1, DrScrl_m, supsilon_m, salpha_m, upsilon_m, alpha_m, vN_m1, DvSFLI_n1_m, DvGCorN_m, CL_n1L_m, CL_n1L_m1, CB_m1L_n1, Tm, cst, fcnDrRot_m ) %#codegen
%POS_NS_HIRES 中速位置解算，高精度位置算法
%
% Input Arguments:
% fcnDrRot_m: 计算DrRot_m的函数句柄
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

DrRot_m = fcnDrRot_m(supsilon_m, salpha_m, upsilon_m, alpha_m); 
DrSFB_m = supsilon_m + DrRot_m + DrScrl_m; % REF1式8.80
DrSFL_m = (CL_n1L_m-CL_n1L_m1)*DvSFLI_n1_m*Tm/6 + CL_n1L_m1*CB_m1L_n1*DrSFB_m; % REF1式8.76
DrN_m = (vN_m1 + DvGCorN_m/2)*Tm + cst.CNL'*DrSFL_m; % REF1式8.73
sumDrN_m = sumDrN_m1 + DrN_m;
end