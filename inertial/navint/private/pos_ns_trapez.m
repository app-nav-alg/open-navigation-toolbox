function [ DrN_m, sumDrN_m ] = pos_ns_trapez( sumDrN_m1, vN_m, vN_m1, Tm ) %#codegen
%POS_NS_TRAPEZ 中速位置解算，梯形积分位置算法
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

DrN_m = (vN_m+vN_m1) * Tm / 2; % REF1式8.31
sumDrN_m = sumDrN_m1 + DrN_m;
end