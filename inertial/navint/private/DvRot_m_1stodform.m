function [ DvRot_m ] = DvRot_m_1stodform( upsilon_m, alpha_m ) %#codegen
%DVROT_M_1STODFORM 速度旋转补偿项的1阶近似形式
%
% References:
% [1] 理论文档“捷联惯性导航数值积分算法”， % 青鸟版本0.6 %

DvRot_m = cross(alpha_m, upsilon_m) / 2; % REF1式8.54
end