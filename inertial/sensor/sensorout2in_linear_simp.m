function [in] = sensorout2in_linear_simp(DalphaTildeOutSl, KScal0, domegaIBBiasS, dKScalP, dPSS0, f, DGSens, KScal0n) %#codegen
%SENSOROUT2IN_LINEARSIMP 将输出值按线性化法转换为输入值
%
% Tips:
% 1. 对增量计算，传入的domegaIBBiasS需要乘以采样周期
%
% Input Arguments:
% DalphaTildeOutSl: 3*n矩阵，n为采样个数，器件输出值
% KScal0: 3*1向量，（正向）标度因数
% domegaIBBiasS: 3*1向量，零偏
% dKScalP: 3*1向量，标定因数误差
% dPSS0: 3*3矩阵，失准角（安装误差）
% f: 3*n矩阵，比力，空矩阵或不输入时默认全为0
% DGSens: 3*3矩阵，g敏感项系数，空矩阵或不输入时默认全为0
% KScal0n: 3*1向量，负向标度因数，默认与KScal0相同
%
% Output Arguments:
% in: 3*n矩阵，经补偿后的各采样的器件输入值

if nargin < 8
    KScal0n = [];
end
if (nargin<7) || isempty(DGSens)
    DGSens = zeros(3);
end
if (nargin<6) || isempty(f)
    f = zeros(size(DalphaTildeOutSl));
end

n = size(DalphaTildeOutSl, 2);
KScal0 = repmat(KScal0, 1, n);
if ~isempty(KScal0n)
    KScal0n = repmat(KScal0n, 1, n);
    ind = (DalphaTildeOutSl<0);
    KScal0(ind) = KScal0n(ind);
end
dSFMA = diag(dKScalP) + dPSS0';
in = (eye(3)+dSFMA) \ (DalphaTildeOutSl./KScal0 - repmat(domegaIBBiasS, 1, size(DalphaTildeOutSl, 2)) - DGSens*f);
end