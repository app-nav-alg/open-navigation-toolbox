function [ dfAnisoSInt ] = accanisoerr_inc( KAniso, CBA, DalphaBl, DalphaBl1, Tl )
%ACCANISOERR_INC 计算加速度计三元组增量形式的不等惯量误差
%
% Tips
% # 本函数针对增量形式，该误差仅适用于摆式加速度计
%
% Input Arguments
% # KAniso: 3*1向量，3只加速度计不等惯量误差的系数，单位：(m/s^2)/(rad/s)^2
% # CBA: 3*3*3矩阵，本体系到3只加速度计坐标系的坐标转换矩阵，无单位
% # DalphaBl: 3*1向量，角度增量，单位：rad
% # DalphaBl1: 3*1向量，前一周期的角度增量，单位：rad
% # Tl: 标量，单位：s
%
% Output Arguments
% # dfAnisoSInt：3*1向量，加速度计三元组增量形式的不等惯量误差，单位：m/s
%
% References
% # 《应用导航算法工程基础》“惯性传感器模型”→“加速度计不等惯量误差”

dfAnisoSInt = [accanisoerrelem_inc(KAniso(1), CBA(:, :, 1), DalphaBl, DalphaBl1, Tl);
    accanisoerrelem_inc(KAniso(2), CBA(:, :, 2), DalphaBl, DalphaBl1, Tl);
    accanisoerrelem_inc(KAniso(3), CBA(:, :, 3), DalphaBl, DalphaBl1, Tl)];
end

function [ dfAnisoSIntk ] = accanisoerrelem_inc( KAnisok, CBAk, DalphaBl, DalphaBl1, Tl )
%ACCANISOERR_INC_COM 计算加速度计三元组增量形式的不等惯量误差的分量
%
% Tips:
% # 本函数针对增量形式，该误差仅适用于摆式加速度计
%
% Input Arguments:
% # KAnisok: KAniso的第k个元素
% # CBAk: CBA的第k页
% # DalphaBl: 3*1向量，角度增量，单位：rad
% # DalphaBl1: 3*1向量，前一周期的角度增量，单位：rad
% # Tl: 标量，单位：s
%
% Output Arguments:
% # dfAnisoSIntk：dfAnisoSInt的第k个分量
%
% References:
% # 《应用导航算法工程基础》“惯性传感器模型”→“加速度计不等惯量误差”

dfAnisoSIntk = KAnisok*(13/(12*Tl)*(CBAk(1, :)*DalphaBl)*(CBAk(3, :)*DalphaBl) ...
    - 1/(12*Tl)*(CBAk(1, :)*DalphaBl)*(CBAk(3, :)*DalphaBl1) ...
    - 1/(12*Tl)*(CBAk(1, :)*DalphaBl1)*(CBAk(3, :)*DalphaBl) ...
    + 1/(12*Tl)*(CBAk(1, :)*DalphaBl1)*(CBAk(3, :)*DalphaBl1));
end