function [RWC, sigmaOmega, SNR, VRate, sigmahvSq, sigmasvSq, sigmarvSq] = randwalkcoef_theor_ifog(D, L, T, lambda, Dlambda, RD, idark, R, P0, phib)
%RANDWALKCOEF_THEOR_IFOG 干涉型光纤陀螺的随机游走系数理论值
%
% Input Arguments
% # D: 标量，光纤线圈直径，单位mm
% # L: 标量，光纤线圈长度，单位m
% # T: 标量，温度，单位℃，默认30
% # lambda: 标量，光源平均波长，单位nm，默认1310
% # Dlambda: 标量，光谱宽度，单位nm，默认30
% # RD: 标量，探测器响应度，单位A/W，默认0.9
% # idark: 标量，探测器暗电流，单位nA，默认5
% # R: 标量，探测器跨阻抗，单位kΩ，默认100
% # P0: 标量，光源尾纤输出功率，单位μW，默认10/(1+cos(phib))
% # phib: 标量，相位偏置工作点，单位rad，默认pi/2
%
% Output Arguments
% # RWC: 标量，随机游走系数，单位°/sqrt(h)
% # sigmaOmega: 标量，百秒采样角速度噪声标准差，单位°/h
% # SNR: 标量，信噪比，单位Hz/rad^2
% # VRate: 标量，灵敏度，单位V/rad
% # sigmasvSq: 标量，散粒噪声单位带宽方差，单位V^2/Hz
% # sigmahvSq: 标量，热噪声单位带宽方差，单位V^2/Hz
% # sigmarvSq: 标量，相对强度噪声单位带宽方差，单位V^2/Hz
%
% Assumptions and Limitations
% # 在计算噪声方差时Df=1Hz
%
% References
% # 王巍《干涉型光纤陀螺仪技术》第1版 3.4.2节

if nargin < 10
    phib = pi/2;
end
if nargin < 9
    P0 = 10/(1+cos(phib));
end
if nargin < 8
    R = 100;
end
if nargin < 7
    idark = 5;
end
if nargin < 6
    RD = 0.9;
end
if nargin < 5
    Dlambda = 30;
end
if nargin < 4
    lambda = 1310;
end
if nargin < 3
    T = 30;
end

kB = 1.38e-23; % 玻耳兹曼常数，J/K
e = 1.6e-19; % 电子电量，C=A*s
c = lightspeed; % 真空中的光速，m/s

% 单位转换为国际单位制
D = D * 1e-3;
T = T + 273.15; % 单位转换为K
lambda = lambda * 1e-9;
Dlambda = Dlambda * 1e-9;
idark = idark * 1e-9;
R = R * 1e3;
P0 = P0 * 1e-6;

VRate = -R * RD * P0 * sin(phib);
P = P0 * (1+cos(phib));
sigmasvSq = 2 * e * R^2 * (idark + RD*P);
sigmahvSq = 4 * kB * T * R;
sigmarvSq = (R * RD * P * lambda)^2 / (c * Dlambda);
SNR = VRate^2 / (sigmasvSq + sigmahvSq + sigmarvSq);
angRate2PhaseShiftRatio = 2*pi*L*D / (lambda*c); % s
RWC = 1/sqrt(SNR) / angRate2PhaseShiftRatio; % rad/sqrt(s)
RWC = RWC / pi * 180 * 60; % 单位转换为°/sqrt(h) TODO: 与参考文献表3-1计算结果不同
sigmaOmega = RWC/sqrt(100/3600);
end