function [ fullStructPar ] = makeupimupar( structPar, setNonExistParTypeToNaN )
%MAKEUPIMUPAR 补全并排序包含加速度计和陀螺三元组的惯组的结构体形式的参数中的域
%
% Tips
% # 域名及顺序：加速度计，KScal0、PS0B、dBias、dKScalP、dKScalN、dPSS0、dQuant、lB、KAniso、CBA
%               陀螺，KScal0、PS0B、dBias、dKScalP、dKScalN、dPSS0、dQuant、DGSens
%
% Input Arguments
% # structPar: 包含结构体acc和gyro的结构体
% # setNonExistParTypeToNaN: 逻辑标量，true表示缺少的参数类型域设置为NaN，否则标度因数设置为1，其它类型参数设置为0，默认为false
%
% Output Arguments
% # fullStructPar: 包含结构体acc和gyro结构体，acc与gyro域已被补全并排序

if nargin < 2
    setNonExistParTypeToNaN = false;
end

fullStructPar = struct('acc', makeupacctriadpar(structPar.acc, setNonExistParTypeToNaN), 'gyro', makeupgyrotriadpar(structPar.gyro, setNonExistParTypeToNaN));
end