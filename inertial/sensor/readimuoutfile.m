function [ accOut, gyroOut, t, flag ] = readimuoutfile( filePath, bgnRowNum, endRowNum, accCols, gyroCols, tCol, flagCol )
%READIMUOUTFILE 从文件中读加速度计和陀螺输出和采样时间
%
% Input Arguments
% # filePath: 字符串，惯组输出数据文件的路径
% # bgnRowNum: 标量，开始读取行数，以0为基数
% # endRowNum: 标量，结束读取行数，以0为基数，为空表示读取到最后一行
% # accCols: 1*3向量，分别为X、Y、Z加速度计输出在文件中所占的列数，以1为基数，负数表示对相应列取负值
% # gyroCols: 1*3向量，分别为X、Y、Z陀螺输出在文件中所占的列数，以1为基数，负数表示对相应列取负值
% # tCol: 标量，时间在文件中所占的列数，以1为基数，默认为0（不存在）
% # flagCol: 标量，标志位在文件中所占的列数，以1为基数，默认为0（不存在）
%
% Output Arguments
% # accOut、gyroOut: 列数为3的矩阵，分别代表X、Y、Z器件
% # t: 列向量，时间
% # flag: 列向量，标志位

if nargin < 7
    flagCol = 0;
end
if nargin < 6
    tCol = 0;
end

if ~isempty(endRowNum)
    M = dlmread(filePath, '', [bgnRowNum, 0, endRowNum, max([max(abs(accCols)), max(abs(gyroCols)), tCol, flagCol])-1]);
else
    M = dlmread(filePath, '', bgnRowNum, 0);
end
accOut = repmat(sign(accCols), size(M, 1), 1) .* M(:, abs(accCols));
gyroOut = repmat(sign(gyroCols), size(M, 1), 1) .* M(:, abs(gyroCols));
if tCol > 0
    t = M(:, tCol);
else
    t = [];
end
if flagCol > 0
    flag = M(:, flagCol);
else
    flag = [];
end
end