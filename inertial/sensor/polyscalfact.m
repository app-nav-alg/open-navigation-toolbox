function [ dKScal, KScal ] = polyscalfact(vTildeScalS, dKScalCoef, KScal0, Tl)
%POLYSCALFACT 计算KScal(vTildeScalS)
%
% Input Arguments
% # vTildeScalS: 3*1向量，传感器坐标系下进入标度因数缩放环节的三传感器组测量值（或对应的Tl积分增量）
% # dKScalCoef: 3*x矩阵，x为阶数，统一正负向之后的标度因数误差多项式系数
% # KScal0: 3*1向量，标度因数的标称值，默认为ones(3, 1)
% # Tl: 标量，采样周期（当vTildeScalS为增量时），单位s，默认为空（瞬时值）
%
% Output Arguments
% # dKScal: 3*1向量，标度因数误差
% # KScal: 3*1向量，标度因数
%
% References
% # 《应用导航算法工程基础》“惯性传感器模型” 标度因数误差

if nargin < 4
    Tl = [];
end
if nargin < 3
    KScal0 = ones(3, 1, class(vTildeScalS));
end

% 对于增量值，1次及以上系数需要除以Tl的相应次数
if ~isempty(Tl)
    for idKScal=2:size(dKScalCoef, 2)
        dKScalCoef(:, idKScal) = dKScalCoef(:, idKScal) / (Tl^(idKScal-1));
    end
end

dKScal = zeros(3, 1);
for idKScal=1:size(dKScalCoef, 2)
    dKScal = dKScal + dKScalCoef(:, idKScal).*(vTildeScalS.^(idKScal-1));
end
if nargout > 1
    KScal = KScal0.*(ones(3, 1)+dKScal);
end
end