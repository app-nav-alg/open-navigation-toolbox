function [ accPar1 ] = convaccpar( accPar, inIsConvent, g )
% CONVACCPAR 加速度计三元组的确定性误差参数的单位转换
%
% Input Arguments
% # accPar: 各域为数组的结构体，加速度计的确定性误差参数，标准单位/常用单位
% # inIsConvent：逻辑标量，默认为true，true表示输入为常用单位，向标准单位转换；false表示输入为标准单位，向常用单位转换
% # g: 标量，重力加速度，单位：m/s^2，默认为9.8
%
% Output Arguments
% # accPar1: 各域为数组的结构体，加速度计的确定性误差参数，常用单位/标准单位

if nargin < 3
    g = 9.8;
end
if nargin < 2
    inIsConvent = true;
end

accPar1 = accPar;
if inIsConvent
    % 常用单位→国际单位
    accPar1.KScal0 = accPar.KScal0/g; % (^/s)/g→(^/s)/（m/s^2）
    accPar1.dfBiasS = 1e-6*g*accPar.dfBiasS; % μg→m/s^2
    accPar1.dKScalP(:, 1, :) = 1e-6*accPar.dKScalP(:, 1, :); % ppm→无单位
    if size(accPar.dKScalP, 2) >= 2
        accPar1.dKScalP(:, 2, :) = 1e-6*accPar.dKScalP(:, 2, :)/g; % ppm/g→(m/s^2)^-1
        if size(accPar.dKScalP, 2) == 3
            accPar1.dKScalP(:, 3, :) = 1e-6*accPar.dKScalP(:, 3, :)/(g^2); % ppm/g^2→(m/s^2)^-2
        else
            warning('convaccpar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    accPar1.dKScalN(:, 1, :) = 1e-6*accPar.dKScalN(:, 1, :); % ppm→无单位
    if size(accPar.dKScalN, 2) >= 2
        accPar1.dKScalN(:, 2, :) = 1e-6*accPar.dKScalN(:, 2, :)/g; % ppm/g→(m/s^2)^-1
        if size(accPar.dKScalN, 2) == 3
            accPar1.dKScalN(:, 3, :) = 1e-6*accPar.dKScalN(:, 3, :)/(g^2); % ppm/g^2→(m/s^2)^-2
        else
            warning('convaccpar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    accPar1.dPSS0 = 1e-6*accPar.dPSS0; % μrad→rad
    accPar1.dfQuantS = 1e-6*g*accPar.dfQuantS; % μg→m/s^2
    accPar1.lB = 1e-3*accPar.lB; % mm→m
    accPar1.KAniso = 1e-6*g*180/pi*accPar.KAniso; % μg/(°/s)^2→m/s^2/(rad/s)^2
else
    % 国际单位→常用单位
    accPar1.KScal0 = g*accPar.KScal0; % (^/s)/（m/s^2）→(^/s)/g
    accPar1.dfBiasS = 1e6/g*accPar.dfBiasS; % m/s^2→μg
    accPar1.dKScalP(:, 1, :) = 1e6*accPar.dKScalP(:, 1, :); % 无单位→ppm
    if size(accPar.dKScalP, 2) >= 2
        accPar1.dKScalP(:, 2, :) = 1e6*g*accPar.dKScalP(:, 2, :); % (m/s^2)^-1→ppm/g
        if size(accPar.dKScalP, 2) == 3
            accPar1.dKScalP(:, 3, :) = 1e6*g^2*accPar.dKScalP(:, 3, :); % (m/s^2)^-2→ppm/g^2
        else
            warning('convaccpar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    accPar1.dKScalN(:, 1, :) = 1e6*accPar.dKScalN(:, 1, :); % 无单位→ppm
    if size(accPar.dKScalN, 2) >= 2
        accPar1.dKScalN(:, 2, :) = 1e6*g*accPar.dKScalN(:, 2, :); % (m/s^2)^-1→ppm/g
        if size(accPar.dKScalN, 2) == 3
            accPar1.dKScalN(:, 3, :) = 1e6*g^2*accPar.dKScalN(:, 3, :); % (m/s^2)^-2→ppm/g^2
        else
            warning('convaccpar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    accPar1.dPSS0 = 1e6*accPar.dPSS0; % rad→μrad
    accPar1.dfQuantS = 1e6/g*accPar.dfQuantS; % m/s^2→μg
    accPar1.lB = 1e3*accPar.lB; % m→mm
    accPar1.KAniso = 1e6/g/180*pi*accPar.KAniso; % m/s^2/(rad/s)^2→μg/(°/s)^2
end
end