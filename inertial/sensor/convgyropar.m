function [ gyroPar1 ] = convgyropar( gyroPar, inIsConvent, g )
% CONVGYROPAR 陀螺三元组的确定性误差参数的单位转换
%
% Input Arguments
% # gyroPar: 各域为数组的结构体，陀螺的确定性误差参数，标准单位/常用单位
% # inIsConvent：逻辑标量，默认为true，true表示输入为常用单位，向标准单位转换；false表示输入为标准单位，向常用单位转换
% # g: 标量，重力加速度，单位：m/s^2，默认为9.8
%
% Output Arguments
% # gyroPar1: 各域为数组的结构体，陀螺的确定性误差参数，常用单位/标准单位

if nargin < 3
    g = 9.8;
end
if nargin < 2
    inIsConvent = true;
end

gyroPar1 = gyroPar;
if inIsConvent
    % 常用单位→国际单位
    gyroPar1.KScal0 = 180*3600/pi*gyroPar.KScal0; % (^/s)/(°/h)→(^/s)/（rad/s）
    gyroPar1.domegaIBBiasS = pi/180/3600*gyroPar.domegaIBBiasS; % °/h→rad/s
    gyroPar1.dKScalP(:, 1, :) = 1e-6*gyroPar.dKScalP(:, 1, :); % ppm→无单位
    if size(gyroPar.dKScalP, 2) >= 2
        gyroPar1.dKScalP(:, 2, :) = 1e-6*180/pi*gyroPar.dKScalP(:, 2, :); % ppm/(°/s) →(rad/s)^-1
        if size(gyroPar.dKScalP, 2) == 3
            gyroPar1.dKScalP(:, 3, :) = 1e-6*(180/pi)^2*gyroPar.dKScalP(:, 3, :); % ppm/(°/s)^2 →(rad/s)^-2
        else
            warning('convgyropar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    gyroPar1.dKScalN(:, 1, :) = 1e-6*gyroPar.dKScalN(:, 1, :); % ppm→无单位
    if size(gyroPar.dKScalN, 2) >= 2
        gyroPar1.dKScalN(:, 2, :) = 1e-6*180/pi*gyroPar.dKScalN(:, 2, :); % ppm/(°/s) →(rad/s)^-1
        if size(gyroPar.dKScalN, 2) == 3
            gyroPar1.dKScalN(:, 3, :) = 1e-6*(180/pi)^2*gyroPar.dKScalN(:, 3, :); % ppm/(°/s)^2 →(rad/s)^-2
        else
            warning('convgyropar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    gyroPar1.dPSS0 = 1e-6*gyroPar.dPSS0; % μrad→rad
    gyroPar1.domegaIBQuantS = pi/180/3600*gyroPar.domegaIBQuantS;  % °/h→rad/s
    gyroPar1.DGSens = pi/180/3600/g*gyroPar.DGSens; % °/h/g→rad/s/(m/s^2)
else
    % 国际单位→常用单位
    gyroPar1.KScal0 = pi/180/3600*gyroPar.KScal0; % (^/s)/（rad/s）→(^/s)/(°/h)
    gyroPar1.domegaIBBiasS = 180*3600/pi*gyroPar.domegaIBBiasS; % rad/s→°/h
    gyroPar1.dKScalP(:, 1, :) = 1e6*gyroPar.dKScalP(:, 1, :); % ppm→无单位
    if size(gyroPar.dKScalP, 2) >= 2
        gyroPar1.dKScalP(:, 2, :) = 1e6*pi/180*gyroPar.dKScalP(:, 2, :); % (rad/s)^-1→ppm/(°/s)
        if size(gyroPar.dKScalP, 2) == 3
            gyroPar1.dKScalP(:, 3, :) = 1e6*(pi/180)^2*gyroPar.dKScalP(:, 3, :); % (rad/s)^-2→ppm/(°/s)^2
        else
            warning('convgyropar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    gyroPar1.dKScalN(:, 1, :) = 1e6*gyroPar.dKScalN(:, 1, :); % ppm→无单位
    if size(gyroPar.dKScalN, 2) >= 2
        gyroPar1.dKScalN(:, 2, :) = 1e6*pi/180*gyroPar.dKScalN(:, 2, :); % (rad/s)^-1→ppm/(°/s)
        if size(gyroPar.dKScalN, 2) == 3
            gyroPar1.dKScalN(:, 3, :) = 1e6*(pi/180)^2*gyroPar.dKScalN(:, 3, :); % (rad/s)^-2→ppm/(°/s)^2
        else
            warning('convgyropar:ordertoohigh', 'The order of scale factor error should not be greater than 3.');
        end
    end
    gyroPar1.dPSS0 = 1e6*gyroPar.dPSS0; % rad→μrad
    gyroPar1.domegaIBQuantS = 180*3600/pi*gyroPar.domegaIBQuantS; % rad/s→°/h
    gyroPar1.DGSens = 180*3600/pi*g*gyroPar.DGSens; % rad/s/(m/s^2)→°/h/g
end
end