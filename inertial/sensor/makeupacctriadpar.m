function [ fullStructPar ] = makeupacctriadpar( structPar, setNonExistParTypeToNaN )
%MAKEUPACCTRIADPAR 补全并排序加速度计三元组结构体形式的参数中的域
%
% Tips
% # 域名及顺序：KScal0、PS0B、dBias、dKScalP、dKScalN、dPSS0、dQuant、lB、KAniso、CBA
%
% Input Arguments
% # structPar: 结构体，各域的时间在最后一维上
% # setNonExistParTypeToNaN: 逻辑标量，true表示缺少的参数类型域设置为NaN，否则KScal0设置为1，PS0B设置为单位阵，其它类型参数设置为0，默认为false
%
% Output Arguments
% # fullStructPar: 结构体，各域时间维尺寸与structPar中第一个域的时间维相同

if nargin < 2
    setNonExistParTypeToNaN = false;
end

fullFieldNames = {'KScal0', 'PS0B', 'dBias', 'dKScalP', 'dKScalN', 'dPSS0', 'dQuant', 'lB', 'KAniso', 'CBA'};
if setNonExistParTypeToNaN
    fullFieldValues = {NaN(3,1), NaN(3,3), NaN(3,1), NaN(3,3), NaN(3,3), NaN(3,3), NaN(3,1), NaN(3,3), NaN(3,1), NaN(3,3,3)};
else
    fullFieldValues = {ones(3,1), eye(3), zeros(3,1), zeros(3,3), zeros(3,3), zeros(3,3), zeros(3,1), zeros(3,3), zeros(3,1), zeros(3,3,3)};
end
fullStructPar = orderfields(makeupstruct( structPar, fullFieldNames, fullFieldValues), fullFieldNames);
end