classdef GyroTriadPars < InertSensTriadPars
    %GYROTRIADPARS 陀螺三元组参数，确定性误差参数各域最后一维尺寸n为采样数或分段数，默认为1
    % KScal0: 3*n向量，陀螺标度因数标称值，单位：1rad对应的脉冲数
    % PS0B：3*3*n矩阵，传感器系与本体系之间的坐标转换矩阵标称值，无单位，默认全为单位阵
    % domegaIBBiasS: 3*n向量，零偏，单位：rad/s
    % dKScalP: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，陀螺正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(rad/s)^-(i-1)，默认全为0
    % dKScalN: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，陀螺正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(rad/s)^-(i-1)，默认全为0
    % dPSS0：3*3*n矩阵，失准角（安装误差），无单位，默认全为0
    % domegaIBQuantS: 3*n向量，量化误差，单位：rad/s，默认全为0
    % DGSens: 3*3*n矩阵，g敏感项系数，单位：(rad/s)/(m/s^2)，默认全为0
    % R: 3*1向量，陀螺角速度斜坡系数，单位rad/s^2，默认为0
    % K: 3*1向量，陀螺角速度随机游走系数，单位rad/s^(3/2)，默认为0
    % B: 3*1向量，陀螺零偏不稳定性系数，单位rad/s，默认为0
    % N: 3*1向量，陀螺角度随机游走系数，单位rad/sqrt(s)，默认为0
    % Q: 3*1向量，陀螺量化噪声系数，单位rad，默认为realmin
    % qc: 3*1向量，陀螺指数相关噪声驱动白噪声的功率谱密度平方根，单位rad/s^(3/2)，默认为0
    % Tc: 3*1向量，陀螺指数相关噪声相关时间，单位s，默认为Inf
    % Omega0: 3*1向量，陀螺正弦噪声幅值，单位rad/s，默认为0
    % f0: 3*1向量，陀螺正弦噪声频率，单位Hz，默认为1
    
    properties
        KScal0(3, :) double = ones(3, 1)
        PS0B(3, 3, :) double = eye(3)
        domegaIBBiasS(3, :) double = zeros(3, 1)
        dKScalP(3, :, :) double = zeros(3, 1)
        dKScalN(3, :, :) double = zeros(3, 1)
        dPSS0(3, 3, :) double = zeros(3)
        domegaIBQuantS(3, :) double = zeros(3, 1)
        DGSens(3, 3, :) double = zeros(3)
        R(3, 1) double = zeros(3, 1)
        K(3, 1) double = zeros(3, 1)
        B(3, 1) double = zeros(3, 1)
        N(3, 1) double = zeros(3, 1)
        Q(3, 1) double = realmin * ones(3, 1)
        qc(3, 1) double = zeros(3, 1)
        Tc(3, 1) double = Inf * ones(3, 1)
        Omega0(3, 1) double = zeros(3, 1)
        f0(3, 1) double = ones(3, 1)
    end
    
    properties (Constant)
        DetParNames = {'KScal0', 'PS0B', 'domegaIBBiasS', 'dKScalP', 'dKScalN', 'dPSS0', 'domegaIBQuantS', 'DGSens'}
        DetParDimNum = [2, 3, 2, 3, 3, 3, 2, 3]
        RandParNames = {'R', 'K', 'B', 'N', 'Q', 'qc', 'Tc', 'Omega0', 'f0'}
        LinParNames = {'domegaIBBiasS', 'dKScalP', 'dKScalN', 'dPSS0', 'domegaIBQuantS', 'DGSens'}
        LinParDimNum = [2, 3, 3, 3, 2, 3]
        PosScalFactErrName = 'dKScalP'
        NegScalFactErrName = 'dKScalN'
    end
    
    methods
        function obj = GyroTriadPars(nSamp, std, mean, chooseFrom3StdAndZero)
            %GYROTRIADPARS 构造此类的实例。生成标准差std，均值mean正态分布的线性确定性参数，std与mean结构体包含确定性参数域，默认为0
            if nargin < 1
                nSamp = 1;
            end
            if nargin < 2
                std = struct();
            end
            if nargin < 3
                mean = struct();
            end
            if nargin < 4
                chooseFrom3StdAndZero = false;
            end
            
            obj = obj@InertSensTriadPars(nSamp, std, mean, chooseFrom3StdAndZero);
        end
    end
end