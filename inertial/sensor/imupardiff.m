function [parDiff] = imupardiff(parCmp, parRef, convToConvUnit, doDisp, g)
%IMUPARDIFF 比较并显示惯组参数差值
%
% Input Arguments
% # parCmp: 结构体，惯组参数比较值，可包含acc、gyro域，acc域可包含B（3*1，零偏）、SF/SFp/SFn/SF0/dSF（3*1，标度因数）、MA（3*3，失准角）、SO（3*1，二次项系数）等域，gyro域包含B（3*1，零偏）、SF/SFp/SFn/SF0/dSF（3*1，标度因数）、MA（3*3，失准角）、GS（3*3，g敏感项）等域，单位均为国际单位制单位
% # parRef: 结构体，惯组参数参考值，包含与parCmp相同的域
% # convToConvUnit: 逻辑标量，true表示输出量单位转换为常用单位，默认为true
% # doDisp: 逻辑标量，true表示显示输出量，默认为true，仅在convToConvUnit为true时有效
% # g: 标量，重力加速度，单位m/s^2，默认为9.8
%
% Output Arguments
% # parDiff: 结构体，惯组参数差值，包含与parCmp相同的域

if nargin < 5
    g = 9.8;
end
if nargin < 4
    doDisp = true;
end
if nargin < 3
    convToConvUnit = true;
end

fieldName = {'acc', 'gyro'};
fieldDispName = {'加速度计', '陀螺'};
subFieldName = {'KScal0', 'PS0B', 'dfBiasS', 'dKScalP', 'dKScalN', 'dPSS0', 'dfQuantS', 'lB', 'KAniso', 'CBA', 'domegaIBBiasS', 'domegaIBQuantS', 'DGSens'};
subFieldDispName = {'标度因数标称值', '安装转换矩阵标称值', '零偏', '正向标度因数误差', '负向标度因数误差', '安装失准角', '量化误差', '杆臂', '不等惯量误差系数', '加速度计坐标系转换矩阵', '零偏', '量化误差', 'g敏感项系数'};
relativeDiff = [true, false(1, 12)];
unitConvCoef = [1e6, 1, 1e6/g, 1e6, 1e6, 1e6, 1e6/g, 1e3, 1e6/g/180*pi, 1, NaN, NaN, NaN
    1e6, 1, NaN, 1e6, 1e6, 1e6, NaN, NaN, NaN, NaN, 180*3600/pi, 180*3600/pi, 180*3600/pi*g];
convUnit = {'ppm', '无单位', 'μg', 'ppm/g^(列序号-1)', 'ppm/g^(列序号-1)', 'μrad', 'μg', 'mm', 'μg/(°/s)^2', '无单位', '', '', ''
    'ppm', '无单位', '', 'ppm/(°/s)^(列序号-1)', 'ppm/(°/s)^(列序号-1)', 'μrad', '', '', '', '', '°/h', '°/h', '(°/h)/g'};
dKScalHigherOrderUCCoef = [g, pi/180];

for i=1:length(fieldName)
    if isfield(parCmp, fieldName{i}) && isfield(parRef, fieldName{i}) % NOTE: 使用线性索引
        for j=1:length(subFieldName)
            if isfield(parCmp.(fieldName{i}), subFieldName{j}) && isfield(parRef.(fieldName{i}), subFieldName{j}) % NOTE: 使用线性索引
                parDiff.(fieldName{i}).(subFieldName{j}) = parCmp.(fieldName{i}).(subFieldName{j}) - parRef.(fieldName{i}).(subFieldName{j});
                if relativeDiff(j) % NOTE: 使用线性索引
                    parDiff.(fieldName{i}).(subFieldName{j}) = parDiff.(fieldName{i}).(subFieldName{j}) ./ parRef.(fieldName{i}).(subFieldName{j});
                end
                if convToConvUnit
                    parDiff.(fieldName{i}).(subFieldName{j}) = parDiff.(fieldName{i}).(subFieldName{j}) * unitConvCoef(i, j);
                    if ~isempty(strfind(subFieldName{j}, 'dKScal'))
                        for k=2:size(parDiff.(fieldName{i}).(subFieldName{j}), 2)
                            parDiff.(fieldName{i}).(subFieldName{j})(:, k) = parDiff.(fieldName{i}).(subFieldName{j})(:, k)*dKScalHigherOrderUCCoef(i)^(k-1); % NOTE: 使用线性索引
                        end
                    end
                    if doDisp
                        disp([fieldDispName{i} subFieldDispName{j} '差值（' convUnit{i, j} '）']); % NOTE: 使用线性索引
                        disp(parDiff.(fieldName{i}).(subFieldName{j}));
                    end
                end
            end
        end
    end
end
end