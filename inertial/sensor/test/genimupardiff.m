function [IMUParDiff, IMUParAppr] = genimupardiff(nSamp, IMUParTrue, IMUParTrueEquiv, Diff2TrueRatio)
%GENIMUPARDIFF 生成测试用惯组参数误差及近似惯组参数

IMUParDiff.acc = errfreeaccpar(nSamp);
IMUParDiff.gyro = errfreegyropar(nSamp);
IMUParDiff.acc.PS0B = IMUParTrue.acc.PS0B;
IMUParDiff.acc.dfBiasS = IMUParTrueEquiv.acc.dfBiasS * Diff2TrueRatio;
IMUParDiff.acc.dKScalP = IMUParTrueEquiv.acc.dKScalP * Diff2TrueRatio;
IMUParDiff.acc.dKScalN = IMUParTrueEquiv.acc.dKScalN * Diff2TrueRatio;
IMUParDiff.acc.dPSS0 = IMUParTrueEquiv.acc.dPSS0 * Diff2TrueRatio;
IMUParDiff.gyro.PS0B = IMUParTrue.gyro.PS0B;
IMUParDiff.gyro.domegaIBBiasS = IMUParTrueEquiv.gyro.domegaIBBiasS * Diff2TrueRatio;
IMUParDiff.gyro.dKScalP = IMUParTrueEquiv.gyro.dKScalP * Diff2TrueRatio;
IMUParDiff.gyro.dKScalN = IMUParTrueEquiv.gyro.dKScalN * Diff2TrueRatio;
IMUParDiff.gyro.dPSS0 = IMUParTrueEquiv.gyro.dPSS0 * Diff2TrueRatio;

IMUParAppr = IMUParTrue;
IMUParAppr.acc.dfBiasS = IMUParTrue.acc.dfBiasS + IMUParDiff.acc.dfBiasS;
IMUParAppr.acc.dKScalP = IMUParTrue.acc.dKScalP + IMUParDiff.acc.dKScalP;
IMUParAppr.acc.dKScalN = IMUParTrue.acc.dKScalN + IMUParDiff.acc.dKScalN;
IMUParAppr.acc.dPSS0 = IMUParTrue.acc.dPSS0 + IMUParDiff.acc.dPSS0;
IMUParAppr.gyro.domegaIBBiasS = IMUParTrue.gyro.domegaIBBiasS + IMUParDiff.gyro.domegaIBBiasS;
IMUParAppr.gyro.dKScalP = IMUParTrue.gyro.dKScalP + IMUParDiff.gyro.dKScalP;
IMUParAppr.gyro.dKScalN = IMUParTrue.gyro.dKScalN + IMUParDiff.gyro.dKScalN;
IMUParAppr.gyro.dPSS0 = IMUParTrue.gyro.dPSS0 + IMUParDiff.gyro.dPSS0;
end