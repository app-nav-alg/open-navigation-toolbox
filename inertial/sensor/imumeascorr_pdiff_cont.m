function [fBCorr, omegaIBBCorr] = imumeascorr_pdiff_cont(IMUParDiff, IMUPar, fB, omegaIBB, omegaIBBRate) %#codegen
%IMUMEASCORR_PDIFF_CONT 按偏微分算法计算比力及角速度修正量（用于修正由惯组误差参数残差引起的惯组测量值误差，连续形式）
%
% Input Arguments
% # IMUParDiff: 结构体，包含n个采样，惯组误差参数的误差（微分），pAppr-pTrue，包含的域见accerror及gyroerror函数，本函数使用了零偏、标度因数误差、失准角等域
% # IMUPar: 结构体，包含n个采样，惯组误差参数，包含的域见accerror及gyroerror函数，除PS0B域外，其它域对计算结果的影响较小
% # fB: 3*n向量，修正前的比力，单位m/s^2
% # omegaIBB: 3*n向量，修正前的角速度，单位rad/s
% # omegaIBBRate: 3*n向量，修正前的角加速度，单位rad/s^2
%
% Output Arguments
% # fBCorr: 3*n向量，比力修正量，fBTrue-fBAppr，单位m/s^2
% # omegaIBBCorr: 3*n向量，角速度修正量，omegaIBBTrue-omegaIBBAppr，单位rad/s
%
% References
% # 《应用导航算法工程基础》 误差参数残差补偿算法

fBCorr = coder.nullcopy(NaN(size(fB)));
omegaIBBCorr = coder.nullcopy(NaN(size(omegaIBB)));

for iSamp=1:size(fB, 2)
    [dfTildeOutS_par, domegaTildeIBOutS_par, accPSBT, gyroPSBT, dfCrsDepS, domegaIBCrsDepS, accdKScalCoef, gyrodKScalCoef] ...
        = imuoutdiff_par(IMUParDiff, IMUPar, fB, omegaIBB, omegaIBBRate, iSamp);
    [~, accJf, accJg] = fgjacobian_vB(accPSBT*fB(:, iSamp), accPSBT, IMUPar.acc.KScal0(:, iSamp), accdKScalCoef, ...
        IMUPar.acc.dfBiasS(:, iSamp), IMUPar.acc.dfQuantS(:, iSamp), dfCrsDepS);
    [~, gyroJf, gyroJg] = fgjacobian_vB(gyroPSBT*omegaIBB(:, iSamp), gyroPSBT, IMUPar.gyro.KScal0(:, iSamp), gyrodKScalCoef, ...
        IMUPar.gyro.domegaIBBiasS(:, iSamp), IMUPar.gyro.domegaIBQuantS(:, iSamp), domegaIBCrsDepS);
    fBCorr(:, iSamp) = (accJf+accJg) \ dfTildeOutS_par;
    omegaIBBCorr(:, iSamp) = (gyroJf+gyroJg) \ domegaTildeIBOutS_par;
end
end