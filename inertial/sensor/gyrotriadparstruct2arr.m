function [arrPar] = gyrotriadparstruct2arr(structPar, indexMap)
%GYROTRIADPARSTRUCT2ARR 将以结构体形式保存的陀螺三元组的参数转换为二维数组形式保存的参数
%
% Input Arguments
% # structPar: 各域为数组的结构体，时间在各域的最后一维上
% # indexMap: 1*54向量，绝对值依次为陀螺标度因数标称值（X、Y、Z，下同）（3）、非正交传感器组的安装矩阵标称值（按列展开）（9）、零偏（3）、正向标度因数误差（到3阶，按列展开）（9）、
%           负向标度因数误差（到3阶）（9）、安装误差矩阵（按列展开）（9）、量化误差（3）、g敏感项比力一次方补偿矩阵（9）
%           在arrPar中的列数，NaN表示不转换，arrPar中对应列设置为0，负数表示取负值。
%
% Output Arguments
% # arrPar: 二维数组形式保存的参数，每行为同一时刻的各项参数，每列为同一参数的各时刻值

if (length(indexMap) ~= 54)
    error('Length of indexMap should be 54.');
end

indexMapStruct.KScal0 = indexMap(1:3)';
indexMapStruct.PS0B = reshape(indexMap(4:12), 3, 3);
indexMapStruct.dBias = indexMap(13:15)';
indexMapStruct.dKScalP = reshape(indexMap(16:24), 3, 3);
indexMapStruct.dKScalN = reshape(indexMap(25:33), 3, 3);
indexMapStruct.dPSS0 = reshape(indexMap(34:42), 3, 3);
indexMapStruct.dQuant = indexMap(43:45)';
indexMapStruct.DGSens = reshape(indexMap(46:54), 3, 3);

[arrPar] = struct2array(structPar, indexMapStruct);
end