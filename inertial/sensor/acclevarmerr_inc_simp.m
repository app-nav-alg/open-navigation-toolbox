function [ dfLevArm ] = acclevarmerr_inc_simp( lB, DalphaBl, DalphaBl1, Dt ) %#codegen
%ACCLEVARMERR_INC_SIMP 计算加速度计的杆臂误差，增量形式
%
% Input Arguments
% # lB: 3*3矩阵，矩阵第1、2、3列分别为X、Y、Z加速度计的杆臂在B系下的投影，单位m
% # DalphaBl: 3*1向量，本周期角增量，单位rad
% # DalphaBl1: 3*1向量，上一周期角增量，单位rad
% # Dt: 标量，惯导解算周期，单位s
%
% Output Arguments
% # dfLevArm：3*1向量，加速度计杆臂误差，单位m/s

dfLevArm = diag((skewsymmat(DalphaBl)^2+skewsymmat(DalphaBl-DalphaBl1)) * lB / Dt);
end