function [ accRandPar1] = convaccrandpar( accRandPar, g, inIsConvent )
% CONVACCRANDPAR 加速度计三元组的随机性误差参数的单位转换
%
% Input Arguments
% # accRandPar: 各域为数组的结构体，加速度计的随机性误差参数，标准单位/常用单位
%        accRandPar.Q：3*1向量，量化噪声
%        accRandPar.N：3*1向量，角度随机游走
%        accRandPar.B：3*1向量，零偏不稳定性噪声
%        accRandPar.K：3*1向量，角速率随机游走
%        accPar.R：3*1向量，速率斜坡
% # g: 标量，重力加速度，单位：m/s^2，默认为9.8
% # inIsConvent：逻辑标量，默认为true，true表示输入为常用单位，向标准单位转换；false表示输入为标准单位，向常用单位转换
%
% Output Arguments
% # accRandPar1: 各域为数组的结构体，加速度计的确定性误差参数，常用单位/标准单位

if nargin < 2
    g = 9.8;
end
if nargin < 3
    inIsConvent = true;
end

if inIsConvent
    % 常用单位→国际单位
    accRandPar1.Q = accRandPar.Q/3600; % m/h→m/s
    accRandPar1.N = 1e-6*g*60*accRandPar.N; % μgsqrt(h)→m/s/sqrt(s)
    accRandPar1.B = 1e-6*g*accRandPar.B; % μg→m/s^2
    accRandPar1.K = 1e-6*g/60*accRandPar.K; % μg/sqrt(h)→m/s^2/sqrt(s)
    accRandPar1.R =  1e-6*g/3600*accRandPar.R; % μg/h→m/s^2/s
else
    % 国际单位→常用单位
    accRandPar1.Q = 3600*accRandPar.Q; % m/s→m/h
    accRandPar1.N = 1e6/g/60*accRandPar.N; % m/s/sqrt(s)→μgsqrt(h)
    accRandPar1.B = 1e6/g*accRandPar.B; % m/s^2→μg
    accRandPar1.K = 1e6/g*60*accRandPar.K; % m/s^2/sqrt(s)→μg/sqrt(h)
    accRandPar1.R =  1e6/g*3600*accRandPar.R; % m/s^2/s→μg/h
end
end