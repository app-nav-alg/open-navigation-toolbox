function [structPar] = gyrotriadpararr2struct(gyroPar, indexMap, setNonExistParToNaN, keepAllNaNField)
%GYROTRIADPARARR2STRUCT 将以二维数组形式保存的陀螺三元组的参数转换为结构体形式保存的参数
%
% Input Arguments
% # gyroPar: 二维数组形式保存的参数，每行为同一时刻的各项参数，每列为同一参数的各时刻值，参数详细说明参考gyroerror_cont函数
% # indexMap: 1*54向量，绝对值依次为陀螺标度因数标称值（X、Y、Z，下同）（3）、非正交传感器组的安装矩阵标称值（按列展开）（9）、零偏（3）、正向标度因数误差（到3阶，按列展开）（9）、
%           负向标度因数误差（到3阶）（9）、安装误差矩阵（按列展开）（9）、量化误差（3）、g敏感项比力一次方补偿矩阵（9）
%           在arrPar中的列数，NaN表示不存在，负数表示取负值。
% # setNonExistParToNaN: 逻辑标量，true表示存在的参数类型域中不存在的参数项设置为NaN，否则标度因数设置为1，其它类型参数设置为0，默认为false
% # keepAllNaNField: 逻辑标量，是否对indexMap中元素全为NaN的域赋默认值，默认为false
%
% Output Arguments
% # structPar: 仅含有indexMap中包含的参数类型域，时间在最后一维上

% NOTE: 本函数使用线性索引
if nargin < 3
    setNonExistParToNaN = false;
end

if nargin < 4
    keepAllNaNField = false;
end

if (length(indexMap) ~= 54)
    error('Length of indexMap should be 54.');
end

indexMapStruct.KScal0 = indexMap(1:3)';
indexMapStruct.PS0B = reshape(indexMap(4:12), 3, 3);
indexMapStruct.domegaIBBiasS = indexMap(13:15)';
indexMapStruct.dKScalP = reshape(indexMap(16:24), 3, 3);
indexMapStruct.dKScalN = reshape(indexMap(25:33), 3, 3);
indexMapStruct.dPSS0 = reshape(indexMap(34:42), 3, 3);
indexMapStruct.domegaIBQuantS = indexMap(43:45)';
indexMapStruct.DGSens = reshape(indexMap(46:54), 3, 3);

if setNonExistParToNaN
    nonExistEleVal = NaN;
else
    nonExistEleVal.KScal0 = ones(3,1);
    nonExistEleVal.PS0B = eye(3);
    nonExistEleVal.domegaIBBiasS = zeros(3,1);
    nonExistEleVal.dKScalP = zeros(3,3);
    nonExistEleVal.dKScalN = zeros(3,3);
    nonExistEleVal.dPSS0 = zeros(3,3);
    nonExistEleVal.domegaIBQuantS = zeros(3,1);  
    nonExistEleVal.DGSens = zeros(3,3);
end
[structPar] = array2struct(gyroPar, indexMapStruct, nonExistEleVal, keepAllNaNField);
end