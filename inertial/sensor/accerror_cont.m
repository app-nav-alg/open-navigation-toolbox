function [ dfB, fTildeS ] = accerror_cont( fB, accPar, omegaIBB, omegaIBBRate, ignore2Od ) %#codegen
%ACCERROR_CONT 计算加速度计器件误差引入的测量误差
%
% Tips
% # 本函数针对连续形式
%
% Input Arguments
% # fB: 3*n矩阵，n为采样个数，比力，单位m/s^2
% # accPar: 各域为数组的结构体，加速度计的确定性误差参数，各域最后一维为时间m
%       accPar.KScal0: 3*n矩阵，加速度计标度因数标称值，单位：1m/s对应的脉冲数
%       accPar.PS0B：3*3*n数组，传感器系与本体系之间的坐标转换矩阵标称值，无单位，默认全为单位阵
%       accPar.dfBiasS: 3*n矩阵，加速度计零偏，单位：m/s^2
%       accPar.dKScalP: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，加速度计正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(m/s^2)^-(i-1)，默认全为0
%       accPar.dKScalN: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，加速度计负向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(m/s^2)^-(i-1),默认全为0
%       accPar.dPSS0：3*3*n数组，失准角（安装误差），无单位，默认全为0
%       accPar.dfQuantS: 3*n矩阵，加速度计量化误差，单位：m/s^2，默认全为0
%       accPar.lB: 3*3*n数组，按列分别为3只加速度计的杆臂项，单位：m，默认全为0
%       accPar.KAniso：3*n矩阵，3只加速度计不等惯量误差的系数，单位：(m/s^2)/(rad/s)^2，默认全为0
%       accPar.CBA：3*3*3*n数组，按页分别为本体系到3只加速度计坐标系的坐标转换矩阵，无单位，默认各页全为单位阵
% # omegaIBB：3*n矩阵，角速度，单位：rad/s
% # omegaIBBRate: 3*n矩阵，角速度变化率，单位：rad/s^2
% # ignore2Od: 逻辑标量，true表示忽略2阶误差，默认为true
%
% Output Arguments
% # dfB: 3*n矩阵，各采样时刻的比力误差，单位m/s^2
% # fTildeS: 3*n矩阵，各采样时刻的S系下的比力（包含测量误差），单位m/s^2
%
% References
% # 《应用导航算法工程基础》“惯性传感器模型” 总误差模型

if nargin < 5
    ignore2Od = true;
end
n = size(fB, 2);

dfB = coder.nullcopy(NaN(3, n, class(fB)));
if nargout > 1
    fTildeS = coder.nullcopy(NaN(3, n, class(fB)));
end
for j=1:n
    %% 计算加速度计各项误差
    % 杆臂效应误差-比力形式
    PSBT = (accPar.PS0B(:, :, j)*(eye(3)+accPar.dPSS0(:, :, j)))';
    dfSizeS = acclevarmerr_cont(PSBT, accPar.lB(:, :, j), omegaIBB(:, j), omegaIBBRate(:, j));
    % 不等惯量误差-比力形式
    dfAnisoS = accanisoerr_cont(accPar.KAniso(:, j), accPar.CBA(:, :, :, j), omegaIBB(:, j));
    %% 计算加速度计比力输出值fTildeS与比力理论值fB之差
    % 不加标度因数误差阵的比力输出
    dfBiasExS = accPar.dfBiasS(:, j) + dfSizeS + dfAnisoS + accPar.dfQuantS(:, j);
    fS = PSBT * fB(:, j);
    fTildeScalS = fS + accPar.dfBiasS(:, j) + dfSizeS + dfAnisoS;
    % 计算j时刻标度因数误差阵
    accdKScalCoef = scalfacterrsign(accPar.dKScalP(:, :, j), accPar.dKScalN(:, :, j), fTildeScalS);
    % 计算dKScal
    dKScal = polyscalfact(fTildeScalS, accdKScalCoef); % NOTE: vTildeScalS计算式应与fgjacobian及imuout2in函数一致
    % 忽略二阶误差的加速度计误差
    dPSBT = (accPar.PS0B(:, :, j)*accPar.dPSS0(:, :, j))';
    dfB(:, j) = (accPar.PS0B(:, :, j)')\(dKScal.*(accPar.PS0B(:, :, j)'*fB(:, j))+dPSBT*fB(:, j)+dfBiasExS);
    if ~ignore2Od
        % 考虑二阶误差的加速度计误差
        dfB2 = (accPar.PS0B(:, :, j)')\(dKScal.*(dPSBT*fB(:, j)+dfBiasExS));
        dfB(:, j) = dfB(:, j)+dfB2;
    end
    if nargout > 1
        fTildeS(:, j) = (ones(3, 1)+dKScal) .* (fS+dfBiasExS);
    end
end
end