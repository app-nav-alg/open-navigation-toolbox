function [ accPar ] = errfreeaccpar(m, dKScalOrder)
%ERRFREEACCPAR 生成无误差的加速度计参数
%
% Input Arguments
% # m: 标量，误差采样数，默认为1
% # dKScalOrder: 标量，标度因数误差的阶次，默认为1
%
% Output Arguments
% # accPar: 加速度计的确定性误差参数，说明见accerror_cont函数

if nargin < 1
    m = 1;
end
if nargin < 2
    dKScalOrder = 1;
end

accPar.KScal0 = ones(3, m);
accPar.PS0B = repmat(eye(3), [1 1 m]);
accPar.dfBiasS = zeros(3, m);
accPar.dKScalP = zeros(3, dKScalOrder, m);
accPar.dKScalN = zeros(3, dKScalOrder, m);
accPar.dPSS0 = zeros(3, 3, m);
accPar.dfQuantS = zeros(3, m);
accPar.lB = zeros(3, 3, m);
accPar.KAniso = zeros(3, m);
accPar.CBA(:, :, 1) = eye(3);
accPar.CBA(:, :, 2) = [0 1 0; 0 0 1; 1 0 0];
accPar.CBA(:, :, 3) = [0 0 1; 1 0 0; 0 1 0];
accPar.CBA = repmat(accPar.CBA, 1, 1, 1, m);
end