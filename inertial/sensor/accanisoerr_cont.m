function [ dfAnisoS ] = accanisoerr_cont( KAniso, CBA, omegaIBB )
%ACCANISOERR_CONT 计算加速度计三元组的不等惯量误差
%
% Tips
% # 本函数针对连续形式，该误差仅适用于摆式加速度计
%
% Input Arguments
% # KAniso: 3*1向量，3只加速度计不等惯量误差的系数，单位(m/s^2)/(rad/s)^2
% # CBA: 3*3*3矩阵，本体系到3只加速度计坐标系的坐标转换矩阵，无单位
% # omegaIBB: 3*1向量，角速度，单位rad/s
%
% Output Arguments
% # dfAnisoS：3*1向量，加速度计三元组的不等惯量误差，单位m/s^2
%
% References
% # 《应用导航算法工程基础》“加速度计不等惯量误差”

dfAnisoS = [KAniso(1)*(CBA(1, :, 1)*omegaIBB)*(CBA(3, :, 1)*omegaIBB);
    KAniso(2)*(CBA(1, :, 2)*omegaIBB)*(CBA(3, :, 2)*omegaIBB);
    KAniso(3)*(CBA(1, :, 3)*omegaIBB)*(CBA(3, :, 3)*omegaIBB)];
end