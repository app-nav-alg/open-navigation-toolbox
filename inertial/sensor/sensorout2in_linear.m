function [vB, JfCond] = sensorout2in_linear(vTildeOutS, g, vB0, PSBT, KScal0, dKScalCoef, dvBiasS, dvQuantS, Dt) %#codegen
%SENSOROUT2IN_LINEAR 使用线性化法进行内层迭代
%
% Input Arguments
% # vTildeOutS: 3*1向量，加速度计或陀螺瞬时输出值（或对应的积分增量输出）
% # g: 3*1向量，g(w,p)值，当vTildeOutS为增量时，该值应为增量
% # PSBT: 3*3矩阵，加速度计或陀螺的坐标变换矩阵
% # KScal0: 3*1向量，加速度计或陀螺的标度因数标称值
% # dKScalCoef: 3*x矩阵，x为阶数，加速度计或陀螺的标度因数误差
% # dvBiasS, dvQuantS: 见fgjacobian函数说明
% # Dt: 标量，采样周期（当vS为增量时），单位s，默认为空（瞬时值）
%
% Output Arguments
% # vB: 3*1向量，采用线性化法求得的比力或角速度
%
% References:
% # 《应用导航算法工程基础》“由输出值计算输入值的算法”

if nargin < 9
    Dt = [];
end

vS0 = PSBT * vB0;
if nargout > 1
    [KScal, Jf, ~, JfCond] = fgjacobian_vB(vS0, PSBT, KScal0, dKScalCoef, dvBiasS, dvQuantS, zeros(3, 1), Dt);
else
    [KScal, Jf] = fgjacobian_vB(vS0, PSBT, KScal0, dKScalCoef, dvBiasS, dvQuantS, zeros(3, 1), Dt);
end
f = KScal .* (vS0+dvBiasS+dvQuantS);
vB = vB0 + Jf \ (vTildeOutS-g-f);
end