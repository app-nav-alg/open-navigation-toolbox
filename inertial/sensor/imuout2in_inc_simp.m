function [ DupsilonBl, DalphaBl ] = imuout2in_inc_simp( DupsilonTildeOutSl, DalphaTildeOutSl, IMUPar, Tl, DalphaBl1, stepNum ) %#codegen
%IMUOUT2IN_INC_SIMP 将器件的输出值转换为输入值
%
% Tips
% # 本函数针对增量形式
%
% Input Arguments
% # DupsilonTildeOutSl: 3*n矩阵，各采样周期内加速度计输出脉冲数
% # DalphaTildeOutSl: 3*n矩阵，各采样周期内陀螺输出脉冲数
% # IMUPar: 包含加速度计三元组和陀螺三元组的确定性误差参数的结构体，参数详细说明参考accerror_cont及gyroerror_cont函数
% # Tl: 标量，采样周期，单位s
% # DalphaBl1: 3*1向量，DalphaTildeOutSl前一周期的经补偿后的角增量,单位rad，
% # stepNum: 迭代步数，默认为20
%
% Output Arguments
% # DupsilonBl: 3*n矩阵，经补偿后的各采样周期内器件输入比速度增量值，单位m/s
% # DalphaBl: 3*n矩阵，经补偿后的各采样周期内器件输入角增量值，单位rad
%
% References:
% # 《应用导航技术》“惯性传感器模型” 由输出值计算输入值

if nargin < 6
    stepNum = 2;
end

% 标定补偿
DupsilonBl = sensorout2in_newton_simp(DupsilonTildeOutSl, IMUPar.acc.KScal0, IMUPar.acc.dfBiasS*Tl, [IMUPar.acc.dKScalP(:,1) IMUPar.acc.dKScalP(:,2)/Tl], IMUPar.acc.dPSS0, stepNum);
DalphaBl = sensorout2in_linear_simp(DalphaTildeOutSl, IMUPar.gyro.KScal0, IMUPar.gyro.domegaIBBiasS*Tl, IMUPar.gyro.dKScalP, IMUPar.gyro.dPSS0, DupsilonBl, IMUPar.gyro.DGSens);
% 补偿惯组内杆臂
if all(DalphaBl1==0)
    DalphaBl1 = DalphaBl; % 第一周期
end
DupsilonBl = DupsilonBl - acclevarmerr_inc_simp(IMUPar.acc.lB', DalphaBl, DalphaBl1, Tl);
end