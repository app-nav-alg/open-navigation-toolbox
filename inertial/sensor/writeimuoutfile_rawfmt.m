function [] = writeimuoutfile_rawfmt( filePath, accOut, gyroOut, accFirst, precision, flag)
%WRITEIMUOUTFILE_RAWFMT 将加速度计和陀螺输出按原始格式写文件
%
% Tips
% # 当precision参数为'%1.16e'或'%1.7e'时将调用save函数以双精度浮点数或单精度浮点数格式写文件，否则调用dlmwrite函数写文件，后者速度较慢
%
% Input Arguments
% # filePath: 字符串，惯组输出数据文件的路径
% # accOut, gyroOut: 列数为3的二维矩阵，分别代表X、Y、Z器件
% # accFirst: 逻辑标量，true表示将加速度计数据放在陀螺数据左侧的列上，false表示放在右侧，默认为true
% # precision: 字符串，指示精度，格式参见dlmwrite函数帮助，默认为空
% # flag: 向量，行数与accout、gyroout相同的矩阵，标志位，将放在最右侧列上，默认为空

if nargin < 6
    flag = [];
end
if nargin < 5
    precision = [];
end
if nargin < 4
    accFirst = true;
end

if accFirst
    M = horzcat(accOut, gyroOut, flag);
else
    M = horzcat(gyroOut, accOut, flag);
end

if isempty(precision)
    dlmwrite(filePath, M, 'delimiter', ' ', 'newline', 'pc');
elseif strcmp(precision, '%.16e') || strcmp(precision, '%1.16e')
    save(filePath, 'M', '-ascii', '-double');
elseif strcmp(precision, '%.7e') || strcmp(precision, '%1.7e')
    save(filePath, 'M', '-ascii');
else
    dlmwrite(filePath, M, 'delimiter', ' ', 'newline', 'pc', 'precision', precision);
end
end