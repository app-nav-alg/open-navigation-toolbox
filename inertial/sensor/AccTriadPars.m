classdef AccTriadPars < InertSensTriadPars
    %ACCTRIADPARS 加速度计三元组参数，确定性误差参数各域最后一维尺寸n为采样数或分段数，默认为1
    % KScal0: 3*n矩阵，加速度计标度因数标称值，单位(^/s)/(m/s^2)，默认为1
    % PS0B：3*3*n数组，传感器系与本体系之间的坐标转换矩阵标称值，无单位，默认全为单位阵
    % dfBiasS: 3*n矩阵，加速度计零偏，单位：m/s^2，默认为0
    % dKScalP: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，加速度计正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(m/s^2)^-(i-1)，默认全为0
    % dKScalN: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，加速度计负向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(m/s^2)^-(i-1),默认全为0
    % dPSS0：3*3*n数组，失准角（安装误差），单位rad，默认全为0
    % dfQuantS: 3*n矩阵，加速度计量化误差，单位：m/s^2，默认全为0
    % lB: 3*3*n数组，按列分别为3只加速度计的杆臂项，单位：m，默认全为0
    % KAniso：3*n矩阵，3只加速度计不等惯量误差的系数，单位：(m/s^2)/(rad/s)^2，默认全为0
    % CBA：3*3*3*n数组，按页分别为本体系到3只加速度计坐标系的坐标转换矩阵，无单位，默认各页全为单位阵
    % R: 3*1向量，加速度计比力斜坡系数，单位m/s^3，默认为0
    % K: 3*1向量，加速度计比力随机游走系数，单位m/s^(5/2)，默认为0
    % B: 3*1向量，加速度计零偏不稳定性系数，单位m/s^2，默认为0
    % N: 3*1向量，加速度计比速度随机游走系数，单位m/s^(3/2)，默认为0
    % Q: 3*1向量，加速度计量化噪声系数，单位m/s，默认为realmin
    % qc: 3*1向量，加速度计指数相关噪声驱动白噪声的功率谱密度平方根，单位m/s^(5/2)，默认为0
    % Tc: 3*1向量，加速度计指数相关噪声相关时间，单位s，默认为Inf
    % Omega0: 3*1向量，加速度计正弦噪声幅值，单位m/s^2，默认为0
    % f0: 3*1向量，加速度计正弦噪声频率，单位Hz，默认为1
    
    properties
        KScal0(3, :) double = ones(3, 1)
        PS0B(3, 3, :) double = eye(3)
        dfBiasS(3, :) double = zeros(3, 1)
        dKScalP(3, :, :) double = zeros(3, 1)
        dKScalN(3, :, :) double = zeros(3, 1)
        dPSS0(3, 3, :) double = zeros(3)
        dfQuantS(3, :) double = zeros(3, 1)
        lB(3, 3, :) double = zeros(3)
        KAniso(3, :) double = zeros(3, 1)
        CBA(3, 3, 3, :) double = repmat(eye(3), 1, 1, 3)
        R(3, 1) double = zeros(3, 1)
        K(3, 1) double = zeros(3, 1)
        B(3, 1) double = zeros(3, 1)
        N(3, 1) double = zeros(3, 1)
        Q(3, 1) double = realmin * ones(3, 1)
        qc(3, 1) double = zeros(3, 1)
        Tc(3, 1) double = Inf * ones(3, 1)
        Omega0(3, 1) double = zeros(3, 1)
        f0(3, 1) double = ones(3, 1)
    end
    
    properties (Constant)
        DetParNames = {'KScal0', 'PS0B', 'dfBiasS', 'dKScalP', 'dKScalN', 'dPSS0', 'dfQuantS', 'lB', 'KAniso', 'CBA'}
        DetParDimNum = [2, 3, 2, 3, 3, 3, 2, 3, 2, 4]
        RandParNames = {'R', 'K', 'B', 'N', 'Q', 'qc', 'Tc', 'Omega0', 'f0'}
        LinParNames = {'dfBiasS', 'dKScalP', 'dKScalN', 'dPSS0', 'dfQuantS', 'lB', 'KAniso'}
        LinParDimNum = [2, 3, 3, 3, 2, 3, 2]
        PosScalFactErrName = 'dKScalP'
        NegScalFactErrName = 'dKScalN'
    end
    
    methods
        function obj = AccTriadPars(nSamp, std, mean, chooseFrom3StdAndZero)
            %ACCTRIADPARS 构造此类的实例。生成标准差std，均值mean正态分布的线性确定性参数，std与mean结构体包含确定性参数域，默认为0
            if nargin < 1
                nSamp = 1;
            end
            if nargin < 2
                std = struct();
            end
            if nargin < 3
                mean = struct();
            end
            if nargin < 4
                chooseFrom3StdAndZero = false;
            end
            
            obj = obj@InertSensTriadPars(nSamp, std, mean, chooseFrom3StdAndZero);
        end
    end
end