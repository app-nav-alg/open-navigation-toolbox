function [ gyroRandPar1] = convgyrorandpar( gyroRandPar, inIsConvent )
% CONVGYRORANDPAR 陀螺三元组的随机性误差参数的单位转换
%
% Input Arguments
% # gyroRandPar: 各域为数组的结构体，陀螺的随机性误差参数，标准单位/常用单位
%        gyroRandPar1.Q：3*1向量，量化噪声
%        gyroRandPar1.N：3*1向量，角度随机游走
%        gyroRandPar1.B：3*1向量，零偏不稳定性噪声
%        gyroRandPar1.K：3*1向量，角速率随机游走
%        gyroRandPar1.R：3*1向量，速率斜坡
% # inIsConvent：逻辑标量，默认为true，true表示输入为常用单位，向标准单位转换；false表示输入为标准单位，向常用单位转换
%
% Output Arguments
% # gyroRandPar1: 各域为数组的结构体，陀螺的确定性误差参数，常用单位/标准单位

if nargin < 2
    inIsConvent = true;
end

if inIsConvent
    % 常用单位→国际单位
    gyroRandPar1.Q = pi/180/3600*gyroRandPar.Q; % ″→rad
    gyroRandPar1.N = pi/180/60*gyroRandPar.N; % °/sqrt(h)→rad/sqrt(s)
    gyroRandPar1.B = pi/180/3600*gyroRandPar.B; % °/h→rad/s
    gyroRandPar1.K = pi/180/60^3*gyroRandPar.K; % °/sqrt(h^3)→rad/s/sqrt(s)
    gyroRandPar1.R = pi/180/3600^2*gyroRandPar.R; % °/h^2→rad/s^2
else
    % 国际单位→常用单位
    gyroRandPar1.Q = 180*3600/pi*gyroRandPar.Q; % rad→″
    gyroRandPar1.N = 180*60/pi*gyroRandPar.N; % rad/sqrt(s)→°/sqrt(h)
    gyroRandPar1.B = 180*3600/pi*gyroRandPar.B; % rad/s→°/h
    gyroRandPar1.K = 180*60^3/pi*gyroRandPar.K; % rad/s/sqrt(s)→°/sqrt(h^3)
    gyroRandPar1.R = 180*3600^2/pi*gyroRandPar.R; % rad/s^2→°/h^2
end
end