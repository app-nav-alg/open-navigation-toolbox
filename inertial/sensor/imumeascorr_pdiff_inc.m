function [DupsilonBlCorr, DalphaBlCorr] = imumeascorr_pdiff_inc(IMUParDiff, IMUPar, DupsilonBl, DalphaBl, Tl, DalphaBl1) %#codegen
%IMUMEASCORR_PDIFF_INC 按偏微分算法计算比速度增量及角增量修正量（用于修正由惯组误差参数残差引起的惯组测量值误差，增量形式）
%
% Input Arguments
% # IMUParDiff: 结构体，包含n个采样，惯组误差参数的误差（微分），pAppr-pTrue，包含的域见accerror及gyroerror函数，本函数使用了零偏、标度因数误差、失准角等域
% # IMUPar: 结构体，包含n个采样，惯组误差参数，包含的域见accerror及gyroerror函数，除PS0B域外，其它域对计算结果的影响较小
% # DupsilonBl: 3*n向量，修正前的比速度增量，单位m/s
% # DalphaBl: 3*n向量，修正前的角增量，单位rad
% # Tl: 标量，采样周期，单位s
% # DalphaBl1: 3*1向量，DalphaBl首周期前一周期的角增量（用于连续采样）；或3*n矩阵，DalphaBl各周期前一周期的角增量（用于不连续采样），单位rad
%
% Output Arguments
% # DupsilonBlCorr: 3*n向量，比速度增量修正量，DupsilonBlTrue-DupsilonBlAppr，单位m/s^2
% # DalphaBlCorr: 3*n向量，角增量修正量，DalphaBlTrue-DalphaBlAppr，单位rad/s
%
% References
% # 《应用导航算法工程基础》 误差参数残差补偿算法

DupsilonBlCorr = coder.nullcopy(NaN(size(DupsilonBl)));
DalphaBlCorr = coder.nullcopy(NaN(size(DalphaBl)));

for iSamp=1:size(DupsilonBl, 2)
    if size(DalphaBl1, 2) == 1
        if iSamp == 1
            DalphaBl1_effective = DalphaBl1;
        else
            DalphaBl1_effective = DalphaBl(:, iSamp-1);
        end
    else % 传入所有周期的前点值，在测试用例中使用
        DalphaBl1_effective = DalphaBl1(:, iSamp);
    end
    [dDupsilonTildeOutSl_par, dDalphaTildeOutSl_par, accPSBT, gyroPSBT, dupsilonCrsDepSl, dalphaCrsDepSl, accdKScalCoef, gyrodKScalCoef] ...
        = imuoutdiff_par(IMUParDiff, IMUPar, DupsilonBl, DalphaBl, DalphaBl1_effective, iSamp, Tl);
    [~, accJf, accJg] = fgjacobian_vB(accPSBT*DupsilonBl(:, iSamp), accPSBT, IMUPar.acc.KScal0(:, iSamp), accdKScalCoef, ...
        IMUPar.acc.dfBiasS(:, iSamp)*Tl, IMUPar.acc.dfQuantS(:, iSamp)*Tl, dupsilonCrsDepSl, Tl);
    [~, gyroJf, gyroJg] = fgjacobian_vB(gyroPSBT*DalphaBl(:, iSamp), gyroPSBT, IMUPar.gyro.KScal0(:, iSamp), gyrodKScalCoef, ...
        IMUPar.gyro.domegaIBBiasS(:, iSamp)*Tl, IMUPar.gyro.domegaIBQuantS(:, iSamp)*Tl, dalphaCrsDepSl, Tl);
    DupsilonBlCorr(:, iSamp) = (accJf+accJg) \ dDupsilonTildeOutSl_par;
    DalphaBlCorr(:, iSamp) = (gyroJf+gyroJg) \ dDalphaTildeOutSl_par;
end
end