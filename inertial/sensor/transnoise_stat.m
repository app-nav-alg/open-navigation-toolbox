function [accOut, gyroOut] = transnoise_stat(destAccOut, destGyroOut, destDt, destKBgn, destKEnd, destIMUPar, srcAccOut, srcGyroOut, srcDt, srcKBgn, srcKEnd, srcIMUPar, n)
% TRANSNOISE_STAT 提取一组惯性器件输出中的噪声并转移到另一组惯性器件输出中
%
% Input Arguments:
% destAccOut: 3*m矩阵，目标数据中的加表输出
% destGyroOut: 3*m矩阵，目标数据中的陀螺输出
% destDt: 标量，目标数据的采样时间间隔，单位s
% destKBgn: p*1向量，p为静止位置个数，目标数据中的各静止段起始点索引
% destKEnd: p*1向量，p为静止位置个数，目标数据中的各静止段终止点索引
% destIMUPar: 目标数据对应的器件参数
% destIMUPar.acc.B: 3*1向量，加速度计零偏，单位m/s^2
% destIMUPar.acc.SF: 3*1向量，加速度计标度因数，单位(^/s)/(m/s^2)
% destIMUPar.acc.MA: 3*3矩阵，加速度计失准角，单位rad
% destIMUPar.acc.SO: 3*1向量，加速度计二次项系数，单位1/(m/s^2)
% destIMUPar.gyro.B: 3*1向量，陀螺零偏，单位rad/s
% destIMUPar.gyro.SFp: 3*1向量，陀螺正向标度因数，单位(^/s)/(rad/s)
% destIMUPar.gyro.SFn: 3*1向量，陀螺负向标度因数，单位(^/s)/(rad/s)
% destIMUPar.gyro.MA: 3*3矩阵，陀螺失准角，单位rad
% srcAccOut: 3*m矩阵，源数据中的加表输出
% srcGyroOut: 3*m矩阵，源数据中的陀螺输出
% srcDt: 标量，源数据的采样时间间隔，单位s
% srcKBgn: p*1向量，p为静止位置个数，源数据中的各静止段起始点索引
% srcKEnd: p*1向量，p为静止位置个数，源数据中的各静止段终止点索引
% srcIMUPar: 源数据对应的器件参数
% srcIMUPar.acc.B: 3*1向量，加速度计零偏，单位m/s^2
% srcIMUPar.acc.SF: 3*1向量，加速度计标度因数，单位(^/s)/(m/s^2)
% srcIMUPar.acc.MA: 3*3矩阵，加速度计失准角，单位rad
% srcIMUPar.acc.SO: 3*1向量，加速度计二次项系数，单位1/(m/s^2)
% srcIMUPar.gyro.B: 3*1向量，陀螺零偏，单位rad/s
% srcIMUPar.gyro.SFp: 3*1向量，陀螺正向标度因数，单位(^/s)/(rad/s)
% srcIMUPar.gyro.SFn: 3*1向量，陀螺负向标度因数，单位(^/s)/(rad/s)
% srcIMUPar.gyro.MA: 3*3矩阵，陀螺失准角，单位rad
% n: 标量，提取噪声时对应的多项式拟合阶数，默认为2
%
% Output Arguments:
% accOut: 3*m矩阵，添加噪声后的目标数据加表输出
% gyroOut: 3*m矩阵，添加噪声后的目标数据陀螺输出

if nargin < 13
    n = 2;
end

%% 使用源参数将脉冲数转换为比速度增量和角度增量
assert(size(srcKBgn, 1) == size(srcKEnd, 1));
assert(size(srcAccOut, 2) == size(srcGyroOut, 2));
mid_posNum = size(srcKBgn, 1);
mid_totalLength = size(srcAccOut, 2);
mid_deltaV = NaN(mid_totalLength, 3);
mid_deltaPhi = NaN(mid_totalLength, 3);
mid_flag = false(mid_totalLength, 1);
for j=1:mid_posNum
    mid_deltaV(srcKBgn(j):srcKEnd(j), :) = sensorout2in_newton(srcAccOut(:, srcKBgn(j):srcKEnd(j)), srcIMUPar.acc.SF, srcIMUPar.acc.B*srcDt, srcIMUPar.acc.MA, srcIMUPar.acc.SO/srcDt)';
    mid_deltaPhi(srcKBgn(j):srcKEnd(j), :) = sensorout2in_linear(srcGyroOut(:, srcKBgn(j):srcKEnd(j)), srcIMUPar.gyro.SFp, srcIMUPar.gyro.B*srcDt, srcIMUPar.gyro.MA)';
    mid_flag(srcKBgn(j):srcKEnd(j)) = true;
end

%% 使用多项式拟合的方法提取静止段的噪声
mid_accNoise = detrend_poly_stat(mid_deltaV, mid_flag, false, n);
mid_gyroNoise = detrend_poly_stat(mid_deltaPhi, mid_flag, false, n);

%% 将噪声添加到目标输出数据
assert(srcDt <= destDt);
mid_cycle = int32(destDt/srcDt);
accOut = destAccOut;
gyroOut = destGyroOut;
for j=1:mid_posNum
    % 得到本静止段的噪声数据，将噪声的时间间隔从srcDt扩大到desDt
    tmp_accNoise = clusterstat(mid_accNoise(srcKBgn(j):srcKEnd(j), :), 'sum', mid_cycle);
    tmp_gyroNoise = clusterstat(mid_gyroNoise(srcKBgn(j):srcKEnd(j), :), 'sum', mid_cycle);
    % 使用目标参数将比速度增量噪声和角度增量噪声转换为脉冲数
    assert(size(tmp_accNoise, 1) == size(tmp_gyroNoise, 1));
    tmp_length = size(tmp_accNoise, 1);
    tmp_accNoisePulse = NaN(tmp_length, 3);
    tmp_gyroNoisePulse = NaN(tmp_length, 3);
    for k=1:tmp_length
        tmp = accerror(tmp_accNoise(k,:), destIMUPar.acc.B'*destDt, destIMUPar.acc.dSF', destIMUPar.acc.MA, destIMUPar.acc.SO'/destDt, false);
        tmp_accNoisePulse(k,:) = destIMUPar.acc.SF0 .* (tmp_accNoise(k,:) + tmp)';
        tmp = gyroerror(tmp_gyroNoise(k,:), destIMUPar.gyro.B'*destDt, destIMUPar.gyro.dSFp', destIMUPar.gyro.dSFn', destIMUPar.gyro.MA);
        tmp_gyroNoisePulse(k,:) = destIMUPar.gyro.SF0 .* (tmp_gyroNoise(k,:) + tmp)';
    end
    % 添加噪声
    tNum = destKEnd(j) - destKBgn(j) + 1;
    assert(tNum <= tmp_length);
    accOut(destKBgn(j):destKEnd(j), :) = accOut(destKBgn(j):destKEnd(j), :) + tmp_accNoisePulse(1:tNum, :);
    gyroOut(destKBgn(j):destKEnd(j), :) = gyroOut(destKBgn(j):destKEnd(j), :) + tmp_gyroNoisePulse(1:tNum, :);
end

end