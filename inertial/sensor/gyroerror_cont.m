function [ domegaIBB, omegaTildeIBS ] = gyroerror_cont( omegaIBB, gyroPar, fB, ignore2Od ) %#codegen
%GYROERROR_CONT 计算陀螺器件误差引入的测量误差
%
% Tips
% # 本函数针对连续形式
%
% Input Arguments
% # omegaIBB: 3*n矩阵，n为采样个数，角速度，单位rad/s
% # gyroPar: 各域为数组的结构体，陀螺的确定性误差参数，各域最后一维为时间m
%        gyroPar.KScal0: 3*n向量，陀螺标度因数标称值，单位：1rad对应的脉冲数
%        gyroPar.PS0B：3*3*n矩阵，传感器系与本体系之间的坐标转换矩阵标称值，无单位，默认全为单位阵
%        gyroPar.domegaIBBiasS: 3*n向量，零偏，单位：rad/s
%        gyroPar.dKScalP: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，陀螺正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(rad/s)^-(i-1)，默认全为0
%        gyroPar.dKScalN: 3*x*n数组，x为考虑的标度因数误差的阶次，x=1，模型为线性，陀螺正向标度因数误差，单位与阶次相关，第1列无单位，第i列单位(rad/s)^-(i-1)，默认全为0
%        gyroPar.dPSS0：3*3*n矩阵，失准角（安装误差），无单位，默认全为0
%        gyroPar.domegaIBQuantS: 3*n向量，量化误差，单位：rad/s，默认全为0
%        gyroPar.DGSens: 3*3*n矩阵，g敏感项系数，单位：(rad/s)/(m/s^2)，默认全为0
% # ignore2Od: 逻辑标量，true表示忽略2阶误差，默认为true
% # fB: 3*n矩阵，比力，单位m/s^2
%
% Output Arguments
% # domegaIBB: 3*n矩阵，角速度误差，单位rad/s
% # omegaTildeIBS: 3*n矩阵，各采样时刻的S系下的比力（包含测量误差），单位rad/s
%
% References:
% # 《应用导航技术》“惯性传感器模型” 总误差模型

if nargin < 4
    ignore2Od = true;
end
n = size(omegaIBB, 2);

domegaIBB = coder.nullcopy(NaN(3, n, class(omegaIBB)));
if nargout > 1
    omegaTildeIBS = coder.nullcopy(NaN(3, n, class(omegaIBB)));
end
for j=1:n
    %% 计算陀螺仪各项误差
    % 陀螺g敏感项
    domegaIBGSensS = gyroPar.DGSens(:, :, j)*fB(:, j);
    %% 计算陀螺角速度输出值omegaTildeIBS与角速度理论值omegaIBB之差
    % 不加标度因数误差阵的角速度输出
    domegaBiasExS = gyroPar.domegaIBBiasS(:, j) + domegaIBGSensS + gyroPar.domegaIBQuantS(:, j);
    omegaIBS = (gyroPar.PS0B(:, :, j)*(eye(3)+gyroPar.dPSS0(:, :, j)))'*omegaIBB(:, j);
    omegaTildeIBScalS = omegaIBS + gyroPar.domegaIBBiasS(:, j) + domegaIBGSensS;
    % 计算j时刻标度因数误差阵
    gyrodKScalCoef = scalfacterrsign(gyroPar.dKScalP(:, :, j), gyroPar.dKScalN(:, :, j), omegaTildeIBScalS);
    % 计算dKScal
    dKScal = polyscalfact(omegaTildeIBScalS, gyrodKScalCoef); % NOTE: vTildeScalS计算式应与fgjacobian及imuout2in函数一致
    % 忽略二阶误差的陀螺误差
    dPSBT = (gyroPar.PS0B(:, :, j)*gyroPar.dPSS0(:, :, j))';
    domegaIBB(:, j) = (gyroPar.PS0B(:, :, j)')\(dKScal.*(gyroPar.PS0B(:, :, j)'*omegaIBB(:, j))+dPSBT*omegaIBB(:, j)+domegaBiasExS);
    if ~ignore2Od
        % 考虑二阶误差的加速度计误差
        domegaIBB2 = (gyroPar.PS0B(:, :, j)')\(dKScal.*(dPSBT*omegaIBB(:, j)+domegaBiasExS));
        domegaIBB(:, j) = domegaIBB(:, j)+domegaIBB2;
    end
    if nargout > 1
        omegaTildeIBS(:, j) = (ones(3, 1)+dKScal) .* (omegaIBS+domegaBiasExS);
    end
end
end