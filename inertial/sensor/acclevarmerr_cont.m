function [ dfSizeS ] = acclevarmerr_cont( PSBT, lB, omegaIBB, omegaIBBRate )
%ACCLEVARMERR_CONT 计算加速度计三元组中杆臂效应带来的比力测量误差
%
% Tips
% # 本函数针对连续形式
%
% Input Arguments
% # PSBT: 3*3矩阵，加速度计斜交安装时，本体系到传感器系的坐标转换矩阵，无单位，默认全为单位阵
% # lB: 3*3矩阵，按列分别为3只加速度计的杆臂项，单位：m，默认全为0
% # omegaIBB: 3*1向量，角速度，单位：rad/s
% # omegaIBBRate: 3*1向量，角速度变化率，单位：rad/s^2
%
% Output Arguments
% # dfSizeS：3*1向量，加速度计三元组中杆臂效应带来的比力测量误差，单位：m/s^2
%
% References
% # 《应用导航算法工程基础》“加速度计杆臂效应”

dfSizeS = [PSBT(1, :)*(cross(omegaIBB, cross(omegaIBB, lB(:, 1)))+cross(omegaIBBRate, lB(:, 1)));
    PSBT(2, :)*(cross(omegaIBB, cross(omegaIBB, lB(:, 2)))+cross(omegaIBBRate, lB(:, 2)));
    PSBT(3, :)*(cross(omegaIBB, cross(omegaIBB, lB(:, 3)))+cross(omegaIBBRate, lB(:, 3)))];
end