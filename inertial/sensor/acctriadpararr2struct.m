function [structPar] = acctriadpararr2struct(arrPar, indexMap, setNonExistParToNaN, keepAllNaNField)
%ACCTRIADPARARR2STRUCT 将以二维数组形式保存的加速度计三元组的参数转换为结构体形式保存的参数
%
% Input Arguments
% # arrPar: 二维数组形式保存的参数，每行为同一时刻的各项参数，每列为同一参数的各时刻值，参数详细说明参考accerror_cont函数
% # indexMap: 1*84向量，绝对值依次为加速度计标度因数标称值（X、Y、Z，下同）（3）、非正交传感器组的安装矩阵标称值（按列展开）（9）、零偏（3）、正向标度因数误差（到3阶，按列展开）（9）、
%           负向标度因数误差（到3阶，按列展开）（9）、安装误差矩阵（按列展开）（9）、量化误差（3）、加速度计的安装杆臂在B系的投影（9）、加速度计不等惯量误差系数（3）、B系到各轴加速度计坐标系的变换矩阵（分别按列展开）（27）
%           在arrPar中的列数，NaN表示不存在，负数表示取负值。
% # setNonExistParToNaN: 逻辑标量，true表示存在的参数类型域中不存在的参数项设置为NaN，否则标度因数标称值设置为1，其它类型参数设置为0，默认为false
% # keepAllNaNField: 逻辑标量，是否对indexMap中元素全为NaN的域赋默认值，默认为false
%
% Output Arguments
% # structPar: 仅含有indexMap中包含的参数类型域，时间在各域的最后一维上

% NOTE: 本函数使用线性索引
if nargin < 3
    setNonExistParToNaN = false;
end

if nargin < 4
    keepAllNaNField = false;
end

if (length(indexMap) ~= 84)
    error('Length of indexMap should be 84.');
end

indexMapStruct.KScal0 = indexMap(1:3)';
indexMapStruct.PS0B = reshape(indexMap(4:12), 3, 3);
indexMapStruct.dfBiasS = indexMap(13:15)';
indexMapStruct.dKScalP = reshape(indexMap(16:24), 3, 3);
indexMapStruct.dKScalN = reshape(indexMap(25:33), 3, 3);
indexMapStruct.dPSS0 = reshape(indexMap(34:42), 3, 3);
indexMapStruct.dfQuantS = indexMap(43:45)';
indexMapStruct.lB = reshape(indexMap(46:54), 3, 3);
indexMapStruct.KAniso = indexMap(55:57)';
indexMapStruct.CBA = reshape(indexMap(58:84), 3, 3, 3);

if setNonExistParToNaN
    nonExistEleVal = NaN;
else
    nonExistEleVal.KScal0 = ones(3,1);
    nonExistEleVal.PS0B = eye(3);
    nonExistEleVal.dfBiasS = zeros(3,1);
    nonExistEleVal.dKScalP = zeros(3,3);
    nonExistEleVal.dKScalN = zeros(3,3);
    nonExistEleVal.dPSS0 = zeros(3,3);
    nonExistEleVal.dfQuantS = zeros(3,1);
    nonExistEleVal.lB = zeros(3,3);
    nonExistEleVal.KAniso = zeros(3,1);
    nonExistEleVal.CBA = zeros(3,3,3);
end
[structPar] = array2struct(arrPar, indexMapStruct, nonExistEleVal, keepAllNaNField);
end