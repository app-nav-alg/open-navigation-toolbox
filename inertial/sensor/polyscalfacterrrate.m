function [ dKScalRate ] = polyscalfacterrrate(vTildeScalS, dKScalCoef, Dt)
%POLYSCALFACTERRRATE 计算dKScal相对于vTildeScalS的导数
%
% Input Arguments
% # vTildeScalS：3*1向量，传感器坐标系下进入标度因数缩放环节的三传感器组测量值（或对应的积分增量）
% # dKScalCoef：3*x矩阵，x为阶数，统一正负向之后的标度因数误差多项式系数
% # Dt: 标量，采样周期（当vTildeScalS为增量时），单位s，默认为瞬时值
%
% Output Arguments
% # dKScalRate: 9*3矩阵，标度因数阵对vTildeScalS求导
%
% References
% # 《应用导航算法工程基础》“惯性传感器模型” 标度因数误差

if nargin < 3
    Dt = [];
end

% 对于增量值，1次及以上系数需要除以Dt的相应次数
if ~isempty(Dt)
    for i=2:size(dKScalCoef, 2)
        dKScalCoef(:, i) = dKScalCoef(:, i) / (Dt^(i-1));
    end
end

dKScalRate = zeros(9, 3);
for idKscal=2:size(dKScalCoef, 2)
    dKScalRate(1, 1) = dKScalRate(1, 1) + (idKscal-1)*dKScalCoef(1, idKscal)*vTildeScalS(1).^(idKscal-2);
    dKScalRate(5, 2) = dKScalRate(5, 2) + (idKscal-1)*dKScalCoef(2, idKscal)*vTildeScalS(2).^(idKscal-2);
    dKScalRate(9, 3) = dKScalRate(9, 3) + (idKscal-1)*dKScalCoef(3, idKscal)*vTildeScalS(3).^(idKscal-2);
end
end