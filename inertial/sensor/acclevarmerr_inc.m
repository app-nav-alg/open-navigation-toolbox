function [ dfSizeSInt ] = acclevarmerr_inc( PSBT, lB, DalphaBl, DalphaBl1, Tl )
%ACCLEVARMERR_INC 计算加速度计三元组中增量形式的杆臂效应带来的比力测量误差
%
% Tips
% # 本函数针对增量形式
%
% Input Arguments
% # PSBT: 3*3矩阵，加速度计斜交安装时，本体系到传感器系的坐标转换矩阵，无单位，默认全为单位阵
% # lB: 3*3矩阵，按列分别为3只加速度计的杆臂项，单位：m，默认全为0
% # DalphaBl: 3*1向量，角度增量，单位：rad
% # DalphaBl1: 3*1向量，前一周期的角度增量，单位：rad
% # Tl: 标量，单位：s
%
% Output Arguments
% # dfSizeSInt：3*1向量，加速度计三元组中增量形式的杆臂效应带来的比力测量误差，单位：m/s
%
% References:
% # 《应用导航算法工程基础》“加速度计杆臂效应”

dfSizeSInt  = [acclevarmerrelem_inc(PSBT(1, :), lB(:, 1), DalphaBl, DalphaBl1, Tl);
    acclevarmerrelem_inc(PSBT(2, :), lB(:, 2), DalphaBl, DalphaBl1, Tl);
    acclevarmerrelem_inc(PSBT(3, :), lB(:, 3), DalphaBl, DalphaBl1, Tl)];
end

function [ dfSizeSInti ] = acclevarmerrelem_inc( PSBTRi, lBCi, DalphaBl, DalphaBl1, Tl )
%ACCLEVARMERRELEM_INC 计算加速度计三元组中增量形式的杆臂效应带来的比力测量误差的分量
%
% Tips:
% # 本函数针对增量形式
%
% Input Arguments:
% # PSBTRi: PSBT的第i行
% # lBCi: lB的第i列
% # DalphaBl: 3*1向量，角度增量，单位：rad
% # DalphaBl1: 3*1向量，前一周期的角度增量，单位：rad
% # Tl: 标量，单位：s
%
% Output Arguments:
% # dfSizeSInti：dfSizeSInt的第i个分量
%
% References:
% # 《应用导航算法工程基础》“加速度计杆臂效应”

dfSizeSInti = PSBTRi*(13/(12*Tl)*cross(DalphaBl, cross(DalphaBl, lBCi)) ...
    - 1/(12*Tl)*cross(DalphaBl, cross(DalphaBl1, lBCi)) ...
    - 1/(12*Tl)*cross(DalphaBl1, cross(DalphaBl, lBCi)) ...
    + 1/(12*Tl)*cross(DalphaBl1, cross(DalphaBl1, lBCi)) ...
    + 1/Tl*cross((DalphaBl-DalphaBl1), lBCi)); % NOTE: 增量形式在测试时传入的DalphaBl1为理想值，DalphaBl为计算值，因此对应的omegaIBBRate是有误差的，而连续形式测试时的omegaIBBRate是理想值
end