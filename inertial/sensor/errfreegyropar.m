function [ gyroPar ] = errfreegyropar(m, dKScalOrder)
%ERRFREEGYROPAR 生成无误差的陀螺参数
%
% Input Arguments
% # m: 标量，误差采样数，默认为1
% # dKScalOrder: 标量，标度因数误差的阶次，默认为1
%
% Output Arguments
% # gyroPar: 陀螺的确定性误差参数，说明见gyroerror_cont函数

if nargin < 1
    m = 1;
end
if nargin < 2
    dKScalOrder = 1;
end

gyroPar.KScal0 = ones(3, m);
gyroPar.PS0B = repmat(eye(3), [1 1 m]);
gyroPar.domegaIBBiasS = zeros(3, m);
gyroPar.dKScalP = zeros(3, dKScalOrder, m);
gyroPar.dKScalN = zeros(3, dKScalOrder, m);
gyroPar.dPSS0 = zeros(3, 3, m);
gyroPar.domegaIBQuantS = zeros(3, m);
gyroPar.DGSens = zeros(3, 3, m);
end