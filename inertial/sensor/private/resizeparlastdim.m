function [par] = resizeparlastdim(par, lastDimTargetSize, nParDim, parName)
%RESIZEPARLASTDIM 调整2维、3维或4维参数最后一维的尺寸至lastDimTargetSize

if nParDim == 2
    lastDimSize = size(par, 2);
    if lastDimSize < lastDimTargetSize
        par = [par repmat(par(:, end), 1, lastDimTargetSize-lastDimSize)];
        if lastDimSize ~= 1
            warning('resizeparlastdim:wrongparsize', [parName '列数小于目标尺寸（' int2str(lastDimTargetSize) '），用最后一列补齐。']);
        end
    elseif lastDimSize > lastDimTargetSize
        par = par(:, 1:lastDimTargetSize);
        warning('resizeparlastdim:wrongparsize', [parName '列数大于目标尺寸（' int2str(lastDimTargetSize) '），截掉多余列。']);
    end
elseif nParDim == 3
    lastDimSize = size(par, 3);
    if lastDimSize < lastDimTargetSize
        par = cat(3, par, repmat(par(:, :, end), [1 1 lastDimTargetSize-lastDimSize]));
        if lastDimSize ~= 1
            warning('resizeparlastdim:wrongparsize', [parName '页数小于目标尺寸（' int2str(lastDimTargetSize) '），用最后一页补齐。']);
        end
    elseif lastDimSize > lastDimTargetSize
        par = par(:, :, 1:lastDimTargetSize);
        warning('resizeparlastdim:wrongparsize', [parName '页数大于目标尺寸（' int2str(lastDimTargetSize) '），截掉多余页。']);
    end
elseif nParDim == 4
    lastDimSize = size(par, 4);
    if lastDimSize < lastDimTargetSize
        par = cat(4, par, repmat(par(:, :, :, end), [1 1 1 lastDimTargetSize-lastDimSize]));
        if lastDimSize ~= 1
            warning('resizeparlastdim:wrongparsize', [parName '第4维尺寸小于目标尺寸（' int2str(lastDimTargetSize) '），用最后一项前三维块补齐。']);
        end
    elseif lastDimSize > lastDimTargetSize
        par = par(:, :, :, 1:lastDimTargetSize);
        warning('resizeparlastdim:wrongparsize', [parName '第4维尺寸大于目标尺寸（' int2str(lastDimTargetSize) '），截掉多余的前三维块。']);
    end
else
    assert(false);
end
end