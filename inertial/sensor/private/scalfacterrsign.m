function [dKScalCoef] = scalfacterrsign(dKScalCoefP, dKScalCoefN, vTildeScalS) %#codegen
%SCALFACTERRSIGN 确定标度因数误差使用正向值还是负向值

assert(size(dKScalCoefP, 2) == size(dKScalCoefN, 2));
dKScalCoef = coder.nullcopy(NaN(3, size(dKScalCoefP, 2), class(dKScalCoefP)));

if isempty(dKScalCoefN)
    dKScalCoef = dKScalCoefP(:, :);
else
    for iAxis=1:3
        if (isa(vTildeScalS, 'sym') && isAlways(vTildeScalS(iAxis, 1) < 0)) ...
                || ~(isa(vTildeScalS, 'sym') && (vTildeScalS(iAxis, 1) < 0))
            dKScalCoef(iAxis, :) = dKScalCoefN(iAxis, :);
        else
            dKScalCoef(iAxis, :) = dKScalCoefP(iAxis, :);
        end
    end
end
end