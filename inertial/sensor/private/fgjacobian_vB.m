function [ KScal, Jf, Jg, JfCond ] = fgjacobian_vB(vS, PSBT, KScal0, dKScalCoef, dvBiasS, dvQuantS, dvCrsDepS, Tl)
%FGJACOBIAN_VB 计算f(v, p)及g(v, w, p)相对于vB的雅克比矩阵
%
% Input Arguments
% # vS: 3*1向量，传感器坐标系的比力或角速度真实值（或对应的积分增量）
% # PSBT: 3*3矩阵，加速度计斜交安装时，本体系到传感器系的坐标转换矩阵，无单位
% # KScal0: 3*1向量，标度因数的标称值
% # dKScalCoef: 3*x矩阵，x为阶数，统一正负向之后的标度因数误差多项式系数
% # dvBiasS: 3*1向量，S系下零偏，当vS为增量时，该值应为增量
% # dvQuantS: 3*1向量，S系下量化误差，当vS为增量时，该值应为增量
% # dvCrsDepS: 3*1向量，dvS中与w相关的项，加速度计为杆臂效应误差+不等惯量误差；陀螺为g敏感项误差，当vS为增量时，该值应为增量，默认全为0
% # Tl: 标量，采样周期（当vS为增量时），单位s，默认为空（瞬时值）
%
% Output Arguments
% # KScal: 3*1向量，标度因数
% # Jf: 3*3矩阵，f相对于vB的雅可比矩阵
% # Jg: 3*3矩阵，g相对于vB的雅可比矩阵
% # JfCond: 标量，Jf的条件数
%
% References
% # 《应用导航技术》“惯组误差模型-由输出值计算输入值的算法”

if nargin < 8
    Tl = [];
end
if nargin < 7
    dvCrsDepS = zeros(3, 1);
end

% 计算KScal(vS)和dKScal'(vS)
[~, KScal] = polyscalfact(vS+dvBiasS+dvCrsDepS, dKScalCoef, KScal0, Tl); % NOTE: vTildeScalS计算式应与accerror、gyroerror及imuout2in函数一致
dKScalRate = polyscalfacterrrate(vS+dvBiasS+dvCrsDepS, dKScalCoef, Tl);
% 计算Jf(vk, p)
Jf = kron((vS+dvBiasS+dvQuantS)', diag(KScal0))*dKScalRate*PSBT + diag(KScal)*PSBT;
if nargout > 2
    % 计算Jg(vk, p)
    Jg = kron(dvCrsDepS', diag(KScal0))*dKScalRate*PSBT;
end
if nargout > 3
    JfCond = cond(Jf);
end
end