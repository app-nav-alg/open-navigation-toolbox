function [ CBL ] = coarsealign_stat_rv2( fB, omegaIBB ) %#codegen
%COARSEALIGN_STAT_RV2 解析粗对准，参考向量为重力加速度、重力加速度与和地球自转角速度的叉积
%
% Tips:
% # 参考坐标系L为NED
%
% Input Arguments:
% # fB: 3*1向量，加速度计测量到的比力（负的重力加速度）
% # omegaIBB: 3*1向量，陀螺测量到的地球自转角速度
%
% Output Arguments:
% # CBL: 3*3矩阵，方向余弦矩阵
%
% References:
% # 《应用导航算法工程基础》“初始方向余弦矩阵正交化的两种算法”中的第二种算法

CBL = coder.nullcopy(NaN(3, 3));
CBL(3, :) = -fB' / norm(fB);
CBL(2, :) = -cross(fB, omegaIBB)';
CBL(2, :) = CBL(2, :) / norm(CBL(2, :));
CBL(1, :) = cross(CBL(2, :), CBL(3, :));
end