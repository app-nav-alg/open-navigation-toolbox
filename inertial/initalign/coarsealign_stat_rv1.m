function [ CBL ] = coarsealign_stat_rv1( fB, omegaIBB, L, g, omegaIE )
%COARSEALIGN_STAT_RV1 解析粗对准，参考向量为重力加速度、地球自转角速度
%
% Tips:
% 1. 参考坐标系L为NED
%
% Input Arguments:
% fB: 3*1向量，加速度计测量到的比力（负的重力加速度）
% omegaIBB: 3*1向量，陀螺测量到的地球自转角速度
% L: 标量，纬度值，单位rad
% g: 标量，当地重力加速度值，单位同fb
% omegaIE: 标量，地球自转角速率值，单位同omegaibb
%
% Output Arguments:
% CBL: 3*3矩阵，方向余弦矩阵
%
% References:
% [1] 理论文档“两种形式的解析粗对准算法及其等效性”的第一种形式

gL = [0 0 g]';
omegaIEL = [omegaIE*cos(L), 0, -omegaIE*sin(L)]';
CBL = dcmnormortho_optimized(([-fB omegaIBB cross(-fB, omegaIBB)] / [gL omegaIEL cross(gL, omegaIEL)])');
end