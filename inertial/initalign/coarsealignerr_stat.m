function [phiLInit] = coarsealignerr_stat(dfL, domegaIBL, g, omegaIE, L, refVecGrp) %codegen
%COARSEALIGNERR_STAT 计算解析粗对准姿态误差
%
% Input Arguments:
% # dfL: 3*1向量，l系（NED）下的加速度计误差，单位m/s^2
% # domegaIBL: 3*1向量，l系下的陀螺误差，单位rad/s
% # g: 标量，重力加速度，单位m/s^2
% # omegaIE: 标量，地球自转角速率，单位rad/s
% # L: 标量，纬度，单位rad
% # refVecGrp: 标量，参考向量组数，1表示参考向量为重力加速度及地球自转角速度，其它表示参考向量为重力加速度及重力加速度和地球自转角速度的叉积
%
% Output Arguments:
% # phiLInit: 3*1向量，l系下的解析粗对准姿态误差，单位rad
%
% References:
% # 《应用导航算法工程基础》“解析粗对准姿态误差”

if refVecGrp == 1
    phiLInit = [dfL(2, 1)/g;
        -1/2*(dfL(1, 1)/g-dfL(3, 1)*tan(L)/g+domegaIBL(3, 1)/omegaIE/cos(L));
        domegaIBL(2, 1)/(omegaIE*cos(L))-dfL(2, 1)*tan(L)/g];
else
    phiLInit = [dfL(2, 1)/g;
        -dfL(1, 1)/g;
        domegaIBL(2, 1)/(omegaIE*cos(L))-dfL(2, 1)*tan(L)/g];
end
end