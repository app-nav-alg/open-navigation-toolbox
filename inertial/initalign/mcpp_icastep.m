function [out] = mcpp_icastep(in)
% 对准蒙特卡洛仿真后处理函数

dataDir = [pwd '\data\'];
for j = 1:length(in.grpSimNum) % 多组
    close all;
    for i = 1:in.grpSimNum(j)
        filename = [dataDir int2str(j) '_' int2str(i)];
        if isdir(filename)
            load([filename '\对准输出分析结果.mat'], 'out_trajGen', 'phiL_CARV2', 'phiL_ICAS', 'out_CARV2', 'out_ICAS');
            % 姿态
            if i == 1
                attError_CARV2 = NaN(length(phiL_CARV2), 3, in.grpSimNum(j));
                attError_ICAS = NaN(length(phiL_ICAS), 3, in.grpSimNum(j));
            end
            attError_CARV2(:, :, i) = phiL_CARV2./pi*180*3600; % 角秒
            attError_ICAS(:, :, i) = phiL_ICAS./pi*180*3600; % 角秒
        end
    end
    % 绘制方位误差分布图
    preparefig('解析式各次仿真方位误差离散图');
    stem(reshape(attError_CARV2(end, end, :), 1, in.grpSimNum(j)));
    xlabel('仿真次数 / 次'); ylabel('方位误差 / ″');
    preparefig('解析式方位误差频数直方图');
    hist(reshape(attError_CARV2(end, end, :), 1, in.grpSimNum(j)), 20);
    xlabel('方位误差 / ″'); ylabel('频数 / 次');
    
    preparefig('惯性系各次仿真方位误差离散图');
    stem(reshape(attError_ICAS(end, end, :), 1, in.grpSimNum(j)));
    xlabel('仿真次数 / 次'); ylabel('方位误差 / ″');
    preparefig('惯性系方位误差频数直方图');
    hist(reshape(attError_ICAS(end, end, :), 1, in.grpSimNum(j)), 20);
    xlabel('方位误差 / ″'); ylabel('频数 / 次');
    % 绘制收敛图
    t = out_trajGen.t(2:end, 1);
    fig1 = preparefig('phi_x_CARV2收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_CARV2(:, 1, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_x^L / (\prime\prime)');
    fig2 = preparefig('phi_y_CARV2收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_CARV2(:, 2, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_y^L / (\prime\prime)');
    fig3 = preparefig('phi_z_CARV2收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_CARV2(:, 3, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_z^L / (\prime\prime)');
    
    fig4 = preparefig('phi_x_ICAS收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_ICAS(:, 1, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_x^L / (\prime\prime)');
    fig5 = preparefig('phi_y_ICAS收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_ICAS(:, 2, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_y^L / (\prime\prime)');
    fig6 = preparefig('phi_z_ICAS收敛图');
    for i = 1:in.grpSimNum(j)
        plot(t, attError_ICAS(:, 3, i)); hold on;
    end
    grid on; xlabel('time / s'); ylabel('\phi_z^L / (\prime\prime)');
    % RMS统计
    out.phiRMS_CARV2 = 3*rms(attError_CARV2, 3);
    out.phiRMS_ICAS = 3*rms(attError_ICAS, 3);
    % 绘制姿态误差
    t = out_trajGen.t(2:end, 1);
    preparefig('phi_x_CARV2',fig1,false);
    plot(t, out.phiRMS_CARV2(:, 1), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_x^L / (\prime\prime)');
    preparefig('phi_y_CARV2',fig2,false);
    plot(t, out.phiRMS_CARV2(:, 2), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_y^L / (\prime\prime)');
    preparefig('phi_z_CARV2',fig3,false);
    plot(t, out.phiRMS_CARV2(:, 3), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_z^L / (\prime\prime)');
    
    preparefig('phi_x_ICAS',fig4,false);
    plot(t, out.phiRMS_ICAS(:, 1), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_x^L / (\prime\prime)');
    preparefig('phi_y_ICAS',fig5,false);
    plot(t, out.phiRMS_ICAS(:, 2), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_y^L / (\prime\prime)');
    preparefig('phi_z_ICAS',fig6,false);
    plot(t, out.phiRMS_ICAS(:, 3), 'r-', 'LineWidth', 2);
    grid on; xlabel('time / s'); ylabel('\phi_z^L / (\prime\prime)');
    
    %% 生成报告
    saveDataDir = [dataDir '第' int2str(j) '组蒙特卡洛计算结果\'];
    if ~isdir(saveDataDir)
        mkdir(saveDataDir);
    end
    diary([saveDataDir int2str(in.grpSimNum(j)) '次蒙特卡洛计算结果.txt']);
    diary on;
    % 输出时间
    disp(['时间：' datestr(now)]);
    % 输出最终姿态误差
    disp('解析式对准姿态误差3*RMS（单位 角秒）');
    disp(out.phiRMS_CARV2(end, :));
    disp('惯性系对准姿态误差3*RMS（单位 角秒）');
    disp(out.phiRMS_ICAS(end, :));
    diary off;
    saveallfigs(saveDataDir, 'fig', false);
end
end