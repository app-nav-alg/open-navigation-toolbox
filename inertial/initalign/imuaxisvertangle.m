function [ vertAngle ] = imuaxisvertangle( pulseRate, aSF0, aB, adSFMA )
%IMUAXISVERTANGLE 利用加速度计输出计算惯组坐标系各轴与水平面的夹角
%
% Input Arguments:
% pulseRate: 3*1向量，加速度计每秒输出的脉冲数，单位^/s
% aSF0: 3*1向量，加速度计标度因数，单位^/(m/s)
% aB: 3*1向量，加速度计零偏，单位m/s^2
% adSFMA: 3*3矩阵，加速度计标度因数误差与失准角之和，无单位（单位rad）
%
% Output Arguments:
% vertAngle: 3*1向量，惯组坐标系各轴与水平面的夹角，轴在水平面上方为正，单位rad

[ fb ] = sensorout2in_linear(pulseRate, aSF0, aB, adSFMA);
vertAngle = asin(fb / norm(fb));
end