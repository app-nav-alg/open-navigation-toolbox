classdef (Enumeration) CoarseAlignAlgo < int32
    enumeration
        INERT(1)
        RV2(2)
    end
end