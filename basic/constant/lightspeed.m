function [c] = lightspeed() %#codegen
%LIGHTSPEED 真空中的光速常数
%
% Output Arguments
% # c: 标量，真空中的光速，单位m/s
%
% References:
% # 《应用导航算法工程基础》“附录A 常数”

c = 299792458;
end