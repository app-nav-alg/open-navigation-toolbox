function [mu] = geogravconst(standard) %#codegen
%GEOGRAVCONST 地心引力常数
%
% Input Arguments
% # standard: 字符串，可以为GPS、BeiDou、WGS84或CGCS2000，默认为WGS84
%
% Output Arguments
% # G: 标量，地心引力常数，单位m^3/(s^2)
%
% References:
% # 《应用导航算法工程基础》“附录A 常数”

if nargin < 1
    standard = 'WGS84';
end

switch standard
    case {'GPS'}
        mu = 3.986005e14;
    case {'WGS84', 'CGCS2000', 'BeiDou'}
        mu = 3.986004418e14;
    otherwise
        error('geocentricgravconst:unknownstd', 'Standard is not GPS, BeiDou, WGS84 or CGCS2000');
end
end