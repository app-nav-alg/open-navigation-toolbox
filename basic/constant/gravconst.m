function [G] = gravconst() %#codegen
%GRAVCONST 万有引力常数，国际科技数据委员会（CODATA）基础物理常数推荐值2014版
%
% Output Arguments
% # G: 标量，万有引力常数，单位m^3/(kg*s^2)
%
% References:
% #《应用导航算法工程基础》“附录A 常数”

G = 6.67408e-11;
end