function [ omegaIEN ] = earthrate( CNE, omegaIE ) %#codegen
%EARTHRATE 计算导航系N下的地球自转角速度
%
% Tips:
% # ECEF系E取GwEN
%
% Input Arguments:
% # CNE: 3*3矩阵，位置矩阵
% # omegaIE: 标量，地球自转角速率，单位rad/s
%
% Output Arguments:
% # omegaIEN: 3*1向量，导航系N下的地球自转角速度，单位rad/s
% # 《应用导航算法工程基础》“导航系下地球自转角速度及位移角速度误差”

omegaIEN = CNE' * [0, 0, omegaIE]';
end