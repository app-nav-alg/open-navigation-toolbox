function [ RM, RN ] = earthradiiofcurv_qyy( sinL, RE, f ) %#codegen
%EARTHRADIIOFCURV_QYY 按照秦永元老师课本计算在指定纬度处的地球子午圈和卯酉圈曲率半径
%
% Input Arguments:
% # sinL: 纬度的正弦，单位rad
% # RE: 标量，地球长半轴，单位m
% # f: 标量，地球扁率，无单位
%
% Output Arguments:
% # RM: 尺寸与sinL相同的矩阵，子午圈曲率半径，单位m
% # RN: 尺寸与sinL相同的矩阵，卯酉圈曲率半径，单位m
%
% References:
% # 秦永元. 惯性导航[M]. 科学出版社. 2006

sinL2 = sinL .* sinL;
RN = RE ./ sqrt(1+sinL2 * ((1-f)^2 -1)); % REF1式7.2.6
RM = (1-f)^2 * RN .^3 /RE^2; % REF1式7.2.5
end