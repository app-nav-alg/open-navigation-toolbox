function [ RMeridian, RNormal ] = earthradiiofcurv( L, RE, eSq ) %#codegen
%EARTHRADIIOFCURV 计算在指定纬度处的地球子午圈和卯酉圈曲率半径
%
% Input Arguments:
% # L: 矩阵，纬度，单位rad
% # RE: 标量，地球长半轴，单位m
% # eSq: 标量，地球偏心率的平方，无单位
%
% Output Arguments:
% # RMeridian: 尺寸与L相同的矩阵，子午圈曲率半径，单位m
% # RNormal: 尺寸与L相同的矩阵，卯酉圈曲率半径，单位m
%
% References:
% #《应用导航算法工程基础》“主曲率半径”

k = 1 - eSq * (sin(L)).^2;
RMeridian = RE * (1-eSq) ./ k.^(3/2);
RNormal = RE ./ k.^(1/2);
end