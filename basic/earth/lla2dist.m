function [dist] = lla2dist(latLonAltBgn, latLonAltEnd, f, RE) %#codegen
%LLA2DIST 由两点的经纬高求两点的直线距离
%
% Input Arguments:
% # latLonAlt: 3*1向量，分别为纬度、经度、高度，单位为rad、rad、m
% # f: 标量，地球模型椭球体的扁率，无单位
% # RE: 标量，地球模型椭球体的半长轴，单位m
%
% Output Arguments:
% # dist: 标量，两点直线距离，单位为m

posBgn_E = lla2ecef_cg(latLonAltBgn', f, RE);
posEnd_E = lla2ecef_cg(latLonAltEnd', f, RE);
P_E = (posEnd_E - posBgn_E)';
dist = norm(P_E);
end