function [dL, dlambda, dalpha] = dposvec2dllwa(drENTildex, drENTildey, dthetaNz, L, alpha, CNE, h, RE, f)
%DPOSVEC2DLLWA 将地球系下定义的位置矢量误差转换为纬度误差、经度误差、游动方位角误差
%
% Input Arguments:
% # drENTildex: m*1向量，地球系下定义的位置矢量误差的x分量，单位m
% # drENTildey: m*1向量，地球系下定义的位置矢量误差的y分量，单位m
% # dthetaNz: m*1向量，位置误差角的z分量，单位rad
% # L: m*1向量，纬度，单位rad
% # alpha: m*1向量，游动方位角，单位rad
% # CNE: 3*3*m矩阵，导航位置矩阵
% # h: m*1向量，高度，单位m
% # RE: 标量，地球模型椭球体的半长轴，单位m
% # f: 标量，地球模型椭球体的扁率，无单位
%
% Output Arguments:
% # dL: m*1向量，纬度误差，单位rad
% # dlambda: m*1向量，经度误差，单位rad
% # dalpha: m*1向量，游动方位角误差，单位rad
%
% References:
% #《应用导航算法工程基础》“纬度、经度、游移方位角误差”

d23 = squeeze(CNE(2, 3, :));
R = (1 - (d23.*d23) * f) * RE + h; % REF1式9.44
cosAlpha = cos(alpha);
sinAlpha = sin(alpha);
dL = 1 ./ R .* (sinAlpha.*drENTildex + cosAlpha.*drENTildey); % REF1式9.57
dlambda = 1 ./ R .* (sec(L).*cosAlpha.*drENTildex - sec(L).*sinAlpha.*drENTildey);
dalpha = 1 ./ R .* (tan(L).*cosAlpha.*drENTildex - tan(L).*sinAlpha.*drENTildey + R.*dthetaNz);
end
