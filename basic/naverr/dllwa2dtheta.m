function [dthetaN] = dllwa2dtheta(dL, dlambda, dalpha, L, alpha) %#codegen
%DLLWA2DTHETA 将纬度误差、经度误差、游动方位角误差转换为位置误差角
%
% Input Arguments:
% # dL: m*1向量，纬度误差，单位rad
% # dlambda: m*1向量，经度误差，单位rad
% # dalpha: m*1向量，游动方位角误差，单位rad
% # L: m*1向量，纬度，单位rad
% # alpha: m*1向量，游动方位角，单位rad
%
% Output Arguments:
% # dthetaN: m*3矩阵，位置误差角，单位rad
%
% References:
% #《应用导航算法工程基础》“纬度、经度、游移方位角误差”

m = size(dL, 1);
dthetaN = coder.nullcopy(NaN(m, 3));
cosAlpha = cos(alpha);
sinAlpha = sin(alpha);
dthetaN(:, 1) = -cosAlpha.*dL + cos(L).*sinAlpha.*dlambda; % REF1式9.54
dthetaN(:, 2) = sinAlpha.*dL + cos(L).*cosAlpha.*dlambda;
dthetaN(:, 3) = -sin(L).*dlambda + dalpha;
end