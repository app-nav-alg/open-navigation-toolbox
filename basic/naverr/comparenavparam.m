function [ dp ] = comparenavparam( cmp, ref, RE, hFig, cmpName, refName, figName, orientSpec, phiSpec, plotInOneFig )
%COMPARENAVPARAM 将多组导航参数（位置、速度、方向余弦矩阵）数据与参考导航参数数据比较
%
% Tips:
% # 本函数简单封装了comparepos、comparevel、compareatt_dcm函数的功能，本函数帮助中未说明的可参考上述函数的帮助
%
% Input Arguments:
% # cmp: 长度为n的元胞数组，比较值，每个元素为结构体，包含pos、vG、CBG、t等域
% # ref: 结构体，参考值，包含pos、vG、CBG、t等域
% # hFig: 长度为3的元胞数组，分别为位置、速度、姿态比较的图形句柄
% # figName: 长度为3的元胞数组，各元素分别为字符串，分别为位置、速度、姿态标校绘图的名称
%
% Output Arguments:
% # dp: 结构体，包含dPos、dvG、phi、dOrth、dNorm、t等域，每个域为长度为n的元胞数组

if nargin < 10
    plotInOneFig = true;
end
if nargin < 9
    phiSpec = [];
end
if nargin < 8
    orientSpec = {'', '', ''};
end
if nargin < 7
    figName = {'位置比较', '速度比较', '姿态比较'};
end
if nargin < 6
    refName = [];
end
if nargin < 5
    cmpName = [];
end
if nargin<4 || isempty(hFig)
    hFig = {[], [], []};
end

% NOTE: 本函数使用线性索引
posCmp = cell(size(cmp));
vGCmp = cell(size(cmp));
CBGCmp = cell(size(cmp));
tCmp = cell(size(cmp));
for i=1:length(cmp)
    posCmp{i} = cmp{i}.pos;
    vGCmp{i} = cmp{i}.vG;
    CBGCmp{i} = cmp{i}.CBG;
    tCmp{i} = cmp{i}.t;
end
[ dp.dPos, dp.t ] = comparepos(posCmp, tCmp, ref.pos, ref.t, 'RE', RE, 'hFig', hFig{1}, 'cmpName', cmpName, 'refName', refName, 'figName', figName{1}, 'plotInOneFig', plotInOneFig);
[ dp.dvG] = comparevector(vGCmp, tCmp, ref.vG, ref.t, 'hFig', hFig{2}, 'cmpName', cmpName, 'refName', refName, 'figName', figName{2}, 'orientSpec', orientSpec, 'plotInOneFig', plotInOneFig);
[ dp.phi, dp.dOrth, dp.dNorm ] = compareatt_dcm(CBGCmp, tCmp, ref.CBG, ref.t, hFig{3}, cmpName, refName, figName{3}, phiSpec, plotInOneFig);
end