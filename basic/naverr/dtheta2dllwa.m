function [dL, dlambda, dalpha] = dtheta2dllwa(dthetaN, alpha, L)
%DTHETA2DLLWA 将位置误差角转换为纬度误差、经度误差、游动方位角误差
%
% Input Arguments:
% # dthetaN: m*3矩阵，位置误差角，单位rad
% # alpha: m*1向量，游动方位角，单位rad
% # L: m*1向量，纬度，单位rad
%
% Output Arguments:
% # dL: m*1向量，纬度误差，单位rad
% # dlambda: m*1向量，经度误差，单位rad
% # dalpha: m*1向量，游动方位角误差，单位rad
%
% References:
% #《应用导航算法工程基础》“纬度、经度、游移方位角误差”

cosAlpha = cos(alpha);
sinAlpha = sin(alpha);
dL = -cosAlpha.*dthetaN(:, 1) + sinAlpha.*dthetaN(:, 2); % REF1式9.55
dlambda = sec(L).*sinAlpha.*dthetaN(:, 1) + sec(L).*cosAlpha.*dthetaN(:, 2);
dalpha = tan(L).*sinAlpha.*dthetaN(:, 1) + tan(L).*cosAlpha.*dthetaN(:, 1) + dthetaN(:, 3);
end