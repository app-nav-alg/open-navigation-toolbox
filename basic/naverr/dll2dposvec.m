function [drENTildex, drENTildey] = dll2dposvec(dL, dlambda, L, alpha, CNE, h, RE, f)
%DLL2DPOSVEC 将纬度误差、经度误差转换为地球系下定义的位置矢量误差
%
% Input Arguments:
% # dL: m*1向量，纬度误差，单位rad
% # dlambda: m*1向量，经度误差，单位rad
% # L: m*1向量，纬度，单位rad
% # alpha: m*1向量，游动方位角，单位rad
% # CNE: 3*3*m矩阵，导航位置矩阵
% # h: m*1向量，高度，单位m
% # RE: 标量，地球模型椭球体的半长轴，单位m
% # f: 标量，地球模型椭球体的扁率，无单位
%
% Output Arguments:
% drENTildex: m*1向量，地球系下定义的位置矢量误差的x分量，单位m
% drENTildey: m*1向量，地球系下定义的位置矢量误差的y分量，单位m
%
% References:
% #《应用导航算法工程基础》“纬度、经度、游移方位角误差”

cosAlpha = cos(alpha);
sinAlpha = sin(alpha);
d23 = squeeze(CNE(2, 3, :));
R = (1 - (d23.*d23) * f) * RE + h; % REF1式9.44
drENTildex = R .* (sinAlpha.*dL + cos(L).*cosAlpha.*dlambda); % REF1式9.56
drENTildey = R .* (cosAlpha.*dL - cos(L).*sinAlpha.*dlambda);
end