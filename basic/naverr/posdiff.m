function [ dLatLonAlt ] = posdiff( latLonAltCmp, latLonAltRef, RE )
%POSDIFF 计算以北向距离、东向距离、高度表示的位置的差值
%
% Input Arguments:
% latLonAltCmp: 列数为3的二维矩阵，位置比较值，各列分别为纬度、经度、高度，单位分别为rad、rad、m
% latLonAltRef: 列数为3的二维矩阵，位置参考值，各列意义与latLonAltCmp相同
% RE: 标量，地球长半轴，单位m
%
% Output Arguments:
% dLatLonAlt: 尺寸与latLonAltRef相同的矩阵，各列分别为北向、东向、高度位置差值（比较值减去参考值），单位为m

dLatLonAlt = NaN(size(latLonAltRef));
dLatLonAlt(:, 1) = (latLonAltCmp(:, 1)-latLonAltRef(:, 1)) * RE;
dLatLonAlt(:, 2) = wrap(latLonAltCmp(:, 2)-latLonAltRef(:, 2), -pi, pi) * RE .* cos(latLonAltRef(:, 1));
dLatLonAlt(:, 3) = latLonAltCmp(:, 3) - latLonAltRef(:, 3);
end