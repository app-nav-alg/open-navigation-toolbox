function [structPar] = navparerrarr2struct(arrPar, indexMap, setNonExistParToNaN)
%NAVPARERRARR2STRUCT 将以二维数组形式保存的导航参数误差转换为结构体形式保存的参数误差
%
% Input Arguments:
% arrPar: 二维数组形式保存的参数，每行为同一时刻的各项参数，每列为同一参数的各时刻值
% indexMap: 1*15向量，绝对值依次为δθx、δθy、dh、速度误差（X、Y、Z，下同）、姿态误差、δθz、纬度误差、经度误差、直角系位置误差在arrPar中的列数，NaN表示不存在，负数表示取负值
% setNonExistParToNaN: 逻辑标量，true表示存在的参数，类型域中不存在的参数项设置为NaN，否则设置为0，默认为false
%
% Output Arguments:
% structPar: 仅含有indexMap中包含的参数类型域，时间在最后一维上

% NOTE: 本函数使用线性索引
if nargin < 3
    setNonExistParToNaN = false;
end

if (length(indexMap) ~= 15)
    error('Length of indexMap should be 15.');
end
indexMapStruct.dtheta = indexMap([1 2 10])';
indexMapStruct.dh = indexMap(3);
indexMapStruct.dVel = indexMap(4:6)';
indexMapStruct.dAtt = indexMap(7:9)';
indexMapStruct.dL = indexMap(11);
indexMapStruct.dlambda = indexMap(12);
indexMapStruct.dR = indexMap(13:15)';
if setNonExistParToNaN
    nonExistEleVal.dtheta = NaN;
    nonExistEleVal.dh = NaN;
    nonExistEleVal.dVel = NaN;
    nonExistEleVal.dAtt = NaN;
    nonExistEleVal.dL = NaN;
    nonExistEleVal.dlambda = NaN;
    nonExistEleVal.dR = NaN;
else
    nonExistEleVal.dtheta = ones(3,1);
    nonExistEleVal.dh = 0;
    nonExistEleVal.dVel = ones(3,1);
    nonExistEleVal.dAtt = ones(3,1);
    nonExistEleVal.dL = 0;
    nonExistEleVal.dlambda = 0;
    nonExistEleVal.dR = ones(3,1);
end
[structPar] = array2struct(arrPar, indexMapStruct, nonExistEleVal);
end