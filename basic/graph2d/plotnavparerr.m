function [] = plotnavparerr(t, par, errIsRelat, parName, fieldPlotMask, tSects, figName, hFig, m, n, p)
%PLOTNAVPARERR 绘制结构体形式的导航参数误差曲线/导航参数相对误差曲线
%
% Tips:
% # 每种类型的参数占用一个subplot
%
% Input Arguments:
% # par: 结构体或结构体向量形式的导航参数误差（可以但不必须包含dtheta、dh、dVel、dAtt、dL、dlambda、dR等域），参数组数与t长度相等，时间在最后一维上，单位为相应的国际单位制单位
% # errIsRelat: 逻辑标量，true代表绘制结构体形式的导航参数相对误差曲线，false代表绘制结构体形式的导航参数误差曲线，默认为false
% # parName: 元胞向量，长度与par相同，各元素为字符串，对应各组参数的名称，默认为空
% # 其它参数: 参见plotstructpar函数相关帮助

if nargin < 11
    p = 1;
end
if nargin < 10
    n = 0;
end
if nargin < 9
    m = 0;
end
if nargin < 8
    hFig = [];
end
if nargin < 7
    figName = [];
end
if nargin < 6
    tSects = [];
end
if nargin < 5
    fieldPlotMask = [];
end
if (nargin<4) || isempty(parName)
    parName = repmat({''}, size(par));
end
if (nargin<3) || isempty(errIsRelat)
    errIsRelat = false;
end

fieldTitles = struct('dtheta', '角位置误差', 'dVel', '速度误差', 'dAtt', '姿态误差', 'dh', '高度误差', 'dL', '经度误差', 'dlambda', '纬度误差', 'dR', '位置误差');
if errIsRelat
    fieldUnits = struct('dtheta', '无单位', 'dVel', '无单位', 'dAtt', '无单位', 'dh', '无单位', 'dL', '无单位', 'dlambda', '无单位', 'dR', '无单位');
    parUnitConvertCoef = struct('dtheta', 1, 'dVel', 1, 'dAtt', 1, 'dh', 1, 'dL', 1, 'dlambda', 1, 'dR', 1);
else
    fieldUnits = struct('dtheta', '″', 'dVel', 'm/s', 'dAtt', '″', 'dh', 'm', 'dL', '″', 'dlambda', '″', 'dR', 'm');
    parUnitConvertCoef = struct('dtheta', 180*3600/pi, 'dVel', 1, 'dAtt', 180*3600/pi, 'dh', 1, 'dL', 180*3600/pi, 'dlambda', 180*3600/pi, 'dR', 1);
end
parLegends = struct;
for i = 1:length(par)
    parLegends(i).dtheta = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dVel = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dAtt = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
    parLegends(i).dh = {''};
    parLegends(i).dL = {''};
    parLegends(i).dlambda = {''};
    parLegends(i).dR = {[parName{i} 'X'] [parName{i} 'Y'] [parName{i} 'Z']}';
end
if isempty(figName)
    figName = '导航参数误差';
end
plotstructpar(t, par, fieldTitles, fieldUnits, parUnitConvertCoef, parLegends, 'fieldPlotMask', fieldPlotMask, 'tSects', tSects, 'figName', figName, 'hFig', hFig, 'm', m, 'n', n, 'p', p);
end