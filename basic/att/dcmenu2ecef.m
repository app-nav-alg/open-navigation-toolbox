function [ CNE ] = dcmenu2ecef( L, lambda ) %#codegen
%DCMENU2ECEF 将纬度和经度转换为表示位置的方向余弦矩阵
%
% Tips:
% # ECEF系E为GwEN，导航系N初始为ENU
%
% Input Arguments:
% # L: 长度为m的列向量，纬度，单位rad
% # lambda: 长度为m的列向量，经度，单位rad
%
% Output Arguments:
% # CNE: 3*3*m矩阵，具体说明参见dcm2latlonwangle函数帮助

CNE = angle2dcm_cg(lambda+pi/2, pi/2-L, zeros(size(L)), 'ZXY')';
end