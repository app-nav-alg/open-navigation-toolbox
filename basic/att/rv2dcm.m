function [CMF] = rv2dcm(thetaFM)
%RV2DCM 将旋转矢量转换为方向余弦矩阵，根据输入类型及大小自动选择精确或5阶近似算法
%
% Input Arguments:
% # thetaFM: m*3矩阵，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments:
% # CMF: 3*3*m矩阵，F坐标系相对于M坐标系的方向余弦矩阵

if isa(thetaFM, 'sym')
    CMF = rv2dcm_exact(thetaFM);
else
    % when theta is smaller than 3 degrees, use taylor expansion to avoid singularity
    CMF = NaN(3, 3, size(thetaFM, 1));
    isSmallAngle = vecnorm(thetaFM') < 0.05;
    CMF(:, :, isSmallAngle) = rv2dcm_5thod(thetaFM(isSmallAngle, :));
    CMF(:, :, ~isSmallAngle) = rv2dcm_exact(thetaFM(~isSmallAngle, :));
end
end