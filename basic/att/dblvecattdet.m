function [CFM] = dblvecattdet(v1F, v2F, v1M, v2M)
%DBLVECATTDET 双矢量定姿
%
% Input Arguments
% # v1F, v2F: 长度为3的向量，向量在F系下的投影
% # v1M, v2M: 长度为3的向量，向量在M系下的投影
%
% Output Arguments
% # CFM: 3*3矩阵，M系相对于F系的方向余弦矩阵
%
% References
% #《应用导航算法工程基础》“双矢量定姿算法及其误差”

CFM = [v1M(:), v2M(:), cross(v1M(:), v2M(:))] / [v1F(:) v2F(:) cross(v1F(:), v2F(:))];
CFM = dcmnormortho_optimized(CFM);
end