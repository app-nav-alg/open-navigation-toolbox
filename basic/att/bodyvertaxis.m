function [vertAxisInd] = bodyvertaxis(CBL)
%BODYVERTAXIS 由方向余弦矩阵求体坐标系垂向轴（与水平面夹角最大轴）的序号及符号
%
% Tips
% # 参考坐标系L为NED
%
% Input Arguments
% # CBL: 3*3*m矩阵
%
% Output Arguments
% # vertAxisInd: m*1向量，体坐标系垂向轴序号，1、2、3分别表示X、Y、Z轴，朝上为正，朝下为负

m = size(CBL, 3);
vertAngle = bodyaxisvertangle(CBL);
[~, vertAxisInd] = max(abs(vertAngle), [], 2);
vertAxisSign = sign(vertAngle(sub2ind([m 3], 1:m, vertAxisInd')));
vertAxisInd = vertAxisInd .* vertAxisSign;
end