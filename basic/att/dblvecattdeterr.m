function [phiFFHatF] = dblvecattdeterr(v1F, v2F, dv1MFBar, dv2MFBar) %codegen
%DBLVECATTDETERR 计算双矢量定姿误差
%
% Input Arguments
% # v1F, v2F: 3*1向量，向量在F系下的投影
% # dv1MFBar, dv2MFBar: 3*1向量，向量在M系下观测的误差在F系下的投影
%
% Output Arguments
% # phiFFHatF: 3*1向量，双矢量定姿误差，计算F系相对于F系的旋转矢量在F系下的投影，单位rad
%
% References
% #《应用导航算法工程基础》“双矢量定姿算法及其误差”

if(isa(v1F, 'sym') || isa(v2F, 'sym') || isa(dv1MFBar, 'sym') || isa(dv2MFBar, 'sym'))
    isSymbolic = true;
else
    isSymbolic = false;
end
CMFdMMT = [dv1MFBar, dv2MFBar, cross(dv1MFBar, v2F)+cross(v1F, dv2MFBar)];
if isSymbolic
    CMFdMMT = simplify(CMFdMMT);
end
MF = inv([v1F v2F cross(v1F, v2F)])';
if isSymbolic
    MF = simplify(MF);
end
tmp = CMFdMMT * MF';
if isSymbolic
    tmp = simplify(tmp);
end
tmp = (tmp - tmp') / 2;
phiFFHatF = [-tmp(2, 3) tmp(1, 3) -tmp(1, 2)]';
if isSymbolic
    phiFFHatF = expand(simplify(phiFFHatF));
end
end