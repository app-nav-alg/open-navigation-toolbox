function [ CMF ] = rv2dcm_exact( thetaFM ) %#codegen
%RV2DCM_EXACT 将旋转矢量转换为方向余弦矩阵，精确算法
%
% Input Arguments:
% # thetaFM: m*3矩阵，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments:
% # CMF: 3*3*m矩阵，F坐标系相对于M坐标系的方向余弦矩阵
%
% References
% #《应用导航算法工程基础》“等效旋转矢量转换为方向余弦矩阵”

m = size(thetaFM, 1);
CMF = NaN(3, 3, m, class(thetaFM));

for i=1:m
    thetaFMNorm = norm(thetaFM(i, :));
    if thetaFMNorm ~= 0
        CMF(:, :, i) = eye(3) + sin(thetaFMNorm)/thetaFMNorm*skewsymmat(thetaFM(i, :)) + (1-cos(thetaFMNorm))/(thetaFMNorm^2)*(skewsymmat(thetaFM(i, :))^2);
    else
        CMF(:, :, i) = eye(3);
    end
end
if isa(CMF, 'sym')
    CMF = simplify(CMF);
end
end