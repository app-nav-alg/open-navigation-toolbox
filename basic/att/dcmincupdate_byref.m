function [C] = dcmincupdate_byref(C, DELTATHETA) %#codegen
%DCMINCUPDATE_BYREF 采用4阶定时增量算法更新DCM矩阵
%
% Input Arguments:
% # C: 3*3矩阵，输入量为更新前的方向余弦矩阵，输出量为更新后的方向余弦矩阵
% # DELTATHETA: 3*1向量，角增量
%
% References:
% #《应用导航算法工程基础》“定时增量算法”

C4 = 1/2 - norm(DELTATHETA)^2/24;
S4 = 1 - norm(DELTATHETA)^2/6;
DELTAC = eye(3) + S4*skewsymmat(DELTATHETA) + C4*(skewsymmat(DELTATHETA)^2);
C = C * DELTAC;
end