function [ qMF ] = rv2quat_exact( thetaFM ) %#codegen
%RV2QUAT_EXACT 将旋转矢量转换为四元数，精确算法
%
% Input Arguments:
% # thetaFM: 3*1列向量，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments:
% # qMF: 1*4向量，thetaFM对应的特征四元数
%
% References
% # 《应用导航算法工程基础》“等效旋转矢量转换为特征四元数”

thetaFMNorm = norm(thetaFM);
if thetaFMNorm ~= 0
    qMF = [cos(thetaFMNorm/2) sin(thetaFMNorm/2)/(thetaFMNorm)*thetaFM'];
else
    qMF = [1 0 0 0];
end
end