function [ phi, dOrth, dNorm, t ] = compareatt_dcm( DCMCmp, tCmp, DCMRef, tRef, hFig, cmpName, refName, figName, phiSpec, plotInOneFig )
%COMPAREATT_DCM 将多组姿态数据与参考姿态数据比较（利用方向余弦矩阵）

%
% Tips:
% # DCMCmp/tCmp和DCMRef/tRef的行数可以不同，本函数仅对tCmp和tRef中均存在的时刻的姿态数据进行比较
%
% Input Arguments:
% # DCMCmp: 长度为n的元胞数组，姿态比较值，每个元素格式同DCMRef
% # tCmp: 长度为n的元胞数组，姿态比较值对应的时刻，每个元素格式同tRef，单位s
% # DCMRef: 3*3*m的数组，姿态参考值，前两维为各时刻上的方向余弦矩阵（假设为正交矩阵）
% # tRef: 单调递增的向量，其长度为m，姿态参考值对应的时刻，单位s
% # hFig: 外部给出的figure句柄，如为NaN则不绘图，如为空则新建绘图，否则在hFig指定的绘图对象上绘图，这时plotInOneFig选项无效（默认为true），默认为空
% # cmpName: 长度为n的元胞数组，每个元素为字符串，姿态比较值的名称，用于绘图，默认为'比较姿态i'
% # refName: 字符串，姿态参考值的名称，用于绘图，默认为'参考姿态'
% # figName: 字符串，绘图名称，默认为'姿态比较'
% # phiSpec: 1*3的元胞数组，姿态数组各列的标识，默认为φx、φy、φz
% # plotInOneFig: 逻辑标量，true表示各组比较结果绘制在一个图形上，否则各组比较结果分别绘图，默认为true
%
% Output Arguments:
% # phi: 尺寸与DCMCmp相同的元胞数组，每个元素为列数为3的数组，为在比较时刻的姿态差值（比较值相对于参考值），设CCmp=CBA2，CRef=CBA1，则phi为A2相对于A1的旋转矢量，单位为″
% # dOrth: 尺寸与DCMCmp相同的元胞数组，每个元素为列数为3的数组，分别为在比较时刻方向余弦矩阵第1、2行，第1、3行，第2、3行向量实际夹角与90度之差（当参考值为正交矩阵时），单位为″
% # dNorm: 尺寸与DCMCmp相同的元胞数组，每个元素为列数为3的数组，分别为在比较时刻方向余弦矩阵第1行，第2行，第3行向量长度平方与1之差的一半（当参考值为正交矩阵时），单位为ppm
% # t: 尺寸与tCmp相同的元胞数组，每个元素为对应phi元素的比较时刻，单位s
%
% Assumptions and Limitations:
% # 对于差值不大于0.1μs的比较时间点与参考时间点认为是同一时刻
% # 比较算法在姿态差较大时精度较低，仅供参考
% # 图例仅支持前20组（包含参考值）数据

% # NOTE: 本函数使用线性索引
n = length(DCMCmp);
if nargin < 10
    plotInOneFig = true;
end
if (nargin<9) || isempty(phiSpec)
    phiSpec = {'φx', 'φy', 'φz'};
end
if (nargin<8) || isempty(figName)
    figName = '姿态比较';
end
if (nargin<7) || isempty(refName)
    refName = '参考姿态';
end
if (nargin<6) || isempty(cmpName)
    cmpName = cell(size(DCMCmp));
    for i=1:n
        cmpName{i} = ['比较姿态' int2str(i)];
    end
end
if nargin < 5
    hFig = [];
end

rad2Sec = 3600 * 180 / pi;
phi = cell(size(DCMCmp));
dOrth = cell(size(DCMCmp));
dNorm = cell(size(DCMCmp));
t = cell(size(tCmp));
for i=1:n
    [indCmp, indRef, t{i}] = findeqelem(tCmp{i}, tRef, 1e-7);
    [phi{i}, dOrth{i}, dNorm{i}] = dcmdiff(DCMCmp{i}(:, :, indCmp), DCMRef(:, :, indRef));
    phi{i} = phi{i} * rad2Sec;
    dOrth{i} = dOrth{i} * rad2Sec;
    dNorm{i} = dNorm{i} * 1e6;
end

if isempty(hFig) || ~isnan(hFig)
    if ~isempty(hFig)
        plotInOneFig = true;
    end
    colorSpecs = {'b', 'g', 'r', 'c', 'm', 'y'};
    for i=1:n
        if (i==1) || (~plotInOneFig)
            preparefig(figName, hFig);
        end
        
        for j=1:3
            if j == 1
                hMainAxis = subplot(4, 2, [j*2-1 j*2]);
            else
                subplot(4, 2, [j*2-1 j*2]);
            end
            if (i==1) || (~plotInOneFig)
                set(gca, 'NextPlot', 'add');
                xlabel('时间(s)');
                title([phiSpec{j} '姿态差(″)（相对于' refName '）']);
                grid on;
            end
            plot(t{i}, phi{i}(:, j), colorSpecs{mod(i-1, length(colorSpecs))+1}, 'DisplayName', cmpName{i});
            text(t{i}(end, 1), phi{i}(end, j), num2str(phi{i}(end, j), '%.0f'), 'Color', 'k', 'FontSize', 16);
        end
        
        hdOrthAxis = subplot(4, 2, 7);
        if (i==1) || (~plotInOneFig)
            set(gca, 'NextPlot', 'add');
            xlabel('时间(s)');
            title('正交差(″)');
            grid on;
        end
        plot(t{i}, dOrth{i}(:, 1), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'DisplayName', [cmpName{i} '第1、2行']);
        plot(t{i}, dOrth{i}(:, 2), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'LineStyle', '--', 'DisplayName', [cmpName{i} '第1、3行']);
        plot(t{i}, dOrth{i}(:, 3), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'LineStyle', ':', 'DisplayName', [cmpName{i} '第2、3行']);
        
        hdNormAxis = subplot(4, 2, 8);
        if (i==1) || (~plotInOneFig)
            set(gca, 'NextPlot', 'add');
            xlabel('时间(s)');
            title('规范差(ppm)');
            grid on;
        end
        plot(t{i}, dNorm{i}(:, 1), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'DisplayName', [cmpName{i} '第1行']);
        plot(t{i}, dNorm{i}(:, 2), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'LineStyle', '--', 'DisplayName', [cmpName{i} '第2行']);
        plot(t{i}, dNorm{i}(:, 3), 'Color', colorSpecs{mod(i-1, length(colorSpecs))+1}, 'LineStyle', ':', 'DisplayName', [cmpName{i} '第3行']);
        
        if (i==n) || (~plotInOneFig)
            legend(hMainAxis, 'show', 'Location', 'NorthWest');
            legend(hdOrthAxis, 'show', 'Location', 'NorthWest');
            legend(hdNormAxis, 'show', 'Location', 'NorthWest');
        end
    end
end
end