function [ CMF ] = rv2dcm_3rdod( thetaFM ) %#codegen
%RV2DCM_3RDOD 将旋转矢量转换为方向余弦矩阵，3阶近似算法
%
% Input Arguments:
% # thetaFM: m*3矩阵，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments:
% # CMF: 3*3*m矩阵，F坐标系相对于M坐标系的方向余弦矩阵

m = size(thetaFM, 1);
CMF = NaN(3, 3, m);

for i=1:m
    thetaFMNorm = norm(thetaFM(i, :));
    CMF(:, :, i) = eye(3) + (1-thetaFMNorm^2/6)*skewsymmat(thetaFM(i, :)) + (1/2-thetaFMNorm^2/24)*(skewsymmat(thetaFM(i, :))^2);
end
end