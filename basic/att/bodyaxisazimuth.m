function [ azm ] = bodyaxisazimuth( CBL ) %#codegen
%BODYAXISAZIMUTH 由方向余弦矩阵求体坐标系各轴的北向方位角
%
% Tips
% # 参考坐标系L为NED
%
% Input Arguments
% # CBL: 3*3*m矩阵
%
% Output Arguments
% # azm: m*3向量，体坐标系X、Y、Z轴在水平面上的投影与北向的夹角（由北向绕顺时针转动到该投影为正），值域[0, 2π)，单位rad
%
% References
% #《应用导航算法工程基础》“由方向余弦矩阵求体坐标系各轴的北向方位角”

if size(CBL, 3) > 1
    azm = wrap(atan2(squeeze(CBL(2, :, :)), squeeze(CBL(1, :, :))), 0, 2*pi)';
else
    azm = wrap(atan2(CBL(2, :), CBL(1, :)), 0, 2*pi);
end
end