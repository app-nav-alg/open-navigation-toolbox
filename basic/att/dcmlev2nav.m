function [ CBN ] = dcmlev2nav( CBL )
%DCMLEV2NAV 将CBL转换为CBN，采用矢量化方式
%
% Input Arguments
% # CBL: 3*3*m矩阵
%
% Output Arguments
% # CBN: 3*3*m矩阵

CBN = NaN(size(CBL));
CBN(1, :, :) = CBL(2, :, :);
CBN(2, :, :) = CBL(1, :, :);
CBN(3, :, :) = -CBL(3, :, :);
end