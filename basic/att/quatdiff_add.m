function [dqMF] = quatdiff_add(qHatMF, qMF) %#codegen
%QUATDIFF_ADD 计算四元数的差值（加式差值）
%
% Tips
% # 本函数自动判断使误差最小的四元数正负，以避免四元数反向导致误差计算错误
%
% Input Arguments
% # qHatMF: m*4矩阵，比较单位四元数
% # qMF: m*4矩阵，参考单位四元数
%
% Output Arguments
% # dqMF: m*3矩阵，比较值相对于参考值的差值

dqMF = qHatMF - qMF;
dqMFAdd = -qHatMF - qMF;
ind = vecnorm(dqMF, 2, 2) > vecnorm(dqMFAdd, 2, 2);
dqMF(ind, :) = dqMFAdd(ind, :);
end