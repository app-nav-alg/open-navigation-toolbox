function [thetaFM] = quat2rv(qMF) %#codegen
%QUAT2RV 将四元数转换为等效旋转矢量
%
% Input Arguments
% # thetaFM: m*3矩阵，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments
% # qMF: m*4矩阵，thetaFM对应的特征四元数
%
% References
% # 《应用导航算法工程基础》“特征四元数转换为等效旋转矢量”


q = quatwrap(qMF);
thetaFMNormHalf = atan2(rss_cg(q(:, 2:4), 2), q(:, 1));
x = thetaFMNormHalf/pi;
thetaFM = 2./sinc(x) .* q(:, 2:4);
end