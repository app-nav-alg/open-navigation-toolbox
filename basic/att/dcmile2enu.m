function [CE_I0N] = dcmile2enu(t, L, omegaIE, dlambda) %#codegen
%DCMILE2ENU 计算由惯性当地地球系E'I0到东北天导航系ENU的方向余弦矩阵
%
% Tips
% # E'I0的定义为，在0时刻原点为地球中心，X轴在赤道平面内且指向初始对准起始时刻的当地子午线，Z沿地球自转方向，三轴构成右手坐标系，后续时刻三轴方向相对惯性空间保持不动
% # N系定义为ENU
%
% Input Arguments
% # t: 标量，当前时间，相对于t0时刻，默认t0为零，单位s
% # L: 标量，当前时刻纬度，单位rad
% # omegaIE: 标量，地球自转角速率，单位rad/s
% # dlambda: 标量，经度变化值，相对于t0时刻的变化，单位rad，默认为0（当载体位置相对地球不变时）
%
% Output Arguments
% # CE_I0N: 3*3矩阵，初始时刻（零时刻）惯性当地地球系E'I0到东北天导航系ENU的方向余弦矩阵CE_I0N

if nargin < 4
    dlambda = 0;
end

CE_I0N = [-sin(omegaIE*t + dlambda) cos(omegaIE*t + dlambda) 0;
    -sin(L)*cos(omegaIE*t + dlambda) -sin(L)*sin(omegaIE*t + dlambda) cos(L);
    cos(L)*cos(omegaIE*t + dlambda) cos(L)*sin(omegaIE*t + dlambda) sin(L)];
end