function [CFM, qMF, lambdaMax, K] = multvecattdet_davq(vM, vF)
%MULTVECATTDET_DAVQ 多矢量定姿算法-Davenport-q方法
%
% Input Arguments:
% # vM: 3*n矩阵，M系下的一组参考矢量，n表示矢量数，单位无
% # vF: 3*n矩阵，F系下的一组参考矢量，n表示矢量数，单位无
%
% Output Arguments:
% # CFM: 3*3矩阵，姿态矩阵，由F系到M系的转换矩阵，单位无
% # qMF: 4*1向量，姿态矩阵对应的四元数，单位无
% # lambdaMax: 标量，与qMF对应的K特征值，单位无
%
% References:
% # 《应用导航算法工程基础》“多矢量定姿算法及其误差”

K = davqquadmat(vM, vF);
[V, D] = eig(K);
[lambdaMax, idx] = max(diag(D));
qMF = V(:, idx);
qMF = quatwrap(real(qMF'))';
% 四元数对应的姿态矩阵
CFM = quat2dcm_cg(qMF');
end