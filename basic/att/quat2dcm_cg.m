function CFM = quat2dcm_cg( qMF ) %#codegen
%QUAT2DCM_CG quat2dcm函数的代码生成用版本，从Aerospace工具箱复制而来
%
% References
% # 《应用导航算法工程基础》“特征四元数转换为方向余弦矩阵”

qin = quatnormalize( qMF );

CFM = coder.nullcopy(zeros(3,3,size(qin,1)));

CFM(1,1,:) = qin(:,1).^2 + qin(:,2).^2 - qin(:,3).^2 - qin(:,4).^2;
CFM(1,2,:) = 2.*(qin(:,2).*qin(:,3) + qin(:,1).*qin(:,4));
CFM(1,3,:) = 2.*(qin(:,2).*qin(:,4) - qin(:,1).*qin(:,3));
CFM(2,1,:) = 2.*(qin(:,2).*qin(:,3) - qin(:,1).*qin(:,4));
CFM(2,2,:) = qin(:,1).^2 - qin(:,2).^2 + qin(:,3).^2 - qin(:,4).^2;
CFM(2,3,:) = 2.*(qin(:,3).*qin(:,4) + qin(:,1).*qin(:,2));
CFM(3,1,:) = 2.*(qin(:,2).*qin(:,4) + qin(:,1).*qin(:,3));
CFM(3,2,:) = 2.*(qin(:,3).*qin(:,4) - qin(:,1).*qin(:,2));
CFM(3,3,:) = qin(:,1).^2 - qin(:,2).^2 - qin(:,3).^2 + qin(:,4).^2;
end