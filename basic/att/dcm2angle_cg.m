function [r1 r2 r3] = dcm2angle_cg( dcm, varargin ) %#codegen
%  DCM2ANGLE_CG dcm2angle函数的代码生成用版本，从Aerospace工具箱复制而来
%   

coder.extrinsic('message');

narginchk(1, 3);

if any(~isreal(dcm) || ~isnumeric(dcm))
    error(message('aero:dcm2angle:isNotReal'));
end

if ((size(dcm,1) ~= 3) || (size(dcm,2) ~= 3))
    error(message('aero:dcm2angle:wrongDimension'));
end

lim = 'default';

if nargin == 1
    type = 'zyx';
else
    if ischar( varargin{1} )
        type = varargin{1};
    else
        error(message('aero:dcm2angle:notChar'));
    end
    if nargin == 3
        if ischar( varargin{2} )
            lim = varargin{2};
        else
            error(message('aero:dcm2angle:notChar1'));
        end
    end
end

switch lower( type )
    case 'zyx'
        %     [          cy*cz,          cy*sz,            -sy]
        %     [ sy*sx*cz-sz*cx, sy*sx*sz+cz*cx,          cy*sx]
        %     [ sy*cx*cz+sz*sx, sy*cx*sz-cz*sx,          cy*cx]

        [r1 r2 r3] = threeaxisrot( dcm(1,2,:), dcm(1,1,:), -dcm(1,3,:), ...
                                   dcm(2,3,:), dcm(3,3,:), ...
                                  -dcm(2,1,:), dcm(2,2,:), lim);

    case 'zyz'
        %     [  cz2*cy*cz-sz2*sz,  cz2*cy*sz+sz2*cz,           -cz2*sy]
        %     [ -sz2*cy*cz-cz2*sz, -sz2*cy*sz+cz2*cz,            sz2*sy]
        %     [             sy*cz,             sy*sz,                cy]
        
        [r1 r2 r3] = twoaxisrot( dcm(3,2,:), dcm(3,1,:), dcm(3,3,:), ...
                                 dcm(2,3,:), -dcm(1,3,:), ...
                                -dcm(2,1,:), dcm(2,2,:), lim);
                
    case 'zxy'
        %     [ cy*cz-sy*sx*sz, cy*sz+sy*sx*cz,         -sy*cx]
        %     [         -sz*cx,          cz*cx,             sx]
        %     [ sy*cz+cy*sx*sz, sy*sz-cy*sx*cz,          cy*cx]

        [r1 r2 r3] = threeaxisrot( -dcm(2,1,:), dcm(2,2,:), dcm(2,3,:), ...
                                   -dcm(1,3,:), dcm(3,3,:), ...
                                    dcm(1,2,:), dcm(1,1,:), lim);

    case 'zxz'
        %     [  cz2*cz-sz2*cx*sz,  cz2*sz+sz2*cx*cz,            sz2*sx]
        %     [ -sz2*cz-cz2*cx*sz, -sz2*sz+cz2*cx*cz,            cz2*sx]
        %     [             sz*sx,            -cz*sx,                cx]

        [r1 r2 r3] = twoaxisrot( dcm(3,1,:), -dcm(3,2,:), dcm(3,3,:), ...
                                 dcm(1,3,:), dcm(2,3,:), ...
                                 dcm(1,2,:), dcm(1,1,:), lim);

    case 'yxz'
        %     [  cy*cz+sy*sx*sz,           sz*cx, -sy*cz+cy*sx*sz]
        %     [ -cy*sz+sy*sx*cz,           cz*cx,  sy*sz+cy*sx*cz]
        %     [           sy*cx,             -sx,           cy*cx]

        [r1 r2 r3] = threeaxisrot( dcm(3,1,:), dcm(3,3,:), -dcm(3,2,:), ...
                                   dcm(1,2,:), dcm(2,2,:), ...
                                  -dcm(1,3,:), dcm(1,1,:), lim);
       
    case 'yxy'
        %     [  cy2*cy-sy2*cx*sy,            sy2*sx, -cy2*sy-sy2*cx*cy]
        %     [             sy*sx,                cx,             cy*sx]
        %     [  sy2*cy+cy2*cx*sy,           -cy2*sx, -sy2*sy+cy2*cx*cy]
     
        [r1 r2 r3] = twoaxisrot( dcm(2,1,:), dcm(2,3,:), dcm(2,2,:), ...
                                 dcm(1,2,:), -dcm(3,2,:), ...
                                -dcm(1,3,:), dcm(1,1,:), lim);
      
    case 'yzx'
        %     [           cy*cz,              sz,          -sy*cz]
        %     [ -sz*cx*cy+sy*sx,           cz*cx,  sy*cx*sz+cy*sx]
        %     [  cy*sx*sz+sy*cx,          -cz*sx, -sy*sx*sz+cy*cx]
        
        [r1 r2 r3] = threeaxisrot( -dcm(1,3,:), dcm(1,1,:), dcm(1,2,:), ...
                                   -dcm(3,2,:), dcm(2,2,:), ...
                                    dcm(3,1,:), dcm(3,3,:), lim);
        
    case 'yzy'
        %     [  cy2*cz*cy-sy2*sy,            cy2*sz, -cy2*cz*sy-sy2*cy]
        %     [            -cy*sz,                cz,             sy*sz]
        %     [  sy2*cz*cy+cy2*sy,            sy2*sz, -sy2*cz*sy+cy2*cy]

        [r1 r2 r3] = twoaxisrot( dcm(2,3,:), -dcm(2,1,:), dcm(2,2,:), ...
                                 dcm(3,2,:), dcm(1,2,:), ...
                                 dcm(3,1,:), dcm(3,3,:), lim);

    case 'xyz'
        %     [          cy*cz, sz*cx+sy*sx*cz, sz*sx-sy*cx*cz]
        %     [         -cy*sz, cz*cx-sy*sx*sz, cz*sx+sy*cx*sz]
        %     [             sy,         -cy*sx,          cy*cx]

        [r1 r2 r3] = threeaxisrot( -dcm(3,2,:), dcm(3,3,:), dcm(3,1,:), ...
                                   -dcm(2,1,:), dcm(1,1,:), ...
                                    dcm(2,3,:), dcm(2,2,:), lim);
        
    case 'xyx'
        %     [                cy,             sy*sx,            -sy*cx]
        %     [            sx2*sy,  cx2*cx-sx2*cy*sx,  cx2*sx+sx2*cy*cx]
        %     [            cx2*sy, -sx2*cx-cx2*cy*sx, -sx2*sx+cx2*cy*cx]

        [r1 r2 r3] = twoaxisrot( dcm(1,2,:), -dcm(1,3,:), dcm(1,1,:), ...
                                 dcm(2,1,:), dcm(3,1,:), ...
                                 dcm(2,3,:), dcm(2,2,:), lim);
        
    case 'xzy'
        %     [          cy*cz, sz*cx*cy+sy*sx, cy*sx*sz-sy*cx]
        %     [            -sz,          cz*cx,          cz*sx]
        %     [          sy*cz, sy*cx*sz-cy*sx, sy*sx*sz+cy*cx]

        [r1 r2 r3] = threeaxisrot( dcm(2,3,:), dcm(2,2,:), -dcm(2,1,:), ...
                                   dcm(3,1,:), dcm(1,1,:), ...
                                  -dcm(3,2,:), dcm(3,3,:), lim);
        
    case 'xzx'
        %     [                cz,             sz*cx,             sz*sx]
        %     [           -cx2*sz,  cx2*cz*cx-sx2*sx,  cx2*cz*sx+sx2*cx]
        %     [            sx2*sz, -sx2*cz*cx-cx2*sx, -sx2*cz*sx+cx2*cx]

        [r1 r2 r3] = twoaxisrot( dcm(1,3,:), dcm(1,2,:), dcm(1,1,:), ...
                                 dcm(3,1,:), -dcm(2,1,:), ...
                                -dcm(3,2,:), dcm(3,3,:), lim);

    otherwise
        error(message('aero:dcm2angle:unknownRotation', type));
end

r1 = r1(:);
r2 = r2(:);
r3 = r3(:);
end

function [r1 r2 r3] = threeaxisrot(r11, r12, r21, r31, r32, r11a, r12a, lim)
% find angles for rotations about X, Y, and Z axes
r1 = atan2( r11, r12 );
r2 = asin( r21 );
r3 = atan2( r31, r32 );
if strcmpi( lim, 'zeror3')
    for i = find(abs( r21 ) >= 1.0)
        r1(i) = atan2( r11a(i), r12a(i) );
        r2(i) = asin( r21(i) );
        r3(i) = 0;
    end
end
end

function [r1 r2 r3] = twoaxisrot(r11, r12, r21, r31, r32, r11a, r12a, lim)
r1 = atan2( r11, r12 );
r2 = acos( r21 );
r3 = atan2( r31, r32 );
if strcmpi( lim, 'zeror3')
    for i = find(abs( r21 ) >= 1.0)
        r1(i) = atan2( r11a(i), r12a(i) );
        r2(i) = acos( r21(i) );
        r3(i) = 0;
    end
end
end
