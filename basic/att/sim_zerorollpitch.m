% 仿真zerorollpitch函数

for i=1:360
    CBL = angle2dcm(i/180*pi, (rand*30-15)/180*pi, (rand*30-15)/180*pi);
    CBLHorz = zerorollpitch(CBL);
    [r1(i, 1) r1(i, 2) r1(i, 3)] = dcm2angle(CBL);
    [r2(i, 1) r2(i, 2) r2(i, 3)] = dcm2angle(CBLHorz);
end
rDiff = wrap((r1-r2)/pi*180, -180, 180);
max(abs(rDiff(:, 1)), [], 1)
max(abs(r2(:, [2 3])), [], 1)