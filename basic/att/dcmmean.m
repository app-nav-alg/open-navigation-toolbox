function [CFM_mean] = dcmmean(CFM)
%DCMMEAN 方向余弦矩阵的均值
%
% Input Arguments
% # CFM: 3*3*p数组，p个M系相对于F系的方向余弦矩阵
%
% Output Arguments
% # CFM_mean: 3*3矩阵，p个M系相对于F系的方向余弦矩阵的均值

thetaMF = dcm2rv(CFM);
thetaMF_mean = rvmean(thetaMF);
CFM_mean = rv2dcm_exact(thetaMF_mean);