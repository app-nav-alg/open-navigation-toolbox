function [ L, lambda, alpha ] = dcm2latlonwangle( CNE ) %#codegen
%DCM2LATLONWANGLE 将位置矩阵转换为经纬度、游移方位角
%
% Tips
% # ECEF系E为GwEN，导航系N初始为ENU
%
% Input Arguments
% # CNE: 3*3*m矩阵，用3个输出的欧拉角表示为（由E系绕Z轴旋转lambda，绕Y轴旋转-L，绕X轴旋转alpha得到N'系，将N'系的X、Y、Z轴分别替换为Z、X、Y轴得到N系）
% [ - cos(alpha)*sin(lambda) - sin(L)*cos(lambda)*sin(alpha),   sin(alpha)*sin(lambda) - sin(L)*cos(alpha)*cos(lambda), cos(L)*cos(lambda)]
% [   cos(alpha)*cos(lambda) - sin(L)*sin(alpha)*sin(lambda), - cos(lambda)*sin(alpha) - sin(L)*cos(alpha)*sin(lambda), cos(L)*sin(lambda)]
% [                                      cos(L)*sin(alpha),                                      cos(L)*cos(alpha),            sin(L)]
%
% Output Arguments
% # L: 长度为m的列向量，纬度，值域[-pi/2, pi/2]，单位rad
% # lambda: 长度为m的列向量，经度，值域[-pi, pi]，单位rad
% # alpha: 长度为m的列向量，游移方位角，导航系从初始姿态绕天向转动（按右手定则）的角度，理论值域[0, 2*pi)（因数值误差实际值域[0, 2*pi]），单位rad
%
% References
% # 《应用导航算法》“将位置矩阵转换为纬度、经度和游移方位角”

if isa(CNE, 'sym')
    L = reshape(atan2(CNE(3, 3, :), sqrt(CNE(3, 1, :).^2+CNE(3, 2, :).^2)), [], 1); % NOTE: use asin(CNE(3, 3, :)) if consequent expressions contain sin(L)
    lambda = reshape(atan2(CNE(2, 3, :), CNE(1, 3, :)), [], 1);
    alpha = reshape(atan2(CNE(3, 1, :), CNE(3, 2, :)), [], 1);
else
    p = size(CNE, 3);
    L = NaN(p, 1);
    lambda = NaN(p, 1);
    alpha = NaN(p, 1);
    for i=1:p
        L(i, 1) = atan2(CNE(3, 3, i), sqrt(CNE(3, 1, i)^2+CNE(3, 2, i)^2));
        lambda(i, 1) = atan2(CNE(2, 3, i), CNE(1, 3, i));
        alpha(i, 1) = atan2(CNE(3, 1, i), CNE(3, 2, i));
        if alpha(i, 1) < 0
            alpha(i, 1) = alpha(i, 1) + 2*pi; % NOTE: 如果alpha绝对值很小，执行此语句后alpha将等于2π
        end
    end
end
end