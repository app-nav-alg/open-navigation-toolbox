function [ CBLHorz ] = zerodcmrollpitch( CBL ) %#codegen
%ZERODCMROLLPITCH 将任意姿态变换到水平方位角相同的水平姿态上
%
% Tips:
% # 参考坐标系L为NED
%
% Input Arguments:
% # CBL: 3*3方向余弦矩阵
%
% Output Arguments:
% # CBLHorz: 3*3方向余弦矩阵，CBL中B系最接近水平的轴的水平方位角与CBLHorz中相应轴的水平方位角相等

azm = bodyaxisazimuth(CBL);
[~, IMax] = max(abs(CBL(3, :)), [], 2);
[~, IMin] = min(abs(CBL(3, :)), [], 2);
fB = zeros(3, 1);
fB(IMax, 1) = -sign(CBL(3, IMax));
CBLHorz = coarsealign_stat_extazm( fB, azm(1, IMin), IMin );
end