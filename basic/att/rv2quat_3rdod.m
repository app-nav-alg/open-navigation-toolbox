function [ qMF ] = rv2quat_3rdod( thetaFM ) %#codegen
%RV2QUAT_3RDOD 将旋转矢量转换为四元数，3阶近似算法
%
% Input Arguments:
% # thetaFM: m*3矩阵，M坐标系相对于F坐标系的旋转矢量
%
% Output Arguments:
% # qMF: m*4向量，thetaFM对应的特征四元数

m = size(thetaFM, 1);
qMF = NaN(m, 4);

for i=1:m
    thetaFMNorm = norm(thetaFM(i,:));
    qMF(i,:) = [(-thetaFMNorm^2/8+1), (-thetaFMNorm^2/48+1/2)*thetaFM(i,:)'];
end
end