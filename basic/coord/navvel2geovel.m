function [ vEG ] = navvel2geovel( vEN, alpha ) %#codegen
%NAVVEL2GEOVEL 将导航系下的地速分量转换为地理系下的地速分量
%
% Tips:
% # 导航系N初始为ENU，地理系G为ENU
%
% Input Arguments:
% # vEN: m*3矩阵，导航系下的地速，单位m/s
% # alpha: 长度为m的列向量，游移方位角，导航系从初始姿态绕天向转动（按右手定则）的角度，单位rad
%
% Output Arguments:
% # vEG: m*3矩阵，地理系下的地速，单位m/s
%
% Assumptions and Limitations:
% # 游移方位角在极区有奇异
%
% References:
% #《应用导航算法工程基础》“将N系下的速度转换为G系下的速度”

vEG = [vEN(:, 1).*cos(alpha) - vEN(:, 2).*sin(alpha), ...
    vEN(:, 1).*sin(alpha) + vEN(:, 2).*cos(alpha), ...
    vEN(:, 3)];
end