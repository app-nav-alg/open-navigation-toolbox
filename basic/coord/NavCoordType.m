% References:
% # 《应用导航算法工程基础》“导航坐标系N系”
classdef (Enumeration) NavCoordType < int32
    enumeration
        GEO      (1)
        WAN_AZM  (2)
        FREE_AZM (3)
    end
end