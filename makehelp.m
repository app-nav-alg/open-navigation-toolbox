function [] = makehelp()
%MAKEHELP 制作帮助文档

%% 切换至工具箱根目录
tbkRootFolder = getdirandfilenamefrompath(mfilename('fullpath'));
oldFolder = cd(tbkRootFolder);

%% 删除html\autogen目录中原有文件
if isdir([tbkRootFolder filesep 'html' filesep 'autogen'])
    rmdir([tbkRootFolder filesep 'html' filesep 'autogen'], 's');
end

%% 调用M2HTML工具箱（http://www.artefact.tk/software/matlab/m2html/）生成函数参考页
% NOTE: 使用的M2HTML工具箱版本为1.5 01-May-2005，包含142个文件，总大小425169（未修改状态）
% NOTE: 将M2HTML工具箱根目录（m2html）放到本工具箱根目录下，然后添加到路径
% NOTE: 需要将m2html\templates\blue\*.tpl中的“iso-8859-1”替换为“GBK”，否则参考页中文显示乱码
m2html('mfiles', {'inertial' 'radio' 'util'}, 'htmldir', ['html' filesep 'autogen'], ...
    'recursive', 'on', 'source', 'off', 'globalHypertextLinks', 'on', ...
    'todo', 'on', 'template', 'blue', 'ignoredDir', {'private' 'vld'}, 'save', 'on', ... % TODO: save改为off
    'helptocxml', 'on', 'language', 'chinese'); % NOTE: 本行为在m2html帮助中未说明的选项

%% 生成帮助目录
% 读入手工编辑的目录
templDoc = xmlread([tbkRootFolder filesep 'helptoc_noautogen.xml']);
templTocItems = templDoc.getElementsByTagName('tocitem');
templTopTocItem = templTocItems.item(0);

% 读入自动生成的目录（函数参考部分）
fcnDoc = xmlread([tbkRootFolder filesep 'html' filesep 'autogen' filesep 'helptoc.xml']);
fcnTocItems = fcnDoc.getElementsByTagName('tocitem');
fcnTopTocItem = fcnTocItems.item(0);

% 将自动生成的目录合并入手工编辑目录
fcnTopTocItemCopy = templDoc.importNode(fcnTopTocItem, true);
fcnTopTocItemCopy.getFirstChild.setData('Function Reference');
fcnTopTocItemCopy.setAttribute('image', 'HelpIcon.FUNCTION');
templTopTocItem.appendChild(fcnTopTocItemCopy);

% 写目录文件
xmlwrite([tbkRootFolder filesep 'html' filesep 'helptoc.xml'], templDoc);

% 删除自动生成的helptoc.xml
delete([tbkRootFolder filesep 'html' filesep 'autogen' filesep 'helptoc.xml']);

%% 生成文档搜索数据库
try
    builddocsearchdb([tbkRootFolder filesep 'html']); % TODO: 无法搜索中文
catch ME
    warning(ME.identifier, '%s', ME.message);
end

%% 切换至原目录
cd(oldFolder);
end