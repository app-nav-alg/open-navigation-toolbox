classdef (Enumeration)GNSSSysType < int8
    enumeration
        GPS(0)
        GLONASS(1)
        Galileo(2)
        BeiDou(3)
    end
end