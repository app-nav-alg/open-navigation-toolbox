fileID = fopen('E:\session.txt');
% rx = uint8(fread(fileID)); % 二进制文件
rx = uint8(fscanf(fileID, '%x ')); % 以“12 34 AB CD ”格式保存的文本文件

% load('C:\gsof_rx.mat');

totolMsgParams = struct();
cnt = 0;
i = 0;
clear('parsegsof');
while(true)
    if i == 0
        [periodMsgParams, packetCount, msgCount, remainingMsgLen, retCode] = parsegsof( rx, 'SIGMA' );
    else
        [periodMsgParams, packetCount, msgCount, remainingMsgLen, retCode] = parsegsof( [], 'SIGMA' );
    end
    if ~isempty(periodMsgParams)
        cnt = cnt + 1;
        
        totolMsgParams(cnt).time = double(periodMsgParams.TIME.GPSTime);
        totolMsgParams(cnt).posFlags2 = double(periodMsgParams.TIME.posFlags2);
        
        totolMsgParams(cnt).latitude = double(periodMsgParams.LLH.latitude);
        totolMsgParams(cnt).longitude = double(periodMsgParams.LLH.longitude);
        totolMsgParams(cnt).height = double(periodMsgParams.LLH.height);
        
        totolMsgParams(cnt).speed = double(periodMsgParams.Velocity.speed);
        totolMsgParams(cnt).heading = double(periodMsgParams.Velocity.heading);
        totolMsgParams(cnt).vertVel = double(periodMsgParams.Velocity.vertVel);
        
        totolMsgParams(cnt).PDOP = double(periodMsgParams.DOP.PDOP);
        totolMsgParams(cnt).HDOP = double(periodMsgParams.DOP.HDOP);
        totolMsgParams(cnt).VDOP = double(periodMsgParams.DOP.VDOP);
        totolMsgParams(cnt).TDOP = double(periodMsgParams.DOP.TDOP);
        
        totolMsgParams(cnt).GPSTime = double(periodMsgParams.UTC.GPSTime);
        totolMsgParams(cnt).GPSWeekNum = double(periodMsgParams.UTC.GPSWeekNum);
        totolMsgParams(cnt).UTCOffset = double(periodMsgParams.UTC.UTCOffset);
        
        totolMsgParams(cnt).posRMS = double(periodMsgParams.SIGMA.posRMS);
        totolMsgParams(cnt).sigmaEast = double(periodMsgParams.SIGMA.sigmaEast);
        totolMsgParams(cnt).sigmaNorth = double(periodMsgParams.SIGMA.sigmaNorth);
        totolMsgParams(cnt).covarEastNorth = double(periodMsgParams.SIGMA.covarEastNorth);
        totolMsgParams(cnt).sigmaUp = double(periodMsgParams.SIGMA.sigmaUp);
    end
    if remainingMsgLen < 150
        break;
    end
    i = i + 1;
end