function [IMUTime, UTCTime, pos, flag, staNum, HDOP] = readgpsoutfile_GGA( filename )
%READGPSOUTFILE_GGA 从文件中读GPS输出NMEA协议中的GGA数据
%
% Input Arguments:
% filename: 字符串，输出数据文件的文件名
%
% Output Arguments:
% IMUTime: n*1列向量，当前帧对应的惯组时间，单位s
% UTCTime: n*1列向量，当前帧对应的UTC天秒，单位s
% pos: n*3矩阵，位置信息，纬度、经度、高度，单位分别为rad、rad、m
% flag: n*1列向量，GPS状态，0=未定位，1=非差分定位，2=差分定位，6=正在估算
% staNum: n*1列向量，卫星数量，00-12
% HDOP: n*1列向量，水平精度因子，0.5-99.9

delimiter = ',';
formatSpec = '%f%s%f%f%s%f%s%f%f%f%f%s%f%s%s%s%[^\n\r]';
fileID = fopen(filename, 'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
fclose(fileID);
IMUTime = dataArray{1, 1};
UTCTime = dataArray{1, 3};
UTCTime = hms2secs(UTCTime); % 转换为秒
L = dataArray{1, 4};
lambda = dataArray{1, 6};
flag = dataArray{1, 8};
staNum = dataArray{1, 9};
HDOP = dataArray{1, 10};
altitude = dataArray{1, 11}; % 海拔高度
offAltitude = dataArray{1, 13};
tmpL = L./100;
L = (floor(tmpL) + ((tmpL - floor(tmpL))*100)/60)/180*pi;
tmpLambda = lambda./100;
lambda = (floor(tmpLambda) + ((tmpLambda - floor(tmpLambda))*100)/60)/180*pi;
h = altitude + offAltitude;
pos = [L lambda h];
end