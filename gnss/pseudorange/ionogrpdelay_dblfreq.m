function [drIonoThis, drIonoOther] = ionogrpdelay_dblfreq(rhoThis, rhoOther, fThis, fOther)
%IONOGRPDELAY_DBLFREQ 计算双频电离层群延迟（相超前）
%
% Tips
% # 本函数适用于伪码测距及载波相位测距，修正公式均为r=rhoThis-drIonoThis或者r=rhoOther-drIonoOther
% # 当输入的rho参数为伪距率时，本函数可以用于计算伪距率修正量drRateIono
%
% Input Arguments
% # rhoThis: 标量，该频点伪距值，单位m
% # rhoOther: 标量，另一频点伪距值，单位m
% # fThis: 标量，该频点频率值，单位Hz
% # fOther: 标量，另一频点频率值，单位Hz
%
% Output Arguments
% # drIonoThis: 标量，该频点电离层群延迟对应的路径长度，单位m
% # drIonoOther: 标量，另一频点电离层群延迟对应的路径长度，单位m
%
% References:
% # 《应用导航算法工程基础》“双频接收机的电离层延迟改正”

gamma = (fThis/fOther)^2;
drIonoThis = (1/(1-gamma)) * (rhoThis - rhoOther);

if nargout > 1
    gamma = 1 / gamma;
    drIonoOther = (1/(1-gamma)) * (rhoOther - rhoThis);
end
end