function [dfSVOverf] = svclkfreqoffset(tSV, clkErrPar, ephem, F)
%SVCLKFREQOFFSET 计算卫星时钟频率偏差率
%
% Input Arguments
% # tSV: 标量，为消息发送时刻的卫星测距码相位时间，单位s
% # clkErrPar: 结构体，钟差参数，包含以下域：
%     tOC: 标量，参考时间，单位：秒
%     af1: 标量，一阶项，单位：秒/秒
%     af2: 标量，二阶项，单位：秒/秒^2
% # ephem: 结构体，星历，包含以下域：
%     ASqrt: 标量，长半轴的平方根，单位：m^0.5
%     e: 标量，偏心率，值域：0~0.03，单位：无量纲
%     E：标量，信号发射时刻的偏近点角，单位：rad
%     ERate：标量，信号发射时刻卫星偏近点角对时间的导数，单位：rad/s
% # f: 长度为5的向量，对于GPS系统，第1、2、5元素分别为L1、L2、L5频点的频率，对于北斗系统，第1、2、3元素分别为B1、B2、B3频点的频率
%
% Output Arguments
% # dfSV: 标量，卫星时钟频偏，无量纲
%
% References:
% # 《应用导航算法工程基础》“卫星时钟频偏校正”

% 修正卫星时钟频率漂移相对论项
dtRRate = F * ephem.e * ephem.ASqrt * ephem.ERate * cos(ephem.E);

% 卫星测距码相位时间偏移
tDiff = accountforweekcrossover(tSV-clkErrPar.tOC);
dfSVOverf = clkErrPar.af1 + 2*clkErrPar.af2*(tDiff) + dtRRate;
end