function [drTropo] = tropodelay_simp(E)
%TROPODELAY_SIMP 使用简化模型计算对流层延迟
%
% Input Arguments
% # E: 标量，用户接收机到导航卫星之间的高度角，值域：0~π/2，单位：rad
%
% Output Arguments
% # drTropo: 标量，对流层延迟，单位：m
%
% References:
% #《应用导航算法工程基础》“对流层延迟改正”

drTropo = 2.47 / (sin(E) + 0.0121);
end