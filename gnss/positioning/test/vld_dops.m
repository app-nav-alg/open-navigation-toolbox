% dops函数验证脚本

azm = [0 120 240 0]' / 180 * pi;
elev = [5 5 5 90]' / 180 * pi;
[x, y, z] = sph2cart(azm, elev, ones(4, 1));
[GDOP, PDOP, HDOP, VDOP, TDOP] = dops(zeros(3, 1), [x y z]);
assert(truncate(GDOP, 2) == 1.83);
assert(truncate(HDOP, 2) == 1.16);
assert(truncate(VDOP, 2) == 1.26);
assert(truncate(PDOP, 2) == 1.72);
assert(truncate(TDOP, 2) == 0.64);