function [t] = accountforweekcrossover(t)
%ACCOUNTFORWEEKCROSSOVER 处理GNSS时间周翻转
%
% Input Arguments
% # t: 标量，时间，单位s
%
% Output Arguments
% # t: 标量，处理了整周翻转的时间，单位s
%
% References:
% # 《应用导航算法工程基础》“根据星历计算卫星在ECEF坐标系中的位置”

if t > 302400
    t = t - 604800;
elseif t < -302400
    t = t + 604800;
end
end