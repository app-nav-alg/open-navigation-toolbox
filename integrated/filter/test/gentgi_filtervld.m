function [ vebr, omeganbb, tSects ] = gentgi_filtervld()
%GENTGI_FILTERVLD 生成轨迹生成器输入量-静止→匀加速→匀速→匀减速……静止→正转→静止→反转轨迹
%
% Output Arguments:
% vebr, omeganbb, tSects: 参见gentgi_static函数帮助

%% 轨迹参数
dt = 0.1; % 单位：s

transAccTime = 100; % 单位：s
transDecTime = 100; % 单位：s
transVel = 1000; % 单位：m/s
transDisplace = 200000; % 单位：m
transUniVec = [1 0 0; 0 1 0; 0 0 1]';

rotAccTime = 1; % 单位：s
rotDecTime = 1; % 单位：s
rotVec = [20.5*pi 0 0; -20*pi 0 0; 0 20.5*pi 0; 0 -20*pi 0; 0 0 20.5*pi; 0 0 -20*pi]'; % 负号代表逆向转动
rotAngRate = rss(rotVec, 1)/100; % 单位：rad/s

%% 生成并合并各轨迹段
% 首个静止段
[vebrValues, omeganbbValues, t] = geneletgi_static(0, 300, dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(end);
% zB加速->zB匀速->zB减速
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_transalgbdfixaxis_targdisp(t(end), transUniVec(:, 3), transDisplace, transVel, transAccTime, transDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 300, dt);
catSect();
% xB正转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 1), rotAngRate(1), rotAccTime, rotDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 100, dt);
catSect();
% xB反转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 2), rotAngRate(2), rotAccTime, rotDecTime, dt);
catSect();

% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 300, dt);
catSect();
% yB加速->yB匀速->yB减速
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_transalgbdfixaxis_targdisp(t(end), transUniVec(:, 2), transDisplace, transVel, transAccTime, transDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 300, dt);
catSect();
% zB正转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 5), rotAngRate(5), rotAccTime, rotDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 100, dt);
catSect();
% zB反转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 6), rotAngRate(6), rotAccTime, rotDecTime, dt);
catSect();

% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 300, dt);
catSect();
% xB加速->xB匀速->xB减速
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_transalgbdfixaxis_targdisp(t(end), transUniVec(:, 1), transDisplace, transVel, transAccTime, transDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 300, dt);
catSect();
% yB正转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 3), rotAngRate(3), rotAccTime, rotDecTime, dt);
catSect();
% 静止段
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_static(t(end), 100, dt);
catSect();
% yB反转360度
[sectvebrValues, sectomeganbbValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
    rotVec(:, 4), rotAngRate(4), rotAccTime, rotDecTime, dt);
catSect();

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omeganbb.time = t;
omeganbb.signals.values = omeganbbValues;
omeganbb.signals.dimensions = 3;

    function [] = catSect()
        t = vertcat(t, sectt);
        vebrValues = vertcat(vebrValues, sectvebrValues);
        omeganbbValues = vertcat(omeganbbValues, sectomeganbbValues);
        tSects(end+1, 1) = t(end);
    end
end