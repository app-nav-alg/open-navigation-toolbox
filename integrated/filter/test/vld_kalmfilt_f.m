% 验证控制量功能

clc;
clear;
load('mcsim', 'in');
par.g = 9.7934752114984;
cst = load('earthconstants_WGS84.mat', 'RE');
cst.deg2Rad = pi / 180;
cst.sec2Rad = pi / 3600 / 180;
in.curSimParVal(1:36, 1) = in.parRefVal(1:36, 2);
in.curSimParVal(37:44, 1) = in.parRefVal(37:44, 1);

%% 生成轨迹
% 常量
cst_trajGen = load('earthconstants_WGS84.mat', 'omegaIE', 'RE', 'epsilonSq');
% 配置
cfg_trajGen.enableTrajectoryGen = true;
cfg_trajGen.enableSensErr = true; % 在轨迹中引入器件误差
cfg_trajGen.enableSensRandErr = false;
cfg_trajGen.enableNavigation = false;
cfg_trajGen.ignore2OdSensErr = true;
cfg_trajGen.IMUOutFilePath = [];
cfg_trajGen.quantizeIMUOut = false;
cfg_trajGen.IMUOutPrecision = '%.16e';
% 参量
par_trajGen.samplePeriod = 0.01;
par_trajGen.g = par.g; % 重力加速度，m/s^2
par_trajGen.IMU.acc.B = in.curSimParVal(10:12, 1) * par_trajGen.g * 1e-6; % 转换前单位μg，转换后单位m/s^2
par_trajGen.IMU.acc.SF0 = ones(3, 1);
par_trajGen.IMU.acc.dSF = in.curSimParVal(13:15, 1) * 1e-6;
par_trajGen.IMU.acc.MA = vec2offdiag(in.curSimParVal(16:21, 1)) * 1e-6; % 转换前单位μrad，转换后单位rad
par_trajGen.IMU.acc.SO = in.curSimParVal(22:24, 1) * 1e-6 / par_trajGen.g; % 转换前单位μg/g^2，转换后单位1/(m/s^2)
par_trajGen.IMU.gyro.B = in.curSimParVal(25:27, 1) * cst.sec2Rad; % 转换前单位°/h，转换后单位rad/s
par_trajGen.IMU.gyro.SF0 = ones(3, 1);
par_trajGen.IMU.gyro.dSFp = in.curSimParVal(28:30, 1) * 1e-6;
par_trajGen.IMU.gyro.dSFn = par_trajGen.IMU.gyro.dSFp;
par_trajGen.IMU.gyro.MA = vec2offdiag(in.curSimParVal(31:36, 1)) * 1e-6; % 转换前单位μrad，转换后单位rad
par_trajGen.IMU.gyro.GS = zeros(3);
% 输入量
in_trajGen.genFuncName = 'gentgi_navtest_regular';
in_trajGen.genFuncInArg = {};
% 初始条件
iniCon_trajGen.CBG = eye(3); % 初始姿态矩阵
iniCon_trajGen.vG = zeros(3, 1); % 初始速度
iniCon_trajGen.pos = [30 * cst.deg2Rad, 110 * cst.deg2Rad, 0]'; % 纬度、经度、高度
% 生成轨迹
[out_trajGen, ~] = gentrajectory(in_trajGen, cfg_trajGen, par_trajGen, iniCon_trajGen, cst_trajGen);

%% 运行组合导航
% 常量
cst_intNav = load('earthconstants_WGS84.mat', 'RE', 'e', 'mu', 'J2', 'J3', 'omegaIE', 'epsilonSq');
% 配置
cfg_intNav.enableKalmFilt = false;
cfg_intNav.enableKFUpdate = false;
cfg_intNav.extrapAlgo = KalmFiltExtrapAlgo.ITER;
cfg_intNav.updAlgo = KalmFiltUpdAlgo.NORMAL;
cfg_intNav.useDCMInAttCalc = false;
cfg_intNav.dynaMatINSSysBlkForm = 1;
cfg_intNav.navCoordType = NavCoordType.WAN_AZM;
cfg_intNav.sysStateMask = [true(1, 10) false(1, 1)];
cfg_intNav.sensStateMask = [true(1, 27) false(1, 9)];
cfg_intNav.enableNumCondCtrl = false; % 关闭数值条件控制
% 初始条件
iniCon_intNav.pos = out_trajGen.pos(1, :)'; % 无误差
iniCon_intNav.vN = out_trajGen.vG(1, :)'; % 无误差
iniCon_intNav.CBN = out_trajGen.CBG(:, :, 1); % 无误差
iniCon_intNav.kalmFilt.x = [-in.curSimParVal(1)/cst.RE; in.curSimParVal(2)/cst.RE; in.curSimParVal(3); in.curSimParVal(6:-1:4); in.curSimParVal(9:-1:7)*cst.sec2Rad; in.curSimParVal(2)/cst.RE*tan(out_trajGen.pos(1, 1));...
    in.curSimParVal(10:12)*par_trajGen.g*1e-6; in.curSimParVal(13:21)*1e-6; in.curSimParVal(22:24)/par_trajGen.g*1e-6; in.curSimParVal(25:27)*cst.sec2Rad; in.curSimParVal(28:36)*1e-6];
iniCon_intNav.kalmFilt.P = iniCon_intNav.kalmFilt.x*iniCon_intNav.kalmFilt.x';
% 参量
par_intNav.tBgn = out_trajGen.t(1);
par_intNav.tEnd = out_trajGen.t(end);
par_intNav.dT = par_trajGen.samplePeriod;
par_intNav.tUpd = 20; % Aid组合导航的量测更新周期
par_intNav.g = par_trajGen.g;
par_intNav.INSCycPerKalFiltExtrapCyc = int32(10);
par_intNav.GammaMRGammaMT = diag([2/cst.RE, 2/cst.RE, 2, 0.005, 0.005, 0.005, 10*cst.sec2Rad*ones(1, 3), 2/cst.RE].^2);
par_intNav.INSSensRWC = eps(iniCon_intNav.kalmFilt.x(4:9, :));
par_intNav.INSSensStateProcNoisePSDFull = [eps(iniCon_intNav.kalmFilt.x(11:end, :)); zeros(9, 1)]; % par_intNav.INSSensRWC及par_intNav.INSSensStateProcNoisePSDFull取0会导致in_kalmFilt.GPQGPT矩阵所有元素为0，进而导致out_kalmFilt.P对角线元素出现负数，故取eps而不是0
par_intNav.PDiagMin = NaN(nnz([cfg_intNav.sysStateMask cfg_intNav.sensStateMask]), 1);
par_intNav.PDiagMax = NaN(nnz([cfg_intNav.sysStateMask cfg_intNav.sensStateMask]), 1);
% 输入量
m = size(out_trajGen.accOutErrFree, 1);
in_intNav.INS.accOut = out_trajGen.accOutErrFree;
in_intNav.INS.gyroOut = out_trajGen.gyroOutErrFree;
in_intNav.aid.CNE = zeros(3, 3, m);
in_intNav.aid.vN = zeros(m, 3);
in_intNav.aid.vG = out_trajGen.vG;
in_intNav.aid.CBN = zeros(3, 3, m);
in_intNav.aid.h = out_trajGen.pos(:, 3);
in_intNav.aid.alpha = zeros(m, 1);
in_intNav.traj.CNE = zeros(3, 3, m);
in_intNav.traj.pos = out_trajGen.pos;
in_intNav.traj.vN = out_trajGen.vG;
in_intNav.traj.CBN = zeros(3, 3, m);
in_intNav.traj.fB = out_trajGen.fBTrue;
in_intNav.traj.omegaIBB = out_trajGen.omegaIBBTrue;
in_intNav.traj.FCN = zeros(3, 3, m);
in_intNav.traj.gN = zeros(m, 3);
in_intNav.u = -iniCon_intNav.kalmFilt.x;
% 运行导航
clear('intnav_kfvld');
% 纯惯导解算
[out_inertNavIdeal, ~] = intnav_kfvld(in_intNav, cfg_intNav, par_intNav, iniCon_intNav, cst_intNav);
% 组合导航
cfg_intNav.enableKalmFilt = true;
cfg_intNav.enableKFUpdate = true;
iniCon_intNav.kalmFilt.x = zeros(37, 1); % 初始状态为0
iniCon_intNav.pos = out_trajGen.pos(1, :)' + [in.curSimParVal(1, 1)/cst.RE; in.curSimParVal(2, 1)/cst.RE*sec(out_trajGen.pos(1, 1)); in.curSimParVal(3)];
iniCon_intNav.vN = out_trajGen.vG(1, :)' + in.curSimParVal(6:-1:4);
iniCon_intNav.CBN = angle2dcm(in.curSimParVal(7)*cst.sec2Rad, in.curSimParVal(8)*cst.sec2Rad, in.curSimParVal(9)*cst.sec2Rad) * out_trajGen.CBG(:, :, 1);
in_intNav.INS.accOut = out_trajGen.accOut;
in_intNav.INS.gyroOut = out_trajGen.gyroOut;
in_intNav.aid.CNE = out_inertNavIdeal.CNE;
in_intNav.aid.vN = out_inertNavIdeal.vN;
in_intNav.aid.CBN = out_inertNavIdeal.CBN;
in_intNav.aid.alpha = out_inertNavIdeal.alpha;
in_intNav.traj.CNE = out_inertNavIdeal.CNE;
in_intNav.traj.CBN = out_inertNavIdeal.CBN;
in_intNav.traj.fB = out_trajGen.fBTrue;
in_intNav.traj.omegaIBB = out_trajGen.omegaIBBTrue;
in_intNav.traj.FCN = out_inertNavIdeal.FCN;
in_intNav.traj.gN = out_inertNavIdeal.gN;
[out_intNavError, auxOut_intNavError] = intnav_kfvld(in_intNav, cfg_intNav, par_intNav, iniCon_intNav, cst_intNav);

%% 后处理
measResidErr = auxOut_intNavError.kalmFilt_measResid - auxOut_intNavError.kalmFilt_measResidApplyUc;
structMeasResid(1) = navparerrarr2struct(auxOut_intNavError.kalmFilt_measResid(:, 1:10), [1:10 NaN(1, 5)], false);
structMeasResid(2) = navparerrarr2struct(auxOut_intNavError.kalmFilt_measResidApplyUc(:, 1:10), [1:10 NaN(1, 5)], false);
plotnavparerr(out_intNavError.kalmFilt_tUpd, structMeasResid, {'校正之前系统量测残差', '校正之后系统量测残差'}, [], [], [], '校正前后系统量测残差');
disp('校正前后系统量测残差');
snapnow;
structMeasResidErr = navparerrarr2struct(measResidErr(:, 1:10), [1:10 NaN(1, 5)], false);
plotnavparerr(out_intNavError.kalmFilt_tUpd, structMeasResidErr, {'系统量测残差绝对误差'}, [], [], [], '系统量测残差绝对误差');
disp('系统量测残差绝对误差');
snapnow;