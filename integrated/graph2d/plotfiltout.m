function [] = plotfiltout( t, M, navParErrIndexMap, accParIndexMap, gyroParIndexMap, parName, tSects, figName, hFig )
%PLOTFILTOUT 绘制滤波器（平滑器）输出的状态向量或状态误差协方差
%
% Input Arguments:
% t: 长度为m的元胞向量，各元素为列向量，为M中相应元素对应的时间，单位s
% M: 长度为m的元胞向量，各元素为二维矩阵，视为一组参数，矩阵行数与t中对应元素的长度相等
% navParErrIndexMap: 1*15向量，M中导航参数误差对应的列数，具体参见navparerrarr2struct函数帮助
% accParIndexMap: 1*84向量，M中加速度计各参数对应的列数，具体参见acctriadpararr2struct函数帮助
% gyroParIndexMap: 1*54向量，M中陀螺各参数对应的列数，具体参见gyrotriadpararr2struct函数帮助
% parName: 长度为m的元胞向量，各元素为字符串，对应各组参数的名称，默认为空
% figName: 字符串，绘图名称的前缀，后缀为导航参数、加速度计参数、陀螺参数，默认为空字符串
% hFig: 长度为3的元胞向量，各元素为导航参数、加速度计参数及陀螺参数图形句柄，默认各元素均为空
% 其它参数: 参见plotstructpar函数帮助

m = length(t);

if nargin < 9
    hFig = cell(1, 3);
end
if nargin < 8
    figName = '';
end
if nargin < 7
    tSects = [];
end
if (nargin<6) || isempty(parName)
    parName = cell(1, m);
end

% NOTE: 本函数使用线性索引
for i=1:m
    navPar(1, i) = navparerrarr2struct(M{i}, navParErrIndexMap); %#ok<*AGROW>
    accPar(1, i) = acctriadpararr2struct(M{i}, accParIndexMap);
    gyroPar(1, i) = gyrotriadpararr2struct(M{i}, gyroParIndexMap);
end
if ~isempty(figName)
    figName = [figName '-'];
end
if ~all(isnan(navParErrIndexMap))
    plotnavparerr(t, navPar, false, parName, [], tSects, [figName '导航参数'], hFig{1});
end
if ~all(isnan(accParIndexMap))
    plotacctriadpar(t, accPar, parName, [], tSects, [figName '加速度计参数'], 's', 'o', hFig{2});
end
if ~all(isnan(gyroParIndexMap))
    plotgyrotriadpar(t, gyroPar, parName, [], tSects, [figName '陀螺参数'], 's', 'o', hFig{3});
end
end