function [] = plotfiltauxout_synth(tUpd, measResid, measResidCovMatDiagSqrt, chiSqIndex, obsMask, tSects)
%PLOTFILTAUXOUT_SYNTH 绘制滤波器的综合辅助输出
%
% Tips:
% 1. 若某输入为空，则不绘制与该输入对应的图
%
% Input Arguments:
% tUpd: 元胞向量，各元素为代表一种量测更新时刻的列向量，单位s
% measResid: 元胞向量，长度与tUpd相等，各元素为代表一种量测的二维矩阵，行数与tUpd长度相等，列数与该种量测向量尺寸相等，按列为各量测的残差，单位为国际单位制单位
% measResidCovMatDiagSqrt: 元胞向量，长度与tUpd相等，各元素为代表一种量测的二维矩阵，行数与tUpd长度相等，列数与该种量测向量尺寸相等，按列为各量测残差协方差矩阵对角线值的平方根，单位为国际单位制单位
% chiSqIndex: 元胞向量，长度与tUpd相等，各元素为代表一种量测的向量，长度与tUpd长度相等，该种量测的χ^2检验指标值，单位为国际单位制单位
% obsMask: 元胞向量，各元素为代表一种量测的逻辑向量，长度11，true表示该量测与对应的系统状态属于同一类物理量且单位相同
% tSects: 参见plotstructpar函数帮助

fullNavParErrNum = 15;
plotArg = '';
tUpdMin = Inf;
tUpdMax = -Inf;

for i=1:length(tUpd)
    if ~isempty(tUpd{i}) && ~isempty(measResid{i}) && ~isempty(measResidCovMatDiagSqrt{i})
        plotfiltout({tUpd{i}, tUpd{i}, tUpd{i}}, {measResid{i}, measResidCovMatDiagSqrt{i}*2, -measResidCovMatDiagSqrt{i}*2}, ...
            [mask2ind(obsMask{i}, 1, true) NaN(1, fullNavParErrNum-length(obsMask{i}))], NaN(1, 84), NaN(1, 54), {'残差', '残差标准差*2', '-残差标准差*2'}, tSects, '量测残差');
    end
    
    if ~isempty(tUpd{i}) && ~isempty(chiSqIndex{i})
        tUpdMin = min(tUpdMin, min(tUpd{i}));
        tUpdMax = max(tUpdMax, max(tUpd{i}));
        plotArg = [plotArg 'tUpd{' int2str(i) '},' 'chiSqIndex{' int2str(i) '},'];
    end
end
if ~isempty(plotArg)
    preparefig('χ^2检验指标值');
    eval(['plot(' plotArg(1:(end-1)) ');']);
    xlabel('t(s)');
    xperplines_ylim(tSects, tUpdMax, tUpdMin);
    legend('show', 'Location', 'Best');
    grid on;
end
end