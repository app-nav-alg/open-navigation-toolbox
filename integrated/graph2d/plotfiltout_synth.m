function [] = plotfiltout_synth(t, err, tExtrap, PDiagSqrt, tUpd, xPost, INSSensStateCor, sysStateMask, sensStateMask, tSects)
%PLOTFILTOUT_SYNTH 绘制滤波器的综合输出
%
% Tips:
% 1. 若某输入为空，则不绘制与该输入对应的图
%
% Input Arguments:
% t: 列向量，err各采样对应的时间，单位s
% err: 二维矩阵，行数与t长度相等，列数与系统状态个数（sysStateMask中true元素个数）相等，各系统状态的误差值，单位为国际单位制单位
% tExtrap: 列向量，卡尔曼滤波外推时刻，单位s
% PDiagSqrt: 二维矩阵，行数与tExtrap长度相等，列数与系统状态与器件状态个数（sensStateMask中true元素个数）之和相等，各状态的误差标准差，单位为国际单位制单位
% tUpd: 列向量，卡尔曼滤波更新时刻，单位s
% xPost: 二维矩阵，行数与tUpd长度相等，列数与系统状态与器件状态个数之和相等，各状态的更新后的值，单位为国际单位制单位
% INSSensStateCor: 二维矩阵，行数与tUpd长度相等，列数为36，各状态的更新后的值，单位为国际单位制单位
% sysStateMask, sensStateMask: 参见naverrdynmat_inssens2sysblk函数帮助
% tSects: 参见plotstructpar函数帮助

fullNavParErrNum = 15;
fullAccStateNum = 15;
fullGyroStateNum = 21;

INSSysStateNum = nnz(sysStateMask);
INSAccStateNum = nnz(sensStateMask(1:fullAccStateNum));

navParErrIndexMap = [mask2ind(sysStateMask, 1, true) NaN(1, fullNavParErrNum-length(sysStateMask))];
accParIndexMap = [NaN(1, 3) NaN(1, 9) mask2ind(sensStateMask([1:6 13:fullAccStateNum]), INSSysStateNum+1, true) NaN(1, 3) NaN(1, 9) NaN(1, 9) NaN(1, 3) NaN(1, 9) NaN(1, 3) NaN(1, 27)];
gyroParIndexMap = [NaN(1, 3) NaN(1, 9) mask2ind(sensStateMask((fullAccStateNum+1):fullAccStateNum+6), INSSysStateNum+INSAccStateNum+1, true) NaN(1, 6) NaN(1, 9) NaN(1, 9) NaN(1, 3) NaN(1, 9)];

if ~isempty(t) && ~isempty(err)
    plotfiltout({t, tExtrap, tExtrap}, {err, PDiagSqrt*2, -PDiagSqrt*2}, navParErrIndexMap, NaN(1, 84), NaN(1, 54), ...
        {'误差', '误差标准差*2', '-误差标准差*2'}, tSects, '误差');
end

if ~isempty(tExtrap) && ~isempty(PDiagSqrt)
    plotfiltout({tExtrap}, {PDiagSqrt}, navParErrIndexMap, accParIndexMap, gyroParIndexMap, [], tSects, 'P对角线平方根');
end

if ~isempty(tUpd) && ~isempty(xPost)
    plotfiltout({tUpd}, {xPost}, navParErrIndexMap, accParIndexMap, gyroParIndexMap, [], tSects, 'x更新步骤估计值');
end

if ~isempty(tUpd) && ~isempty(xPost)
    plotfiltout({tUpd}, {cumsum(xPost)}, navParErrIndexMap, accParIndexMap, gyroParIndexMap, [], tSects, 'x更新步骤累积估计值');
end

if ~isempty(tUpd) && ~isempty(INSSensStateCor)
    plotfiltout({tUpd}, {INSSensStateCor}, NaN(1, fullNavParErrNum), [NaN(1, 3) NaN(1, 9) [1:6 13:15] NaN(1, 6) NaN(1, 6) [NaN 9 11 7 NaN 12 8 10 NaN] NaN(1, 42)], [NaN(1, 3) NaN(1, 9) 16:21 NaN(1, 12) [NaN 24 26 22 NaN 27 23 25 NaN] NaN(1, 3) NaN(1, 3) 28:36], [], tSects, '惯组参数补偿量');
end
end