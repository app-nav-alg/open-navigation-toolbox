function [sectInd] = getsectind(t, tSects, nSects)
%GETSECTIND 计算t时刻对应的分段序号

sectInd = find(t>=tSects, 1, 'last');
if sectInd > nSects
    sectInd = nSects;
end