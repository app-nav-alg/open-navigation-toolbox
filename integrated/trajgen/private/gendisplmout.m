function [out] = gendisplmout(vV, vVErrFree, vVInt, vVIntErrFree, cfg, par)
%GENDISPLMOUT 由速度及其积分生成位移计输出

% 瞬时输出
out.vV = squeeze(vV)';
out.vVErrFree = squeeze(vVErrFree)';
% 增量输出
out.displInc = diff(squeeze(vVInt)'); % t时刻对应的是t-tsamplePeriod到t时刻的速度积分（位移增量）值
out.displmOut = NaN(size(out.displInc));
for i=1 : size(out.displInc, 1)
    out.displmOut(i, :) = (par.SF0 .* out.displInc(i, :)')';
end
% 无误差增量输出
out.displIncErrFree = diff(squeeze(vVIntErrFree)'); % t时刻对应的是t-tsamplePeriod到t时刻的速度积分（位移增量）值
out.displmOutErrFree = NaN(size(out.displIncErrFree));
for i=1 : size(out.displIncErrFree, 1)
    out.displmOutErrFree(i, :) = (par.SF0 .* out.displIncErrFree(i, :)')';
end
% 量化输出
if cfg.quantizeOut
    out.displmOut = quantize(out.displmOut);
    out.displmOutErrFree = quantize(out.displmOutErrFree);
end
end