function [out] = genimuout(fB, omegaIBB, fBErrFree, omegaIBBErrFree, ...
    fBInt, omegaIBBInt, fBIntErrFree, omegaIBBIntErrFree, ...
    cfg, par, samplePeriod, tSects, nSects)
%GENIMUOUT 由比力、角速度及其积分生成IMU输出

% 比力与角速度输出
out.fB = permute(fB, [3 1 2]);
out.omegaIBB = omegaIBB;
out.fBErrFree = permute(fBErrFree, [3 1 2]);
out.omegaIBBErrFree = omegaIBBErrFree;
% 加速度计输出
out.specVelInc = diff(permute(fBInt,[3 1 2])); % t时刻对应的是t-samplePeriod到t时刻的比力积分（比速度增量）值
out.accOut = NaN(size(out.specVelInc));
for i=1 : size(out.specVelInc, 1)
    out.accOut(i, :) = (par.acc.KScal0(:, getsectind(i*samplePeriod, tSects, nSects)) .* out.specVelInc(i, :)')';
end
% 陀螺输出
out.angleInc = diff(omegaIBBInt); % t时刻对应的是t-samplePeriod到t时刻的角速度积分（角度增量）值
out.gyroOut = NaN(size(out.angleInc));
for i=1 : size(out.angleInc, 1)
    out.gyroOut(i, :) = (par.gyro.KScal0(:, getsectind(i*samplePeriod, tSects, nSects)) .* out.angleInc(i, :)')';
end
% 加速度计的无误差输出
out.specVelIncErrFree = diff(permute(fBIntErrFree,[3 1 2])); % t时刻对应的是t-samplePeriod到t时刻的比力积分（比速度增量）值
out.accOutErrFree = NaN(size(out.specVelIncErrFree));
for i=1 : size(out.specVelIncErrFree, 1)
    out.accOutErrFree(i, :) = (par.acc.KScal0(:, getsectind(i*samplePeriod, tSects, nSects)) .* out.specVelIncErrFree(i, :)')';
end
% 陀螺的无误差输出
out.angleIncErrFree = diff(omegaIBBIntErrFree); % t时刻对应的是t-samplePeriod到t时刻的角速度积分（角度增量）值
out.gyroOutErrFree = NaN(size(out.angleIncErrFree));
for i=1 : size(out.angleIncErrFree, 1)
    out.gyroOutErrFree(i, :) = (par.gyro.KScal0(:, getsectind(i*samplePeriod, tSects, nSects)) .* out.angleIncErrFree(i, :)')';
end
% 量化输出
if cfg.quantizeOut
    out.accOut = quantize(out.accOut);
    out.accOutErrFree = quantize(out.accOutErrFree);
    out.gyroOut = quantize(out.gyroOut);
    out.gyroOutErrFree = quantize(out.gyroOutErrFree);
end
% 写入文件
if ~isempty(cfg.outFilePath)
    writeimuoutfile_tradfmt(cfg.outFilePath, out.accOut, out.gyroOut, out.t(2:end, :), cfg.outPrecision);
end
end