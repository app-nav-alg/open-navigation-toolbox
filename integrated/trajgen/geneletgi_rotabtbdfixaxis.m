function [ vebr, omegaNBB, omegaNBBRate, t ] = geneletgi_rotabtbdfixaxis( tStart, rotVec, rotAngRate, tAcc, tDec, dt )
%GENELETGI_ROTABTBDFIXAXIS 生成基本的轨迹生成器输入量-绕相对于体坐标系固定的轴旋转
%
% Tips:
% # 在tAcc时间内匀加速到rotAngRate，然后匀角速度运动，最后在tDec时间内匀减速到0
%
% Input Arguments:
% # tStart: 标量，开始时间，单位s
% # rotVec: 3*1向量，旋转矢量在体坐标系上的投影，单位rad
% # rotAngRate: 正标量，旋转角速率，单位rad/s
% # tAcc: 标量，加速时间，单位s
% # tDec: 标量，减速时间，单位s
% # dt: 标量，输出周期，单位s
%
% Output Arguments:
% # vebr, omegaNBB, omegaNBBRate, t: 参见geneletgi_static函数帮助
%
% References:
% # 《应用导航算法工程基础》“绕V系固定轴转动指定角度”

if ((tAcc<0) || (tDec<0))
    error('加减速时间小于零');
end
if (dt <= 0)
    error('周期不大于零');
end
if (rotAngRate <= 0)
    error('角速率不大于零');
end
if ((abs(rem_round(tAcc, dt))>eps(tAcc)) || (abs(rem_round(tDec, dt))>eps(tDec)))
    warning('geneletgi_rotabtbdfixaxis:warning', '加速时间或减速时间不是采样周期的整数倍');
end
angle = norm(rotVec);
angleAcc = rotAngRate * tAcc / 2;
angleDec = rotAngRate * tDec / 2;
tUni = (angle - angleAcc - angleDec) / rotAngRate;
if (tUni < 0)
    error('加减速过程过慢，不能在指定转动角度内达到指定角速度');
end
if (abs(rem_round(tUni, dt)) > eps(tUni))
    warning('geneletgi_rotabtbdfixaxis:warning', '匀速转动时间不是采样周期的整数倍');
end
t = (0:1:round((tAcc+tUni+tDec)/dt))' * dt;
n = size(t, 1);
omegaNBB = zeros(n, 3);
omegaNBBRate = zeros(n, 3);
rotUniVec = rotVec / angle;
i = 1;
while i<=n
    if (t(i) < tAcc)
        omegaNBB(i, :) = (rotAngRate * t(i) / tAcc) * rotUniVec';
        omegaNBBRate(i, :) = rotAngRate / tAcc *  rotUniVec';
    elseif (t(i) < tAcc+tUni)
        omegaNBB(i, :) = rotAngRate * rotUniVec';
        omegaNBBRate(i, :) = zeros(1, 3);
    else
        omegaNBB(i, :) = (rotAngRate * (tAcc+tUni+tDec-t(i)) / tDec)  * rotUniVec';
        omegaNBBRate(i, :) = -(rotAngRate / tDec)  * rotUniVec';
    end
    i = i + 1;
end
t = t + tStart;
vebr = zeros(n, 3);
end