function [ vebr, omegaNBB, omegaNBBRate, tSects ] = gentgi_statmultipos(rotVec, rotAngRate, posDuration, rotAngleErrStd)
%GENTGI_STATMULTIPOS 生成轨迹生成器输入量-静止→转动→静止……转动→静止轨迹
%
% Input Arguments:
% rotVec: n*3矩阵，n为转动次数，各行分别为各次转动的旋转矢量在体坐标系上的投影，单位rad
% rotAngRate： 正标量，转动角速率，单位rad/s
% posDuration： 向量，各静止位置的时间，单位s
% rotAngleErrStd: 标量，转动角度误差随机量的最大绝对值，单位°
%
% Output Arguments:
% vebr, omeganbb, tSects: 参见gentgi_static函数帮助

%% 轨迹参数
rotAccTime = 1; % 单位：s
rotDecTime = 1; % 单位：s
dt = 0.1; % 单位：s

%% 生成并合并各轨迹段
% 首个静止段
[vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_static(0, posDuration(1), dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(end);
for i=1:size(rotVec, 1)
    % 转动段
    rotUniVec = rotVec(i, :)' / norm(rotVec(i, :));
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
        rotVec(i, :)'+rotUniVec*rotAngleErrStd*randn(1)/180*pi, rotAngRate, rotAccTime, rotDecTime, dt);
    t = vertcat(t, sectt);
    vebrValues = vertcat(vebrValues, sectvebrValues);
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    tSects(i*2+1, 1) = t(end);
    % 静止段
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_static(t(end), posDuration(i+1), dt);
    t = vertcat(t, sectt);
    vebrValues = vertcat(vebrValues, sectvebrValues);
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    tSects(i*2+2, 1) = t(end);
end

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBRate.time = t;
omegaNBBRate.signals.values = omegaNBBRateValues;
omegaNBBRate.signals.dimensions = 3;
end