function [ vebr, omegaNBB, omegaNBBRate, t ] = geneletgi_static( tStart, tDuration, dt )
%GENELETGI_STATIC 生成基本的轨迹生成器输入量-静止
%
% Input Arguments:
% tStart: 标量，开始时间，单位s
% tDuration: 标量，静止持续时间，单位s
% dt: 标量，输出周期，单位s
%
% Output Arguments:
% vebr: n*3矩阵，惯组坐标系下的惯组加速度，单位m/s^2
% omeganbb: n*3矩阵，惯组坐标系下的惯组相对于导航坐标系的角速度，单位rad/s
% t: n*1向量，n为输出采样数，输出时间，单位s

if (mod(tDuration, dt) ~= 0)
    warning('geneletgi_static:warning', '时间段长度不是采样周期的整数倍');
end
t = (tStart:dt:tStart+tDuration)';
vebr = zeros(length(t), 3);
omegaNBB = zeros(length(t), 3);
omegaNBBRate = zeros(length(t), 3);
end