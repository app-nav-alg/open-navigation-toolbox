function [ vebr, omegaNBB, omegaNBBrate, tSects ] = gentgi_static(duration)
%GENTGI_STATIC 生成轨迹生成器输入量-静态轨迹
%
% Input Arguments:
% duration: 标量，静止时间，单位s
%
% Output Arguments:
% vebr: 结构体格式的From Workspace输入量，惯组坐标系下的惯组加速度，单位m/s^2
% omegaNBB: 结构体格式的From Workspace输入量，惯组坐标系下的惯组相对于导航坐标系的角速度，单位rad/s
% tSects: 列向量，各基本轨迹段的分隔时间点，单位s

dt = 0.1; % 单位：s

[vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_static(0, duration, dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(length(t));

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBrate.time = t;
omegaNBBrate.signals.values = omegaNBBRateValues;
omegaNBBrate.signals.dimensions = 3;
end