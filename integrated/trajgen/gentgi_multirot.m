function [ vebr, omegaNBB, omegaNBBRate, tSects ] = gentgi_multirot(rotVec, rotAngRate, rotAngleErrStd)
%GENTGI_ROTMULTIPOS 生成轨迹生成器输入量-连续转动轨迹
%
% Input Arguments:
% rotVec: n*3矩阵，n为转动次数，各行分别为各次转动的旋转矢量在体坐标系上的投影，单位rad
% rotAngRate: n*1向量，各次转动的转动角速率，单位rad/s
% rotAngleErrStd: n*1向量，转动角度误差随机量的最大绝对值，单位°
%
% Output Arguments:
% vebr, omegaNBB, omegaNBBRate, tSects: 参见gentgi_static函数帮助

%% 轨迹参数
rotAccTime = 1; % 单位：s
rotDecTime = 1; % 单位：s
dt = 0.1; % 单位：s

%% 生成并合并各轨迹段
% 初始化
tSects(1, 1) = 0;
for i=1:size(rotVec, 1)
    % 转动段
    rotUniVec = rotVec(i, :)' / norm(rotVec(i, :));
    if i == 1
        [vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_rotabtbdfixaxis(0, ...
        rotVec(i, :)'+rotUniVec*rotAngleErrStd(i, 1)*randn(1)/180*pi, rotAngRate(i, 1), rotAccTime, rotDecTime, dt);
    else
        [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), ...
        rotVec(i, :)'+rotUniVec*rotAngleErrStd(i, 1)*randn(1)/180*pi, rotAngRate(i, 1), rotAccTime, rotDecTime, dt);
        t = vertcat(t, sectt);
        vebrValues = vertcat(vebrValues, sectvebrValues);
        omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
        omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    end
    tSects(i+1, 1) = t(end);
end

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBRate.time = t;
omegaNBBRate.signals.values = omegaNBBRateValues;
omegaNBBRate.signals.dimensions = 3;
end