function [ vebr, omegaNBB, tSects ] = gentgi_rocking(rotMagnitude, rotPeriod, iniPhase, span, slopeCoff)
%GENTGI_ROCKING 生成轨迹生成器输入量-摇摆基座
%
%
% Input Arguments:
% rotMagnitude: 1*3矩阵，体坐标系XYZ各轴角运动摇摆的幅度，单位rad
% rotPeriod: 1*3向量，体坐标系XYZ各轴角运动一个完整摇摆周期的时间，单位s
% iniPhase: 1*3向量，体坐标系XYZ各轴摇摆的初始相位，单位rad
% span: 标量，运动持续时间，单位s
% slopeCoff: 1*3向量，体坐标系XYZ各轴摇摆的斜率系数，单位rad/s
%
% Output Arguments:
% vebr, omegaNBB, tSects: 参见gentgi_static函数帮助

% example:
% rotMagnitude = [6 8 7]*pi/180; % 纵摇、横摇、艏摇
% rotPeriod = [10 11 12];
% iniPhase = [pi/2 pi/3 pi/4];

if nargin < 5
    slopeCoff = [0 0 0];
end

%% 轨迹参数
dt = 0.1; % 单位：s
t = 0:dt:span;

vebrValues = zeros(length(t), 3);
omegaNBBValues = zeros(length(t), 3);

%% 角运动
att_oula = zeros(length(t),3);
for i = 1:length(t)
    att_oula(i,:) = rotMagnitude .* sin(2*pi./rotPeriod .* t(i) + iniPhase) + slopeCoff.* t(i);
    phi = att_oula(i,1);
    theta = att_oula(i,2);
    transMatrix = [1 0 -sin(theta); 0 cos(phi) cos(theta)*sin(phi);  0 -sin(phi) cos(phi)*cos(theta)];
    angleRate = rotMagnitude .* cos(2*pi./rotPeriod .* t(i) + iniPhase).*(2*pi./rotPeriod);
    omegaNBBValues(i,:) = (transMatrix * angleRate')';
end

%% 生成并合并各轨迹段
tSects = t;
vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
end