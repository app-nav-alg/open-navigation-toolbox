function [ vebr, omegaNBB, omegaNBBRate, t ] = geneletgi_transalgbdfixaxis_targdisp( tStart, axisUniVec, displace, vel, tAcc, tDec, dt)
%GENELETGI_TRANSALGBDFIXAXIS_TARGDISP 生成基本的轨迹生成器输入量-沿相对于体坐标系固定的轴平移（指定目标位移）
%
% Tips:
% # 在tAcc时间内匀加速到vel，然后匀速运动，最后在tDec时间内匀减速到0，使位移为displace
%
% Input Arguments:
% # tStart: 标量，开始时间，单位s
% # axisUniVec: 3*1向量，沿平移轴的单位矢量在体坐标系上的投影
% # displace: 标量，位移，单位m
% # vel: 标量，速度，单位m/s
% # tAcc: 标量，加速时间，单位s
% # tDec: 标量，减速时间，单位s
% # dt: 标量，输出周期，单位s
%
% Output Arguments:
% # vebr, omegaNBB, omegaNBBRate, t: 参见geneletgi_static函数帮助
%
% References:
% # 《应用导航算法工程基础》“沿V系固定轴平移至指定位移距离”

if ((norm(axisUniVec)>1+eps) || (norm(axisUniVec)<1-eps))
    error('沿平移轴的单位向量长度非1');
end
if ((tAcc<0) || (tDec<0))
    error('加减速时间小于零');
end
if (dt <= 0)
    error('周期不大于零');
end
if (sign(displace) ~= sign(vel))
    error('位移与速度符号不一致');
end
if ((abs(rem_round(tAcc, dt))>eps(tAcc)) || (abs(rem_round(tDec, dt))>eps(tDec)))
    warning('geneletgi_transalgbdfixaxis:warning', '加速时间或减速时间不是采样周期的整数倍');
end
dispAcc = vel * tAcc / 2;
dispDec = vel * tDec / 2;
tUni = (displace - dispAcc - dispDec) / vel;
if (tUni < 0)
    error('加减速过程过慢，不能在指定位移内达到指定速度');
end
if (abs(rem_round(tUni, dt)) > eps(tUni))
    warning('geneletgi_transalgbdfixaxis:warning', '匀速运动时间不是采样周期的整数倍');
end
t = (0:1:round((tAcc+tUni+tDec)/dt))';
t = t * dt;
n = length(t);
vebr = zeros(n, 3);
i = 1;
while i <= n
    if (t(i) < tAcc)
        vebr(i, :) = (vel / tAcc) * axisUniVec';
    elseif (abs(t(i)-tAcc) <= eps(tAcc))
        % 加速度将突变，加入ZCD点
        t = vertcat(t(1:i-1, :), tAcc, t(i:n, :));
        vebr = vertcat(vebr(1:i-1, :), zeros(n-i+2, 3));
        vebr(i, :) = (vel / tAcc) * axisUniVec';
        i = i + 1;
        vebr(i, :) = zeros(1, 3);
        n = n + 1;
    elseif (t(i) < tAcc+tUni)
        vebr(i, :) = zeros(3, 1);
    elseif (abs(t(i)-tAcc-tUni) <= eps(tAcc+tUni))
        % 加速度将突变，加入ZCD点
        t = vertcat(t(1:i-1, :), tAcc+tUni, t(i:n, :));
        vebr = vertcat(vebr(1:i-1, :), zeros(n-i+2, 3));
        vebr(i, :) = zeros(1, 3);
        i = i + 1;
        vebr(i, :) = -(vel / tDec)  * axisUniVec';
        n = n + 1;
    else
        vebr(i, :) = -(vel / tDec)  * axisUniVec';
    end
    i = i + 1;
end
t = t + tStart;
omegaNBB = zeros(n, 3);
omegaNBBRate = zeros(n, 3);
end