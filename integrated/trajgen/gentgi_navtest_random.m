function [ vebr, omegaNBB, omegaNBBRate, tSects ] = gentgi_navtest_random( )
%GENTGI_NAVTEST_RANDOM 生成轨迹生成器输入量-导航算法测试轨迹（随机型）
%
% Output Arguments:
% vebr, omegaNBB, tSects: 参见gentgi_static函数帮助

%% 轨迹参数
randSectNum = 25;
dt = 0.1; % 单位：s

%% 生成并合并各轨迹段
% 静止段
[vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_static(0, 60, dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(end);
% 随机段
for i=1:randSectNum
    rotVec = rand(3, 1) * 20 * pi;
    rotAngRate = norm(rotVec) / (30+randi(100, 1));
    [~, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), rotVec, rotAngRate, randi(10, 1), randi(10, 1), dt);
    sectvebrValues = 30 * (randn(length(sectt), 3));
    t = vertcat(t, sectt);
    vebrValues = vertcat(vebrValues, sectvebrValues);
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    tSects(end+1, 1) = t(end);
end

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBRate.time = t;
omegaNBBRate.signals.values = omegaNBBRateValues;
omegaNBBRate.signals.dimensions = 3;
end