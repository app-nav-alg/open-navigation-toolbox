function [ vebr, omegaNBB, omegaNBBRate, t ] = geneletgi_transalgbdfixaxis_targvel( tStart, axisUniVec, vel, accel, tAcc, tDec, dt)
%GENELETGI_TRANSALGBDFIXAXIS_TARGVEL 生成基本的轨迹生成器输入量-沿相对于体坐标系固定的轴平移（指定目标速度）
%
% Tips:
% # 在tAcc时间内加速度线性增加到accel，然后以accel匀加速度运动，最后在tDec时间内加速度线性减小到0，并使速度达到vel
%
% Input Arguments:
% # tStart: 标量，开始时间，单位s
% # axisUniVec: 3*1向量，沿平移轴的单位矢量在体坐标系上的投影
% # vel: 标量，速度，单位m/s
% # accel: 标量，加速度，单位m/s^2
% # tAcc: 标量，加速时间，单位s
% # tDec: 标量，减速时间，单位s
% # dt: 标量，输出周期，单位s
%
% Output Arguments:
% # vebr, omegaNBB, omegaNBBRate, t: 参见geneletgi_static函数帮助
%
% References:
% # 《应用导航算法工程基础》“沿V系固定轴平移至指定速率增量”

if ((norm(axisUniVec)>1+eps) || (norm(axisUniVec)<1-eps))
    error('沿平移轴的单位向量长度非1');
end
if ((tAcc<0) || (tDec<0))
    error('加减速时间小于零');
end
if (dt <= 0)
    error('周期不大于零');
end
if (sign(vel) ~= sign(accel))
    error('速度与加速度符号不一致');
end
if ((abs(rem_round(tAcc, dt))>eps(tAcc)) || (abs(rem_round(tDec, dt))>eps(tDec)))
    warning('geneletgi_transalgbdfixaxis:warning', '加速时间或减速时间不是采样周期的整数倍');
end
velAcc = accel * tAcc / 2;
velDec = accel * tDec / 2;
tUni = (vel - velAcc - velDec) / accel;
if (tUni < 0)
    error('加减速过程过慢，不能在指定位移内达到指定速度');
end
if (abs(rem_round(tUni, dt)) > eps(tUni))
    warning('geneletgi_transalgbdfixaxis:warning', '匀速运动时间不是采样周期的整数倍');
end
t = (0:1:round((tAcc+tUni+tDec)/dt))';
t = t * dt;
vebr = zeros(length(t), 3);
for i=1:length(t)
    if (t(i) < tAcc)
        vebr(i, :) = (accel * t(i) / tAcc) * axisUniVec';
    elseif (t(i) <= tAcc+tUni)
        vebr(i, :) = accel * axisUniVec';
    else
        vebr(i, :) = (accel * (tAcc+tUni+tDec-t(i)) / tDec)  * axisUniVec';
    end
end
t = t + tStart;
omegaNBB = zeros(length(t), 3);
omegaNBBRate = zeros(length(t), 3);
end