function [vebr, omegaNBB, omegaNBBrate, tSects] = gentgi_navtest_L_racetrack(transUniVec, rotVec, transTimeVec, transVel)
%GENTGI_NAVTEST_L_RACETRACK 生成轨迹生成器输入量-导航算法测试轨迹（L轨迹或者Z轨迹）
%
% Input Arguments:
% transUniVec: 3*1向量，沿平移轴的单位矢量在体坐标系上的投影
% rotVec: 3*n向量，旋转矢量在体坐标系上的投影，n表示旋转次数，L型n为1，Z型n为2，单位rad
% transTimeVec: 1*（n+3）向量，静止或匀速行驶时间，分别对应初始静止时间1、匀速行驶n+1、结束静止时间1
% transVel: 标量，速度，单位m/s，默认为20
%
% Output Arguments:
% vebr, omegaNBB, tSects: 参见gentgi_static函数帮助

% example:
% 1.直线行驶：transUniVec = [0 1 0]'; rotVec = zeros(3, 0); transTimeVec = [10 20 10];
% 2.L型行驶： transUniVec = [0 1 0]'; rotVec = [0 0 -pi/2]'; transTimeVec = [10 20 20 10]; 
% 3.Z型行驶： transUniVec = [0 1 0]'; rotVec = [0 0 -pi/2; 0 0 pi/2]'; transTimeVec = [10 20 20 20 10]; 

if nargin < 4
    transVel = 20; % 单位：m/s
end

%% 轨迹参数
transAccTime = 1; % 单位：s
transDecTime = 1; % 单位：s
transAccel = 5; % 单位：m/s/s
rotAccTime = 1; % 单位：s
rotDecTime = 1; % 单位：s
rotAngRate = pi/10; % 单位：rad/s
dt = 0.1; % 单位：s

%% 生成并合并各轨迹段
% 静止段
[vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_static(0, transTimeVec(1, 1), dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(end);
% 加速或减速
[sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_transalgbdfixaxis_targvel(t(end), transUniVec, transVel, transAccel, transAccTime, transDecTime, dt);
t = vertcat(t, sectt(2:end)); %#ok<*AGROW>
vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
tSects(end+1, 1) = t(end);
% 匀速或静止
[sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_static(t(end), transTimeVec(1, 2), dt);
t = vertcat(t, sectt(2:end));
vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
tSects(end+1, 1) = t(end);
for i = 1:size(rotVec, 2)
    % 转动
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_rotabtbdfixaxis(t(end), rotVec(:, i), rotAngRate, rotAccTime, rotDecTime, dt);
    t = vertcat(t, sectt(2:end));
    vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
    tSects(end+1, 1) = t(end);
    % 匀速或静止
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_static(t(end), transTimeVec(1, i+2), dt);
    t = vertcat(t, sectt(2:end));
    vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
    tSects(end+1, 1) = t(end);
end
% 加速或减速
[sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_transalgbdfixaxis_targvel(t(end), -transUniVec, transVel, transAccel, transAccTime, transDecTime, dt);
t = vertcat(t, sectt(2:end)); %#ok<*AGROW>
vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
tSects(end+1, 1) = t(end);
% 匀速或静止
[sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_static(t(end), transTimeVec(1, end), dt);
t = vertcat(t, sectt(2:end));
vebrValues = vertcat(vebrValues, sectvebrValues(2:end,:));
omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues(2:end,:));
omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues(2:end,:));
tSects(end+1, 1) = t(end);

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBrate.time = t;
omegaNBBrate.signals.values = omegaNBBRateValues;
omegaNBBrate.signals.dimensions = 3;
end