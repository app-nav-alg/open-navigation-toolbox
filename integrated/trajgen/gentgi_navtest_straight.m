function [ vebr, omegaNBB, omegaNBBRate, tSects ] = gentgi_navtest_straight( )
%GENTGI_NAVTEST_STRAIGHT 生成轨迹生成器输入量-导航算法测试轨迹（直线轨迹）
%
% Output Arguments:
% vebr, omeganbb, tSects: 参见gentgi_static函数帮助

%% 轨迹参数
transAccTime = 1; % 单位：s
transDecTime = 1; % 单位：s
transVel = 20; % 单位：m/s
transAccel = 5; % 单位：m/s/s
transUniVec = [0 1 0; 0 -1 0]'; % 负号代表逆向转动
transTimeVec = [ 20 20 ]';

dt = 0.1; % 单位：s

%% 生成并合并各轨迹段
% 静止段
[vebrValues, omegaNBBValues, omegaNBBRateValues, t] = geneletgi_static(0, 20, dt);
tSects(1, 1) = 0;
tSects(2, 1) = t(end);
%直线加速、匀速、减速段
for i=1:size(transUniVec,2)
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_transalgbdfixaxis_targvel(t(end), transUniVec(:, i), transVel, transAccel, transAccTime, transDecTime, dt);
    t = vertcat(t, sectt); %#ok<*AGROW>
    vebrValues = vertcat(vebrValues, sectvebrValues);
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    tSects(end+1, 1) = t(end);
    
    [sectvebrValues, sectomegaNBBValues, sectomegaNBBRateValues, sectt] = geneletgi_static(t(end), transTimeVec(i), dt);
    t = vertcat(t, sectt);
    vebrValues = vertcat(vebrValues, sectvebrValues);
    omegaNBBValues = vertcat(omegaNBBValues, sectomegaNBBValues);
    omegaNBBRateValues = vertcat(omegaNBBRateValues, sectomegaNBBRateValues);
    tSects(end+1, 1) = t(end);
end

vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omegaNBB.time = t;
omegaNBB.signals.values = omegaNBBValues;
omegaNBB.signals.dimensions = 3;
omegaNBBRate.time = t;
omegaNBBRate.signals.values = sectomegaNBBRateValues;
omegaNBBRate.signals.dimensions = 3;
end