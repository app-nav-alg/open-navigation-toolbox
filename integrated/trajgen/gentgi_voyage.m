function [ vebr, omeganbb, tSects ] = gentgi_voyage(rotMagnitude, rotPeriod, velMagnitude, velPeriod, span)
%GENTGI_VOYAGE 生成轨迹生成器输入量-海况条件
%
% Tips:
% 1. 本函数适合于生成1完全静止状态、2码头系泊状态（角运动及线运动）、3海况下匀速航行状态的轨迹
% 2. 在tAcc时间内加速度线性增加到accel，然后以accel匀加速度运动，最后在tDec时间内加速度线性减小到0，并使速度达到vel
%
% Input Arguments:
% rotMagnitude: 1*3矩阵，体坐标系XYZ各轴角运动摇摆的幅度，单位rad
% rotPeriod: 1*3向量，体坐标系XYZ各轴角运动一个完整摇摆周期的时间，单位s
% velMagnitude: 1*3矩阵，体坐标系XYZ各轴线运动幅度，单位m，设置为>=100表示匀速航行
% velPeriod：1*3矩阵，体坐标系XYZ各轴线运动一个完整周期的时间，单位s
% span: 标量，运动持续时间，单位s
%
% Output Arguments:
% vebr, omeganbb, tSects: 参见gentgi_static函数帮助

% example:
% rotMagnitude = [ 6 8 7 ]*pi/180; % 纵摇、横摇、艏摇
% rotPeriod = [ 10 11 12 ];
% velMagnitude = [ 1.0 1.1 1.2 ]; % 横荡、纵荡、垂荡
% velMagnitude = [ 0 1000 0 ]; % 匀速航行
% velPeriod = [ 4 5 6 ];

%% 轨迹参数
dt = 0.1; % 单位：s
t = 0:dt:span;

vebrValues = zeros(length(t), 3);
omeganbbValues = zeros(length(t), 3);

%% 角运动
att_oula = zeros(length(t),3);
for i=1:length(t)
    att_oula(i,:) = rotMagnitude .* sin(2*pi./rotPeriod .* t(i));
    theta = att_oula(i,1);
    gamma = att_oula(i,2);
    psi = att_oula(i,3);
    transMatrix = [ -cos(theta)*sin(gamma) cos(gamma) 0; sin(theta) 0 1; cos(theta)*cos(gamma) sin(gamma) 0 ];
    if i>=2
        deltaAtt = (att_oula(i,[3 1 2]) - att_oula(i-1,[3 1 2]))/dt;
        omeganbbValues(i,:) = (transMatrix * deltaAtt')';
    end
end

%% 线运动
posXYZ = zeros(length(t),3);
velXYZ = zeros(length(t),3);
% 匀速航行设置
transAccTime = 1; % 单位：s
transDecTime = 1; % 单位：s
transVel = 20; % 单位：m/s
transAccel = 5; % 单位：m/s/s
velAcc = transAccel * transAccTime / 2;
velDec = transAccel * transDecTime / 2;
tUni = (transVel - velAcc - velDec) / transAccel;
if (tUni < 0)
    error('加减速过程过慢，不能在指定位移内达到指定速度');
end
%
for i=1:length(t)
    if all(velMagnitude<100)
        posXYZ(i,:) = velMagnitude .* sin(2*pi./velPeriod .* t(i));
        if i>=2
            velXYZ(i,:) = (posXYZ(i,:) - posXYZ(i-1,:))/dt;
            vebrValues(i,:) = (velXYZ(i,:) - velXYZ(i-1,:))/dt;
        end
    else
        index = find(velMagnitude>=100);
        if (t(i) < transAccTime)
            vebrValues(i, index) = (transAccel * t(i) / transAccTime);
        elseif (t(i) <= transAccTime+tUni)
            vebrValues(i, index) = transAccel;
        elseif (t(i) <= transAccTime+tUni+transDecTime)
            vebrValues(i, index) = (transAccel * (transAccTime+tUni+transDecTime-t(i)) / transDecTime);
        else
            vebrValues(i,:) = zeros(1,3);
        end
    end
end

%% 生成并合并各轨迹段
tSects = t;
vebr.time = t;
vebr.signals.values = vebrValues;
vebr.signals.dimensions = 3;
omeganbb.time = t;
omeganbb.signals.values = omeganbbValues;
omeganbb.signals.dimensions = 3;
end