# 仓库简介
本仓库含工程常用的基础导航算法，大部分算法来自于[《应用导航算法工程基础》](https://item.jd.com/13015211.html)，另有部分辅助函数。


# 仓库代码与[《应用导航算法工程基础》](https://item.jd.com/13015211.html)章节的对应关系
|子目录|章节|
|:---------------|:--------------------|
|basic/kinem|1.1 运动学|
|basic/coord|1.2 导航系统常用坐标系|
|basic/earth|1.3 地球模型|
|basic/att|1.5 姿态算法|
|basic/naverr|1.6 导航误差参数|
|basic/constant|附录A 常数|
|inertial/nav|2.1 捷联惯性导航方程|
|inertial/navint|2.2 捷联惯性导航数值积分算法|
|inertial/naverr|2.3 捷联惯性导航误差方程|
|inertial/initalign|2.4 初始对准|
|inertial/sensor|2.5 惯性传感器模型|
|inertial/testing|2.5 惯性传感器模型|
|gnss/positioning|3.1 GNSS定位与定速|
|gnss/svorbit|3.2 GNSS卫星轨道的计算|
|gnss/pseudorange|3.3 GNSS伪距和伪距率的修正|
|integrated/filter|6.1 卡尔曼滤波|
|integrated/trajgen|6.2 轨迹发生器|
|util/control|6.1 卡尔曼滤波|
|util/matlab|附录B 数学相关知识|

## 仓库的其它子目录介绍
|子目录|简介|
|:---------------|:--------------------|
|util/\*|辅助函数，子目录设置及命名参考MATLAB的toolbox目录|
|\*/graph2d|平面图绘图函数|
|\*/protocol|常用协议解析函数|
|\*/test|测试脚本、测试函数及测试类|
|\*/html|测试结果|

# 关于本仓库的其它说明
1. 本仓库包含一个长期分支（即受保护的master分支），以及feature、hotfix等类型的短期分支；
2. 本仓库代码的编码规范参考[《基于MATLAB的软件编码规范》](https://gitee.com/app-nav-alg/dev-stds/blob/master/MATLABCodingStandard.tex)；
3. 本仓库的大部分函数支持[C/C++代码生成](https://ww2.mathworks.cn/help/coder/matlab-algorithm-design.html)（函数首行末尾有codegen注释）；
4. 部分算法有函数（以全小写字母命名）和类（以大骆驼拼写法命名）两个版本，对于这种算法，后续开发将基于类版本，函数版本将停止功能更新，仅做维护；
5. 主要算法有对应的基于脚本、基于函数或基于类的单元测试代码，即可用于测试算法，也可用于了解相关算法的使用方式。后续开发将优先使用[基于类的单元测试](https://ww2.mathworks.cn/help/matlab/class-based-unit-tests.html)；
6. 可以使用本仓库根目录下的[pub_runtests.m](https://gitee.com/app-nav-alg/open-navigation-toolbox/blob/master/pub_runtests.m)文件运行当前工作目录下所有的单元测试并将结果保存在当前工作目录下的“html\pub_runtests.html”中；
7. 可以使用本仓库根目录下的[makehelp.m](https://gitee.com/app-nav-alg/open-navigation-toolbox/blob/master/makehelp.m)文件生成HTML格式的帮助，生成需要[M2HTML](https://www.artefact.tk/software/matlab/m2html/)，生成后的帮助首页位于仓库根目录下的“html\autogen\index.html”。

# 反馈及参与
1. 如果您发现本仓库或[《应用导航算法工程基础》](https://item.jd.com/13015211.html)书中的错误，推荐采用提交issue的方式反馈，issue标签请选择bug，我们会尽快修复；
2. 如果您对本仓库有新的功能建议，可以先[邮件联系我们](mailto:app_nav_alg@163.com)，沟通确认后提交issue，issue标签请选择feature（实现某任务所必需的功能）或者enhancement（更有利于任务执行但非必需的功能，例如对现有算法运行速度或调用接口的完善）；
3. 如果您有兴趣参与本仓库某个issue的开发，可以采用[Fork + PullRequest 模式](https://gitee.com/help/articles/4128)，建议提前通过gitee站内私信或者[邮件联系我们](mailto:app_nav_alg@163.com)；
4. 如果您有兴趣以成员身份参与本仓库的开发，可以[邮件联系我们](mailto:app_nav_alg@163.com)，并说明您感兴趣的方向；
5. 参与本仓库开发时，请参考[《基于MATLAB的软件编码规范》](https://gitee.com/app-nav-alg/dev-stds/blob/master/MATLABCodingStandard.tex)及[《文档编写规范》](https://gitee.com/app-nav-alg/dev-stds/blob/master/DocumentWritingStandard.tex)；
6. 在参与开发及应用的过程中，如果需要引导或支持，请[邮件联系我们](mailto:app_nav_alg@163.com)。
